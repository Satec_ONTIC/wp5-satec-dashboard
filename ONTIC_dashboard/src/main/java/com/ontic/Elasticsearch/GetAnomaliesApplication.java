package com.ontic.Elasticsearch;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GetAnomaliesApplication{
    public static void main(String[] args) throws IOException {
        SpringApplication.run(GetAnomaliesApplication.class, args);
    }
}

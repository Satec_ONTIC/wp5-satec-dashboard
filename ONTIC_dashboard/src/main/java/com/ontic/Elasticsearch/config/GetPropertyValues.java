package com.ontic.Elasticsearch.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GetPropertyValues {
	InputStream inputStream;
 
	public String getPropValue(String key) throws IOException {
		String p="";
 
		try {
			Properties prop = new Properties();
			String propFileName = "config.properties";
 
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
 
			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
			}
 
			// get the property value and print it out
			p = prop.getProperty(key);
 
		} catch (Exception e) {
			System.err.println("Exception: " + e);
		} finally {
			inputStream.close();
		}
		return p;
	}
}

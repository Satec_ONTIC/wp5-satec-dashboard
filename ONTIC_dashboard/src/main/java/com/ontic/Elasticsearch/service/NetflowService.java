package com.ontic.Elasticsearch.service;

import java.io.IOException;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.ontic.Elasticsearch.domain.AverageBytes;
import com.ontic.Elasticsearch.domain.BytesSegundo;
import com.ontic.Elasticsearch.domain.ClosedCount;
import com.ontic.Elasticsearch.domain.NetflowCSV;
import com.ontic.Elasticsearch.domain.TopConv;
import com.ontic.Elasticsearch.domain.TopIp;
import com.ontic.Elasticsearch.domain.TopPorts;
import com.ontic.Elasticsearch.domain.TopTos;
import com.ontic.Elasticsearch.domain.TotalBytes;
import com.ontic.Elasticsearch.domain.TypeAnomalyAggr;
import com.ontic.Elasticsearch.domain.Value;

import io.searchbox.client.JestResult;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.SearchScroll;
import io.searchbox.core.search.aggregation.TermsAggregation.Entry;
import io.searchbox.core.search.sort.Sort;
import io.searchbox.params.Parameters;

@Service
public class NetflowService extends AbstractService{

	private String time_zone;
	private static Hashtable<String, String> hashIsp = new Hashtable<String, String>();
	private static Hashtable<String, String> hashCountry = new Hashtable<String, String>();
	@Autowired
	private GeoIpService serv;
	 
	public NetflowService(){
		super();
		try{
			this.time_zone = prop.getPropValue("time_zone");
		 }catch(Exception e){}
	}
	public List<String> getFileName(){
		List<String> dinamicPcapLoaded = new ArrayList<String>();
		String query = "{" + 
				"\"size\": 0,"+
				"\"aggs\": {"+
				"\"file\": {"+
				  "\"terms\": {"+
					"\"field\": \"netflowpcapfile\""+
				  "}"+
				"}"+
			"}"+
			"}";
		JSONArray arrj= makeAggrs( query, 0 );
		for(int i=0; i<arrj.length(); i++){
			JSONObject obj=arrj.getJSONObject(i);
			String k= obj.getString( "key" );
			dinamicPcapLoaded.add(k);
		}
		return dinamicPcapLoaded;
	}
	public List<NetflowCSV> getNetflows( String[] tmp ){
		tmp[0] = (tmp[0].split("\\."))[0];
		tmp[1] = (tmp[1].split("\\."))[0];
		String query = "{" + "\"_source\": { \"exclude\": [ \"tags\", \"@version\", \"geoipano\" ] },"
				+ "\"query\": { \"filtered\":{\"query\":{\"type\":{\"value\":\""+"netflow"+"\"} },\"filter\":{ \"range\": { \"netflowfirstswitched\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } } } } }" +
				"}";
//		Search search = new Search.Builder( query ).addIndex(index).addSort(new Sort("_doc")).setParameter(Parameters.SIZE, 10000).setParameter(Parameters.SCROLL, "5m").build();
		Search search = new Search.Builder( query ).addIndex(index).setParameter(Parameters.SIZE, 10000).setParameter(Parameters.SCROLL, "5m").build();
		JestResult result = null;
		try {
			result = client.execute(search);
		}catch (Exception ex){
			System.out.println(ex);
		}
		JsonArray hits = result.getJsonObject().getAsJsonObject("hits").getAsJsonArray("hits");
		String scrollId = result.getJsonObject().get("_scroll_id").getAsString();
		List<NetflowCSV> list = new ArrayList<NetflowCSV>();
		for(;;){
			SearchScroll scroll = new SearchScroll.Builder(scrollId, "5m")
                    .setParameter(Parameters.SIZE, 10000).build();
            try {
				result = client.execute(scroll);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            hits = result.getJsonObject().getAsJsonObject("hits").getAsJsonArray("hits");
            scrollId = result.getJsonObject().getAsJsonPrimitive("_scroll_id").getAsString();
            for (int i = 0; i < hits.size(); i++) {
				JsonElement jsonObj = hits.get(i);
				ObjectMapper mapper = new ObjectMapper();
				NetflowCSV anm = null;
				try {
					anm = mapper.readValue(jsonObj.getAsJsonObject().toString(), NetflowCSV.class);
				} catch (Exception ex) {
					System.out.println(ex);
				}
				list.add(anm);
			}
            if( hits.size()==0 ){
            	break;
            }
		}
			
		return list;
	}
	public List<ClosedCount> getClosedNetflowsBetween( String[] tmp){
		return makeAggrsClosed( createQueryDateClosed( tmp) );
	}
	public List<AverageBytes> getNetflowsBetween( String[] tmp, String type ){
		return makeAggrsB( createQueryDate( tmp, type ) );
	}
	public List<TotalBytes> getTotalsBetween( String[] tmp, String type ){
		return makeAggrsT( createQueryDateTot( tmp, type ) );
	}
	public List<TopPorts> getPorts( String[] tmp, String type ){
		return makeAggrsPorts( createQueryTerm(tmp, type, 5) );
	}
	public List<TopTos> getTos( String[] tmp, String type ){
		return makeAggrsTos( createQueryTerm(tmp, type, 5) );
	}
	public List<TopIp> getIp( String[] tmp, String type ){
		return makeAggrsIp( createQueryTerm(tmp, type, 5) );
	}
	public List<TopConv> getConv( String[] tmp, String type, int size ){
		return makeAggrsConv( createQueryTerm(tmp, type, size) );
	}
	public List<Value> getAvg( String[] tmp, String type ){
		return makeAggrsValue( createQueryAvg(tmp, type) );
	}
	public List<Value> getSum( String[] tmp, String type ){
		return makeAggrsValue( createQuerySum(tmp, type) );
	}
	public BytesSegundo getBaby( String[] tmp ){
		String query = createQuerySum(tmp, "netflowinbytes");
		Search search = new Search.Builder( query ).addIndex (index).build();
		SearchResult result=null;
		try{
			result = client.execute(search);
		}catch(IOException ex){}
        Double sum = result.getAggregations().getSumAggregation("average").getSum();
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dt.setTimeZone(TimeZone.getTimeZone("UTC"));
        long seconds=0;
        try {
			Date a = dt.parse( tmp[0] );
			Date b = dt.parse( tmp[1] );
	
			Date startNetflow = dateNetflow(index, "asc");
			Date endNetflow =dateNetflow(index, "desc");	
			if (startNetflow.getTime()>a.getTime()) a=startNetflow;
			if (b.getTime()>endNetflow.getTime()) b=endNetflow;
			seconds = (b.getTime()-a.getTime())/1000;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}        
		return new BytesSegundo( sum/seconds );
	}
	private String createQueryAvg(String[] tmp, String type){
		String query = "";
		String args[];
		//not decimals
		tmp[0] = (tmp[0].split("\\."))[0];
		tmp[1] = (tmp[1].split("\\."))[0];
		if( tmp.length == 3 ){
			args=tmp[2].split("=");
			query = "{"+
					"\"query\": { \"filtered\":{ \"query\":{\"match\":{\""+args[0]+"\":\""+args[1]+"\"} }, \"filter\":{ \"range\": { \"netflowfirstswitched\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } } } } },"+
				"\"size\": 0,"+ 
				"\"aggs\": {"+
							"\"average\":"+ 
							"{"+
								"\"avg\": {\"field\": \""+type+"\"}"+
							"}"+
						"}"+
				"}";
		}else if( tmp.length == 2 ){
			query = "{"+
					"\"query\": { \"filtered\": { \"filter\":{ \"range\": { \"netflowfirstswitched\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } }"+" } } },"+
				"\"size\": 0,"+ 
				"\"aggs\": {"+
							"\"average\":"+ 
							"{"+
								"\"avg\": {\"field\": \""+type+"\"}"+
							"}"+
						"}"+
				"}";
		}
		return query;
	}
	private String createQuerySum(String[] tmp, String type){
		String query = "";
		String args[];
		if( tmp.length == 3 ){
			//not decimals
			tmp[0] = (tmp[0].split("\\."))[0];
			tmp[1] = (tmp[1].split("\\."))[0];
			args=tmp[2].split("=");
			query = "{"+
					"\"query\": { \"filtered\":{ \"query\":{\"match\":{\""+args[0]+"\":\""+args[1]+"\"} }, \"filter\":{ \"range\": { \"netflowfirstswitched\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } } } } },"+
				"\"size\": 0,"+ 
				"\"aggs\": {"+
							"\"average\":"+ 
							"{"+
								"\"sum\": {\"field\": \""+type+"\"}"+
							"}"+
						"}"+
				"}";
		}else if( tmp.length == 2 ){
			tmp[0] = (tmp[0].split("\\."))[0];
			tmp[1] = (tmp[1].split("\\."))[0];
			query = "{"+
					"\"query\": { \"filtered\":{ \"query\":{\"match_all\":{} }, \"filter\":{ \"range\": { \"netflowfirstswitched\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } } } } },"+
				"\"size\": 0,"+ 
				"\"aggs\": {"+
							"\"average\":"+ 
							"{"+
								"\"sum\": {\"field\": \""+type+"\"}"+
							"}"+
						"}"+
				"}";
		}
		return query;
	}
	private String createQueryDateClosed( String[] tmp){
		String query = "";
		String args[];
		//not decimals
		tmp[0] = (tmp[0].split("\\."))[0];
		tmp[1] = (tmp[1].split("\\."))[0];
		
		int intervalS = calculateInterval( tmp[0],tmp[1]);
	
		if( tmp.length == 2 ){
			query = "{"+
				"\"query\": { \"filtered\": { \"filter\":{ \"range\": { \"netflowfirstswitched\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } }"+" } } },"+
				"\"size\": 0,"+ 
				"\"aggs\": {"+
					"\"group_by_second\":"+ 
					"{"+
						"\"date_histogram\":{"+
							"\"field\": \"netflowfirstswitched\","+
							"\"interval\": \""+intervalS+"s\""+
						"}"+
					"}"+	
				"}"+
				"}";
		}else if( tmp.length == 3 ){
			args=tmp[2].split("=");
			query = "{"+
					"\"query\": { \"filtered\":{ \"query\":{\"match\":{\""+args[0]+"\":\""+args[1]+"\"} }, \"filter\":{ \"range\": { \"netflowfirstswitched\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } } } } },"+
					"\"size\": 0,"+ 
					"\"aggs\": {"+
						"\"group_by_second\":"+ 
						"{"+
							"\"date_histogram\":{"+
								"\"field\": \"netflowfirstswitched\","+
								"\"interval\": \""+intervalS+"s\""+
							"}"+
						"}"+	
					"}"+
					"}";
		}
		return query;
	}
	private String createQueryDate( String[] tmp, String type){
		String query = "";
		String args[];
		//not decimals
		tmp[0] = (tmp[0].split("\\."))[0];
		tmp[1] = (tmp[1].split("\\."))[0];
		
		int intervalS = calculateInterval( tmp[0],tmp[1]);

		if( tmp.length == 2 ){
			query = "{"+
				"\"query\": { \"filtered\": { \"filter\":{ \"range\": { \"netflowfirstswitched\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } }"+" } } },"+
				"\"size\": 0,"+ 
				"\"aggs\": {"+
					"\"group_by_second\":"+ 
					"{"+
						"\"date_histogram\":{"+
							"\"field\": \"netflowfirstswitched\","+
							"\"interval\": \""+intervalS+"s\""+
						"},"+
						"\"aggs\":{"+
							"\"average\":"+ 
							"{"+
								"\"avg\": {\"field\": \""+type+"\"}"+
							"}"+
						"}"+
					"}"+	
				"}"+
				"}";
		}else if( tmp.length == 3 ){
			args=tmp[2].split("=");
			query = "{"+
					"\"query\": { \"filtered\":{ \"query\":{\"match\":{\""+args[0]+"\":\""+args[1]+"\"} }, \"filter\":{ \"range\": { \"netflowfirstswitched\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } } } } },"+
					"\"size\": 0,"+ 
					"\"aggs\": {"+
						"\"group_by_second\":"+ 
						"{"+
							"\"date_histogram\":{"+
								"\"field\": \"netflowfirstswitched\","+
								"\"interval\": \""+intervalS+"s\""+
							"},"+
							"\"aggs\":{"+
								"\"average\":"+ 
								"{"+
									"\"avg\": {\"field\": \""+type+"\"}"+
								"}"+
							"}"+
						"}"+	
					"}"+
					"}";
		}
		return query;
	}
	private String createQueryDateTot( String[] tmp, String type){
		String query = "";
		String args[];
		//not decimals
		tmp[0] = (tmp[0].split("\\."))[0];
		tmp[1] = (tmp[1].split("\\."))[0];

		int intervalS = calculateInterval( tmp[0],tmp[1]);
		
		if( tmp.length == 2 ){
			query = "{"+
				"\"query\": { \"filtered\": { \"filter\":{ \"range\": { \"netflowfirstswitched\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } }"+" } } },"+
				"\"size\": 0,"+ 
				"\"aggs\": {"+
					"\"group_by_second\":"+ 
					"{"+
						"\"date_histogram\":{"+
							"\"field\": \"netflowfirstswitched\","+
							"\"interval\": \""+intervalS+"s\""+
						"},"+
						"\"aggs\":{"+
							"\"total\":"+ 
							"{"+
								"\"sum\": {\"field\": \""+type+"\"}"+
							"}"+
						"}"+
					"}"+	
				"}"+
				"}";
		}else if( tmp.length == 3 ){
			args=tmp[2].split("=");
			query = "{"+
					"\"query\": { \"filtered\":{ \"query\":{\"match\":{\""+args[0]+"\":\""+args[1]+"\"} }, \"filter\":{ \"range\": { \"netflowfirstswitched\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } } } } },"+
					"\"size\": 0,"+ 
					"\"aggs\": {"+
						"\"group_by_second\":"+ 
						"{"+
							"\"date_histogram\":{"+
								"\"field\": \"netflowfirstswitched\","+
								"\"interval\": \""+intervalS+"s\""+
							"},"+
							"\"aggs\":{"+
								"\"total\":"+ 
								"{"+
									"\"sum\": {\"field\": \""+type+"\"}"+
								"}"+
							"}"+
						"}"+	
					"}"+
					"}";
		}
		return query;
	}
	private String createQueryTerm(String[] tmp, String type, int top){
		String query = "";
		String args[];
		//not decimals
		tmp[0] = (tmp[0].split("\\."))[0];
		tmp[1] = (tmp[1].split("\\."))[0];
		if(tmp.length == 3){
			args=tmp[2].split("=");
			query = "{"+
					"\"query\": { \"filtered\": { \"query\":{\"match\":{\""+args[0]+"\":\""+args[1]+"\"} },\"filter\":{ \"range\": { \"netflowfirstswitched\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } } } } },"+
					"\"size\": 0,"+ 
					"\"aggs\": {"+
						"\"group\":"+ 
						"{"+
							"\"terms\":{"+
								"\"field\": \""+type+"\","+
								"\"size\": "+top+
							"}"+
						"}"+	
					"}"+
					"}";
		}else if(tmp.length == 2){
			query = "{"+
					"\"query\": { \"filtered\": { \"filter\":{ \"range\": { \"netflowfirstswitched\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } } } } },"+
					"\"size\": 0,"+ 
					"\"aggs\": {"+
						"\"group\":"+ 
						"{"+
							"\"terms\":{"+
								"\"field\": \""+type+"\","+
								"\"size\": "+top+
							"}"+
						"}"+	
					"}"+
					"}";
		}
		return query;
	}
	
	private List<TopConv> makeAggrsConv( String query ){
		JSONArray obj = makeAggrs( query, 0 );
		List<TopConv> list = new ArrayList<TopConv>();

		for (int i = 0; i < obj.length(); i++) {
				 JSONObject jsonObj = obj.getJSONObject(i);
				 ObjectMapper mapper = new ObjectMapper();
				 try{
					 String arrayInfo [] =jsonObj.getString("key").split("_");
					 String c0 = serv.geoIp( InetAddress.getByName( arrayInfo[0] ) );
					 String c1 = serv.geoIp( InetAddress.getByName( arrayInfo[1] ) );
					 c0=(c0.split("country_code3\":\\[\"")[1]).split("\"]")[0];
					 c1=(c1.split("country_code3\":\\[\"")[1]).split("\"]")[0];
					 hashCountry.put( arrayInfo[0], c0.toLowerCase() );
					 hashCountry.put( arrayInfo[1], c1.toLowerCase() );
					 TopConv anm = new TopConv( hashIsp,hashCountry );
				    anm = mapper.readValue(jsonObj.toString(), TopConv.class);
				    anm.setIsps();
				    if( arrayInfo.length>2 )
				    	anm.setTranslatedPorts( queryServicePort( Long.parseLong( arrayInfo [2] ) ), queryServicePort( Long.parseLong( arrayInfo [3] ) ) );
				    list.add( anm );
				 }catch( Exception ex){} 
		}
		return list;
	}
	private List<Value> makeAggrsValue( String query ){
		JSONArray obj = makeAggrs( query, 1 );
		List<Value> list = new ArrayList<Value>();

		for (int i = 0; i < obj.length(); i++) {
				 JSONObject jsonObj = obj.getJSONObject(i);
				 ObjectMapper mapper = new ObjectMapper();
				 Value anm = null;
				 try{
				    anm = mapper.readValue(jsonObj.toString(), Value.class);
				 }catch( Exception ex){ System.out.println(ex);}
				 list.add( anm );
		}
			
		return list;
	}
	private List<TopIp> makeAggrsIp( String query ){
		JSONArray obj = makeAggrs( query, 0 );
		List<TopIp> list = new ArrayList<TopIp>();

		for (int i = 0; i < obj.length(); i++) {
				 JSONObject jsonObj = obj.getJSONObject(i);
				 ObjectMapper mapper = new ObjectMapper();
				 TopIp anm = null;
				 try{
				    anm = mapper.readValue(jsonObj.toString(), TopIp.class);
				    anm.addIdx( i+1 );
				 }catch( Exception ex){ System.out.println(ex);}
				 list.add( anm );
		}
			
		return list;
	}
	
	private List<TopTos> makeAggrsTos( String query ){
		JSONArray obj = makeAggrs( query, 0 );
		List<TopTos> list = new ArrayList<TopTos>();

		for (int i = 0; i < obj.length(); i++) {
				 JSONObject jsonObj = obj.getJSONObject(i);
				 ObjectMapper mapper = new ObjectMapper();
				 TopTos anm = null;
				 try{
				    anm = mapper.readValue(jsonObj.toString(), TopTos.class);
				    anm.addIdx( i+1 );
				 }catch( Exception ex){ System.out.println(ex);}
				 list.add( anm );
		}
				
		return list;
	}
	private List<TopPorts> makeAggrsPorts( String query ){
		List<TopTos> list = makeAggrsTos( query );
		List<TopPorts> list2 = new ArrayList<TopPorts>();

		for (TopTos tt : list ) {
			String newkey = queryServicePort( tt.getKey() );
				 TopPorts anm = new TopPorts( newkey );
				 anm.setC1( tt.count1 );
				 anm.setC2( tt.count2 );
				 anm.setC3( tt.count3 );
				 anm.setC4( tt.count4 );
				 anm.setC5( tt.count5 );
				 list2.add( anm );
		}
				
		return list2;
	}
	private String queryServicePort(long port) {
		String query = "{"
				+ "\"_source\": { \"exclude\": [\"Assignee\", \"Registration Date\", \"Contact\",\"Assignment Notes\", \"Reference\", \"Modification Date\", \"Service Code\"   ] },"
				+ "\"query\": { \"match\": { \"Port Number\":" + port + "}}" + "}";
		Search search = new Search.Builder(query).addIndex(index).build();
		SearchResult result = null;
		try {
			result = client.execute(search);
		} catch (Exception ex) {
			System.err.println(ex);
		}
		
		 String resultado [] = new String [5];
		
		try{
		    String json = result.getValue("hits").toString();
		    String [] trocitos = json.split("Description=");
		    resultado = trocitos [1].split(",");
		    resultado [0] = resultado [0] + " ("+ port + ")";
		}catch (Exception ea){
			 resultado [0] = "Not recognized service ("+port+")"; 
		}
		return resultado[0];
	}
	private List<AverageBytes> makeAggrsB( String query ){
		JSONArray obj = makeAggrs( query, 0 );
		List<AverageBytes> list = new ArrayList<AverageBytes>();

		for (int i = 0; i < obj.length(); i++) {
				 JSONObject jsonObj = obj.getJSONObject(i);
				 ObjectMapper mapper = new ObjectMapper();
				 AverageBytes anm = null;
				 try{
				    anm = mapper.readValue(jsonObj.toString(), AverageBytes.class);
				 }catch( Exception ex){ System.out.println(ex);}
				 list.add( anm );
		}
		return list;
	}
	private List<TotalBytes> makeAggrsT( String query ){
		JSONArray obj = makeAggrs( query, 0 );
		List<TotalBytes> list = new ArrayList<TotalBytes>();

		for (int i = 0; i < obj.length(); i++) {
				 JSONObject jsonObj = obj.getJSONObject(i);
				 ObjectMapper mapper = new ObjectMapper();
				 TotalBytes anm = null;
				 try{
				    anm = mapper.readValue(jsonObj.toString(), TotalBytes.class);
				 }catch( Exception ex){ System.out.println(ex);}
				 list.add( anm );
		}
				
		return list;
	}
	private List<ClosedCount> makeAggrsClosed( String query ){
		JSONArray obj = makeAggrs( query, 0 );
		List<ClosedCount> list = new ArrayList<ClosedCount>();

		for (int i = 0; i < obj.length(); i++) {
				 JSONObject jsonObj = obj.getJSONObject(i);
				 ObjectMapper mapper = new ObjectMapper();
				 ClosedCount anm = null;
				 try{
				    anm = mapper.readValue(jsonObj.toString(), ClosedCount.class);
				 }catch( Exception ex){ System.out.println(ex);}
				 list.add( anm );
		}		
		return list;
	}
	/*
	 * 
	 */
	// for single aggregation queries
	private JSONArray makeAggrs( String query, int n ){
		Search search = new Search.Builder( query ).addIndex (index).build();
		SearchResult result=null;
		try{
			result = client.execute(search);
		}catch(IOException ex){}
		String jsons[];
		String json = result.getJsonString();
		switch( n ){
		case 0:
			jsons= json.split("\"buckets\":");
			if( jsons.length>=2 ){
				json = (jsons[1].split("}}}"))[0];
			}else{
				json="[]";
			}
			break;
		case 1:
			jsons=json.split("\"average\":");
			if( jsons.length>=2 ){
				json = (jsons[1].split("}}}"))[0];
				json = "["+json+"}]";
			}else{
				json="[]";
			}
			break;
		default:
			break;
		}

		JSONArray obj = new JSONArray( json );
		return obj;
	}
	
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////7777777
	
	
	private Date dateNetflow(String index, String order) {
		String query = "{"+
				"\"size\": 1,"+
				 "\"sort\" :" +
				        "{ \"netflowfirstswitched\" : {\"order\" : \""+order+"\"}},"+
				  "\"query\": {\"match_all\": {} }"+
				"}";
		
		Search search = new Search.Builder(query).addIndex(index).build();
		SearchResult result = null;
		try {
			result = client.execute(search);
		} catch (Exception ex) {
			System.err.println(ex);
		}
		
		 String resultado [] = new String [5];
		
		try{
		    String json = result.getValue("hits").toString();
		    String [] trocitos = json.split("netflowfirstswitched=");
		    resultado = trocitos [1].split(",");
		    resultado [0] = resultado [0];
		}catch (Exception ea){
			 resultado [0] = "Not recognized service ()"; 
		}
		resultado[0]= resultado[0].replace('T', ' ');
		resultado[0]= resultado[0].replace("Z", " GMT");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS zzz");
		Date ret=null;
		try {
			ret = df.parse(resultado[0]);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("Time netflow elastic: "+ ret);
		return ret;
	}
	
	private int calculateInterval( String start, String end){
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("GMT"));;
		Date requestStart=null;
		Date requestEnd=null;
		try {
			requestStart = df.parse(start);
			requestEnd = df.parse(end);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		String searchIndex=index;
		
		Date startNetflow = dateNetflow(searchIndex, "asc");
		Date endNetflow =dateNetflow(searchIndex, "desc");
		
		if (startNetflow.getTime()>requestStart.getTime()) requestStart=startNetflow;
		if (requestEnd.getTime()>endNetflow.getTime()) requestEnd=endNetflow;
		
		long durationMS = requestEnd.getTime()-requestStart.getTime();
		

		int intervalS = Math.round((durationMS/100)/1000);

		if (intervalS > 10){
			return intervalS;
		}else return 10;
	}
	public Map<String, Integer> getImportances( String src ) {
		String monitor = "{"+
							"\"query\": {"+
								"\"bool\": {"+
									"\"should\": ["
										+"{"+
											"\"match\": { \"typeanomaly\":\"*ATTACK*\" }"+
										"},"
										+"{"+
											"\"match\": { \"typeanomaly\":\"*MONITOR*\" }"+
										"}"
									+ "]"+
								"}"+
							"},"+
							"\"size\": 0"+
						"}";
		Search search = new Search.Builder( monitor ).addIndex (index).build();
		SearchResult result=null;
		try{
			result = client.execute( search );
		}catch(IOException ex){}
		String attack = "{"+
				"\"query\": {"+
					"\"bool\": {"+
						"\"should\": ["
							+"{"+
								"\"match\": { \"typeanomaly\":\"*ATTACK*\" }"+
							"}"
						+ "]"+
					"}"+
				"},"+
				"\"size\": 0"+
			"}";
		Search search2 = new Search.Builder( attack ).addIndex (index).build();
		SearchResult result2=null;
		try{
			result2 = client.execute( search2 );
		}catch(IOException ex){}
		String all = "{"+
				"\"query\": {"+
					"\"filtered\" : {"+
			            "\"filter\" : {"+
			                "\"type\" : { \"value\" : \""+src+"\" }"+
			            "}"+
	            	"}"+
				"},"+
				"\"size\": 0"+
			"}";
		Search search3 = new Search.Builder( all ).addIndex (index).build();
		SearchResult result3=null;
		try{
			result3 = client.execute( search3 );
		}catch(IOException ex){}
		Map<String, Integer> map = new TreeMap<String, Integer>();

		map.put("all" , result3.getTotal() );
		map.put("monitor" , result.getTotal() );
		map.put("attack" , result2.getTotal() );
		
		return map;
	}
	
}

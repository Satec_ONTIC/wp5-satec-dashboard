package com.ontic.Elasticsearch.service;

import com.ontic.Elasticsearch.config.GetPropertyValues;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;

public class AbstractService {

	protected JestClient client;
	protected GetPropertyValues prop;
	// name of index in elasticsearch
	protected String index;
	protected String ela;
	
	public AbstractService() {
		this.prop = new GetPropertyValues();
		 try{
			 this.index = prop.getPropValue("index");
			 this.ela = prop.getPropValue("elasticsearch");
		 }catch(Exception e){
			 System.err.println( e );
			 System.exit( 1 );
		 }
		JestClientFactory factory = new JestClientFactory();
		factory.setHttpClientConfig(new HttpClientConfig
		                        .Builder(ela)
		                        .multiThreaded(true)
		                        .readTimeout(0)
		                        .build());
		 this.client = factory.getObject();
	}
}

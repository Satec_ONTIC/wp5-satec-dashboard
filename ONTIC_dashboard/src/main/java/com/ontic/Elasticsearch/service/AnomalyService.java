package com.ontic.Elasticsearch.service;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ontic.Elasticsearch.domain.AnomalyQuery;
import com.ontic.Elasticsearch.domain.NetflowRecord;
import com.ontic.Elasticsearch.domain.TimeSeries;
import com.ontic.Elasticsearch.domain.TypeAnomaly;
import com.ontic.Elasticsearch.domain.TypeAnomalyAggr;
import com.ontic.Elasticsearch.domain.chusta;

import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.search.aggregation.TermsAggregation.Entry;

@Service
public class AnomalyService extends AbstractService{

	private String time_zone;
	@Autowired
	private GeoIpService geoip;

	public AnomalyService() {
		super();
		try{
			this.time_zone = prop.getPropValue("time_zone");
		}catch(Exception e){}
//		geoip = new GeoIpService();
	}
	
//	private String[] limitPeriod( String[] tmp ){
//		tmp[0] = (tmp[0].split("\\."))[0];
//		tmp[1] = (tmp[1].split("\\."))[0];
//		String query="{" + "\"_source\": { \"exclude\": [ \"tags\", \"@version\", \"geoipano\" ] },"
//				+ "\"query\": { \"filtered\":{\"query\":{\"type\":{\"value\":\"netflow\"} } } }," +
//				"\"sort\": { \"netflowfirstswitched\": { \"order\": \"desc\" }}," +
//				"\"size\": 1"+
//				"}";
//		Search search = new Search.Builder( query ).addIndex(index).build();
//		SearchResult result = null;
//		try {
//			result = client.execute(search);
//		} catch (Exception ex) {
//			System.out.println(ex);
//		}
//		String json = result.getJsonString();
//		String jsons[]=json.split("\"_source\":");
//		if( jsons.length >=2 ){
//			json=jsons[1];
//			json=json.split(",\"sort\"")[0];
//			JSONObject obj = new JSONObject( json );
//			ObjectMapper mapper = new ObjectMapper();
//			NetflowRecord anm = null;
//			try {
//				anm = mapper.readValue(obj.toString(), NetflowRecord.class);
//			} catch (Exception ex) {
//				System.out.println(ex);
//			}
//			Date fi= anm.getFirst4db();
//			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			try {
//				Date fi2 = formatter.parse(tmp[1]);
//				Date fi1 = formatter.parse(tmp[0]);
//				if( fi.before(fi2) ){
//					tmp[1]=formatter.format(fi);
//					if( fi.before(fi1)){
//						tmp[0]=formatter.format(fi); // not really needed
//					}
//				}
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		return tmp;
//	}

	public Map<String, Set<chusta>> getLastAnomaliesTable(HttpServletRequest request, String xmlSource, String[] tmp, Object dissim ){
		String query = "";
//		tmp=limitPeriod(tmp);
		if( dissim==null || dissim.equals( "all" ) ){
			query = "{" + "\"_source\": { \"exclude\": [ \"tags\", \"@version\", \"geoipano\" ] },"
					+ "\"query\": { \"filtered\":{\"query\":{\"type\":{\"value\":\""+xmlSource+"\"} },\"filter\":{ \"range\": { \"start\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } } } } }," +
					"\"size\": 1000"+
					"}";
		}else if( dissim.equals( "attack" ) ){
			query = "{" + "\"_source\": { \"exclude\": [ \"tags\", \"@version\", \"geoipano\" ] },"
					+ "\"query\": { \"filtered\":{\"query\":{\"type\":{\"value\":\""+xmlSource+"\"} },\"filter\":{ \"and\":[ {\"range\": { \"start\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" }}},{\"or\":[{ \"match\": { \"typeanomaly\": \"*ATTACK*\"} }]} ]} } }," +
					"\"size\": 1000"+
					"}";
		}else if( dissim.equals( "monitor" ) ){
			query = "{" + "\"_source\": { \"exclude\": [ \"tags\", \"@version\", \"geoipano\" ] },"
					+ "\"query\": { \"filtered\":{\"query\":{\"type\":{\"value\":\""+xmlSource+"\"} },\"filter\":{ \"and\":[ {\"range\": { \"start\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" }}},{\"or\":[{ \"match\": { \"typeanomaly\": \"*ATTACK*\"} },{ \"match\": { \"typeanomaly\": \"*MONITOR*\"} }]} ]} } }," +
					"\"size\": 1000"+
					"}";
		}
		return makeSearchMap( request, xmlSource, query );
	}
	/*
	 * Get the anomalies received some time ago
	 */
	public List<AnomalyQuery> getLastAnomalies( String xmlSource, String[] tmp, Object dissim ) {
		String query="";
//		tmp=limitPeriod( tmp );
		if( dissim==null || dissim.equals( "all" ) ){
			query = "{" + "\"_source\": { \"exclude\": [ \"tags\", \"@version\", \"geoipano\" ] },"
					+ "\"query\": { \"filtered\":{\"query\":{\"type\":{\"value\":\""+xmlSource+"\"} },\"filter\":{ \"range\": { \"start\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } } } } }," +
					"\"size\": 10000"+
					"}";
		}else if( dissim.equals( "attack" ) ){
			query = "{" + "\"_source\": { \"exclude\": [ \"tags\", \"@version\", \"geoipano\" ] },"
					+ "\"query\": { \"filtered\":{\"query\":{\"type\":{\"value\":\""+xmlSource+"\"} },\"filter\":{ \"and\":[ {\"range\": { \"start\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" }}},{\"or\":[{ \"match\": { \"typeanomaly\": \"*ATTACK*\"} }]} ]} } }," +
					"\"size\": 10000"+
					"}";
		}else if( dissim.equals( "monitor" ) ){
			query = "{" + "\"_source\": { \"exclude\": [ \"tags\", \"@version\", \"geoipano\" ] },"
					+ "\"query\": { \"filtered\":{\"query\":{\"type\":{\"value\":\""+xmlSource+"\"} },\"filter\":{ \"and\":[ {\"range\": { \"start\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" }}},{\"or\":[{ \"match\": { \"typeanomaly\": \"*ATTACK*\"} },{ \"match\": { \"typeanomaly\": \"*MONITOR*\"} }]} ]} } }," +
					"\"size\": 10000"+
					"}";
		}
		return makeSearch( query );
	}

	/*
	 * Get the anomaly
	 */
	public List<AnomalyQuery> getAnomaly(String xmlSource, String[] tmp) {
		SimpleDateFormat dt2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String plus1="";
		try{
			Date tmp1 = dt2.parse( tmp[1] );
			tmp1.setTime( tmp1.getTime()+1000 );
			plus1=dt2.format( tmp1 );
		}catch(ParseException ex){
			
		}
		String query = "{"
				+ "\"query\": { \"filtered\":{\"query\":{\"type\":{\"value\":\""+xmlSource+"\"}}, {\"match\":{\"point\":\""+tmp[0]+"\"} }, \"filter\":{ \"range\": { \"start\": { \"gte\": \""+tmp[1]+"\", \"lte\": \""+plus1+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" } } } } }," 
				+ "\"size\": 1,"
				+ "\"_source\": { \"exclude\": [ \"tags\", \"@version\", \"geoipano\" ] }"
				+ "}";
		return makeSearch( query );
	}
	
	public List<List<TypeAnomalyAggr>> getTypesAnomalies( String xmlSource, String[] tmp, Object dissim ){
		String query="";
//		tmp=limitPeriod(tmp);
		List<List<TypeAnomalyAggr>> list = new ArrayList<List<TypeAnomalyAggr>>();
		query = "{"+
				"\"_source\": {"+
					"\"exclude\": [ \"tags\", \"@version\", \"geoipano\", \"doc_count_error_upper_bound\", \"sum_other_doc_count\" ]"+ 
				"},"+
				"\"query\": { \"filtered\":{\"query\":{\"type\":{\"value\":\""+xmlSource+"\"} },\"filter\":{ \"and\":[ {\"range\": { \"start\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" }}},{\"or\":[{ \"match\": { \"typeanomaly\": \"*ATTACK*\"} }]} ]} } }," +
				"\"size\": 0,"+
				"\"aggs\": {"+
					"\"start\": {"+
					  "\"terms\": {"+
						"\"field\": \"start\","+
						"\"size\" : 1000"+
					  "}"+
					"}"+
				"}"+
			"}";
		list.add(makeSearchTypes( query, "attack" ));
		if( dissim==null || dissim.equals( "monitor" ) || dissim.equals( "all" ) ){
			query = "{"+
					"\"_source\": {"+
						"\"exclude\": [ \"tags\", \"@version\", \"geoipano\", \"doc_count_error_upper_bound\", \"sum_other_doc_count\" ]"+ 
					"},"+
					"\"query\": { \"filtered\":{\"query\":{\"type\":{\"value\":\""+xmlSource+"\"} },\"filter\":{ \"and\":[ {\"range\": { \"start\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" }}},{\"or\":[{ \"match\": { \"typeanomaly\": \"*MONITOR*\"} }]} ]} } }," +
					"\"size\": 0,"+
					"\"aggs\": {"+
						"\"start\": {"+
						  "\"terms\": {"+
							"\"field\": \"start\","+
						    "\"size\" : 1000"+
						  "}"+
						"}"+
					"}"+
				"}";
			list.add(makeSearchTypes( query, "to monitor" ));
		}
		if( dissim==null || dissim.equals( "all" ) ){
			query = "{"+
					"\"_source\": {"+
						"\"exclude\": [ \"tags\", \"@version\", \"geoipano\", \"doc_count_error_upper_bound\", \"sum_other_doc_count\" ]"+ 
					"},"+
					"\"query\": { \"filtered\":{\"query\":{\"type\":{\"value\":\""+xmlSource+"\"} },\"filter\":{ \"and\":[ {\"range\": { \"start\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" }}},{\"or\":[{ \"match\": { \"typeanomaly\": \"*RECOGNITION*\"} }]} ]} } }," +
					"\"size\": 0,"+
					"\"aggs\": {"+
						"\"start\": {"+
						  "\"terms\": {"+
							"\"field\": \"start\","+
						    "\"size\" : 1000"+
						  "}"+
						"}"+
					"}"+
				"}";
			list.add(makeSearchTypes( query, "recognition" ));
			query = "{"+
					"\"_source\": {"+
						"\"exclude\": [ \"tags\", \"@version\", \"geoipano\", \"doc_count_error_upper_bound\", \"sum_other_doc_count\" ]"+ 
					"},"+
					"\"query\":{\"bool\":{\"filter\":{\"type\":{\"value\":\""+xmlSource+"\"}},\"must\": {\"range\": { \"start\": { \"gte\": \""+tmp[0]+"\", \"lte\": \""+tmp[1]+"\", \"format\": \"yyyy-MM-dd HH:mm:ss\", \"time_zone\": \""+time_zone+"\" }}},\"must_not\":[{ \"match\": { \"typeanomaly\": \"*MONITOR*\"} },{ \"match\": { \"typeanomaly\": \"*ATTACK*\"} },{ \"match\": { \"typeanomaly\": \"*RECOGNITION*\"} }]}}," +
					"\"size\": 0,"+
					"\"aggs\": {"+
						"\"start\": {"+
						  "\"terms\": {"+
							"\"field\": \"start\""+
						  "}"+
						"}"+
					"}"+
				"}";
			list.add(makeSearchTypes( query, "unknown" ));
		}
		return list;
	}

	private List<TypeAnomalyAggr> makeSearchTypes( String query, String tip) {
		Search search = new Search.Builder( query ).addIndex (index).build();
		SearchResult result = null;
		try {
			result = client.execute(search);
		} catch (Exception ex) {
			System.out.println( ex );
		}
		List<TypeAnomalyAggr> list = new ArrayList<TypeAnomalyAggr>();
		SimpleDateFormat dt2 = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );	
		
		for( Entry ts: result.getAggregations().getTermsAggregation("start").getBuckets() ){
					TypeAnomalyAggr ty = new TypeAnomalyAggr( tip, dt2.format( new Date(Long.parseLong(ts.getKey())) ), ts.getCount() );
					list.add( ty );
		}
		return list;
	}
	/*
	 * make query to elasticsearch and return results
	 */
	private List<AnomalyQuery> makeSearch( String query) {
		Search search = new Search.Builder( query ).addIndex (index).build();
		SearchResult result = null;
		try {
			result = client.execute(search);
		} catch (Exception ex) {
			System.out.println(ex);
		}
		List<AnomalyQuery> list = new ArrayList<AnomalyQuery>();
		String json = result.getJsonString();
		String jsons[]=json.split("\"hits\":");
		if( jsons.length >=3 ){
			json = jsons[2];
			JSONArray obj = new JSONArray(json);
			for (int i = 0; i < obj.length(); i++) {
				JSONObject jsonObj = obj.getJSONObject(i);
				ObjectMapper mapper = new ObjectMapper();
				AnomalyQuery anm = null;
				try {
					anm = mapper.readValue(jsonObj.toString(), AnomalyQuery.class);
				} catch (Exception ex) {
					System.out.println(ex);
				}
				list.add(anm);
			}
		}
		return list;
	}
	
	
	private Map<String, Set<chusta>> makeSearchMap( HttpServletRequest request,String xmlSource, String query ){
		Search search = new Search.Builder( query ).addIndex ( index ).build();
		SearchResult result = null;
		try {
			result = client.execute( search );
		} catch ( Exception ex ){
			System.out.println( ex );
		}
		String json = result.getJsonString();
		Map<String, Set<chusta>> mapa = new HashMap<String, Set<chusta>>();
		String jsons[]=json.split( "\"hits\":" );
		if( jsons.length >= 3 ){
			json = jsons[2];
			JSONArray obj = new JSONArray( json );
			SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			SimpleDateFormat dt2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			SortedSet<Date> set1 = new TreeSet<Date>();
			for (int i = 0; i < obj.length(); i++) {
				JSONObject jsonObj = obj.getJSONObject( i );
				ObjectMapper mapper = new ObjectMapper();
				AnomalyQuery anm = null;
				try {
					anm = mapper.readValue( jsonObj.toString(), AnomalyQuery.class );
				} catch (Exception ex) {
					System.out.println(ex);
				}
				if ( xmlSource.compareTo("frenchxml")==0 ){
					String point = anm.getSrcpoints()[0];
					String type = anm.getTypeanomaly();
					Date start = null;
					try{
						start = dt1.parse( anm.getStart() );
					}catch(ParseException e){
						e.getMessage();
					}
					Date end = null;
					try{
						end = dt1.parse( anm.getEnd() );
					}catch(ParseException e){
						e.getMessage();
					}
					List<String> dstPoint = new ArrayList<String>();
					for( String dstp :anm.getDstpoints() ){
						try {
							dstPoint.add( geoip.geoIp( InetAddress.getByName( dstp ) ) );
						} catch (UnknownHostException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					String[] dArray = new String[ dstPoint.size() ];
					dstPoint.toArray( dArray );
					String[] sArray = new String[ 1 ];
					try {
						sArray[0] = geoip.geoIp( InetAddress.getByName( point ) );
					} catch (UnknownHostException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					chusta nuevachusta = new chusta( sArray, type, dt2.format( start ), dt2.format( end ), dArray );
					if( mapa.containsKey( point ) ){
						Set<chusta> values = mapa.get( point );
						values.add( nuevachusta );
						mapa.put( point, values );
					}else{
						Set<chusta> value = new TreeSet< chusta >();
						value.add( nuevachusta );
						mapa.put( point, value );
					}
					set1.add( start );
				}else{
					/*
					 * alternative source of anomalies
					 */
				}
			}
		}
		return mapa;
	}
}

package com.ontic.Elasticsearch.service;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

import org.springframework.stereotype.Service;

import com.maxmind.geoip.LookupService;
import com.ontic.Elasticsearch.config.GetPropertyValues;

@Service
public class GeoIpService {
	
	static LookupService cl ;
	static com.maxmind.geoip.Location pru = new com.maxmind.geoip.Location();
	
	public GeoIpService(){
		GetPropertyValues prop = new GetPropertyValues();
		try{
			String pathgeo = prop.getPropValue( "geoipdatasource" );
			File database = new File( System.getProperty("user.dir") +'/'+ pathgeo );
			cl = new LookupService(database,
					LookupService.GEOIP_MEMORY_CACHE | LookupService.GEOIP_CHECK_CACHE);
		}catch(IOException e){
			System.err.println( "not geoIp file opened" );
		}
	}
	public String geoIp(InetAddress ipAddress) throws IOException {
		pru = cl.getLocation( ipAddress );
		String ret="";
		if( pru!=null ){
			ret = "{" + "\"ip\":[\"" + ipAddress.getHostAddress() + "\"]"
					+ ","+  "\"longitude\":[" + pru.longitude + "]"
					+ ","+  "\"latitude\":[" + pru.latitude + "]"
					+ ","+  "\"country_name\":[\"" + pru.countryName + "\"]" 
					+ ","+  "\"country_code3\":[\"" + pru.countryCode + "\"]" + "}";
		}
		return ret;
	}
	
	public String geoIpOnlyCountry(InetAddress ipAddress) throws IOException {
		pru = cl.getLocation( ipAddress );
		return pru.countryName;
	}
	
}
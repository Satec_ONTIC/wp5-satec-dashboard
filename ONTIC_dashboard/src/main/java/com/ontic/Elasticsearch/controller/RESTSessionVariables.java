package com.ontic.Elasticsearch.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ontic.Elasticsearch.domain.Timepicker;

@RestController
public class RESTSessionVariables {

	Timepicker test = new Timepicker();
	
	@RequestMapping("/setpicker/{pickerformat}")
	public void setTimePicker(HttpServletRequest request, @PathVariable("pickerformat") String pickerformat){
		String tmp[] = pickerformat.split("X");
		test.setPicker( tmp[0], tmp[1], tmp[2], tmp[3], tmp[4] );
		request.getSession().setAttribute( "picker", test );
	}
	@RequestMapping("/getpicker")
	public String getTimePicker(HttpServletRequest request) {
		Timepicker test = new Timepicker();
		try {
			test = (Timepicker) request.getSession().getAttribute("picker");
			return test.getPicker();
		} catch (java.lang.NullPointerException e){
			return "";
		}
	}
	public int getMode(HttpServletRequest request) {
		Timepicker test = new Timepicker();
		try{
			test = (Timepicker) request.getSession().getAttribute("picker");
			String ret = test.getPicker();
			String tmp[] = ret.split("X");
			return Integer.parseInt(tmp[6]);
		}catch(java.lang.NullPointerException e){
			return 0;
		}
	}
	
	public boolean getFixed(HttpServletRequest request) {
		Timepicker test = new Timepicker();
		try {
			test = (Timepicker) request.getSession().getAttribute("picker");
			String ret = test.getPicker();
			String tmp[] = ret.split("X");
			return Boolean.valueOf(tmp[8]);
		} catch (java.lang.NullPointerException e) {
			return false;
		}
	}
}

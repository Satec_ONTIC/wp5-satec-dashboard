package com.ontic.Elasticsearch.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ontic.Elasticsearch.domain.Body;

@RestController
public class RestHtml {

	@RequestMapping("/traffic")
    public Body getTraffic(HttpServletRequest request) {
		return new Body( getHtml("traffic.html") );	
    }
	@RequestMapping("/flow")
    public Body getFlow(HttpServletRequest request) {
		return new Body( getHtml("flow.html") );	
    }
	@RequestMapping("/unada")
    public Body unada(HttpServletRequest request) {
		return new Body( getHtml("anomalies.html") );	
    }
	@RequestMapping("/settings")
    public Body settings(HttpServletRequest request) {
		return new Body( getHtml("settings.html") );	
    }
	@RequestMapping("/openpcap")
    public Body openpcap(HttpServletRequest request) {
		return new Body( getHtml("open.html") );	
    }
	@RequestMapping("/demo")
    public Body demo(HttpServletRequest request) {
		return new Body( getHtml("demo.html") );
    }
	@RequestMapping("/forensic")
    public Body forensic(HttpServletRequest request) {
		return new Body( getHtml("forensic.html") );
    }
	@RequestMapping("/forensic2")
    public Body forensic2(HttpServletRequest request) {
		return new Body( getHtml("forensic2.html") );
    }
	@RequestMapping("/forensic3")
    public Body forensic3(HttpServletRequest request) {
		return new Body( getHtml("forensic3.html") );
    }
	@RequestMapping("/realtime")
    public Body realtime(HttpServletRequest request) {
		return new Body( getHtml("realtime.html") );
    }
	@RequestMapping("/export")
    public Body export(HttpServletRequest request) {
		return new Body( getHtml("export.html") );
    }
	private String getHtml( String file ){
		StringBuilder contentBuilder = new StringBuilder();
		InputStream in = getClass().getClassLoader().getResourceAsStream("static/"+file);
		
		Scanner scanner = new Scanner( in );
		while(scanner.hasNextLine()){            
			contentBuilder.append( scanner.nextLine().toString() );
		}
		scanner.close();
		try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return contentBuilder.toString();
	}
}

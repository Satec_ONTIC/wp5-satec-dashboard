package com.ontic.Elasticsearch.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.ontic.Elasticsearch.domain.AnomalyCSV;
import com.ontic.Elasticsearch.domain.AnomalyQuery;
import com.ontic.Elasticsearch.domain.NetflowCSV;
import com.ontic.Elasticsearch.service.AnomalyService;
import com.ontic.Elasticsearch.service.NetflowService;

@Controller
public class CSVFileDownloadController {
	@Autowired
	private AnomalyService service;
	@Autowired
	private NetflowService nservice;

	@RequestMapping(value = "/downloadCSVa/{bet}")
    public void downloadCSV( HttpServletResponse response, @PathVariable("bet") String between ) throws IOException {
 
		String tmp[] = between.split("X");
        String csvFileName = "anomalies.csv";
 
        response.setContentType( "text/csv" );
 
        // creates mock data
        String headerKey = "Content-Disposition";
        String headerValue = String.format( "attachment; filename=\"%s\"", csvFileName );
        response.setHeader( headerKey, headerValue );
 
        List<AnomalyQuery> l = service.getLastAnomalies( "frenchxml", tmp, null );
 
 
        // uses the Super CSV API to generate CSV data from the model data
        ICsvBeanWriter csvWriter = new CsvBeanWriter( response.getWriter(), CsvPreference.STANDARD_PREFERENCE );
 
        String[] header = { "id", "start", "end", "important", "typeanomaly", "srcpoints", "dstpoints" };
 
        csvWriter.writeHeader(header);
 
        for ( AnomalyQuery a : l ) {
        	AnomalyCSV csv = new AnomalyCSV( a );
            csvWriter.write( csv, header );
        }
        csvWriter.flush();
        csvWriter.close();
    }
	@RequestMapping(value = "/downloadCSVn/{bet}")
    public void downloadCSVn( HttpServletResponse response, @PathVariable("bet") String between ) throws IOException {
 
		String tmp[] = between.split("X");
        String csvFileName = "netflows.csv";
 
        response.setContentType( "text/csv" );
        
        // creates mock data
        String headerKey = "Content-Disposition";
        String headerValue = String.format( "attachment; filename=\"%s\"", csvFileName );
        response.setHeader( headerKey, headerValue );
 
        List<NetflowCSV> l = nservice.getNetflows( tmp );
        // uses the Super CSV API to generate CSV data from the model data
//        ICsvBeanWriter csvWriter = new CsvBeanWriter( new FileWriter(csvFileName), CsvPreference.STANDARD_PREFERENCE );
        ICsvBeanWriter csvWriter = new CsvBeanWriter( response.getWriter(), CsvPreference.STANDARD_PREFERENCE );
 
        String[] header = { "conversation", "flow",	"netflowfirstswitched", "netflowinbytes","netflowinpkts","netflowipv4dstaddr","netflowipv4srcaddr","netflowl4dstport","netflowl4srcport","netflowlastswitched","netflowpcapfile","netflowprotocol","netflowsrctos","netflowtcpflags" };
 
        csvWriter.writeHeader(header);
 
        for ( NetflowCSV a : l ) {
            csvWriter.write( a, header );
        }
        csvWriter.flush();
        csvWriter.close();
        
//        final File file = new File( csvFileName );
//
//        response.setContentType("text/csv");
//        response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
//        response.setContentLengthLong( file.length() );
//        final BufferedReader br = new BufferedReader(new FileReader(file));
//        try {
//            String line;
//            while ((line = br.readLine()) != null) {
//                response.getWriter().write(line + "\n");
//            }
//        } finally {
//            br.close();
//        }
//        OutputStream out = response.getOutputStream();
//        FileInputStream in = new FileInputStream(file);
//        byte[] buffer = new byte[4096];
//        int length;
//        while ((length = in.read(buffer)) > 0){
//           out.write(buffer, 0, length);
//        }
//        in.close();
//        out.flush();
    }
}

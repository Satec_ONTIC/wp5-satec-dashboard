package com.ontic.Elasticsearch.controller;

import javax.servlet.http.HttpServletRequest;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.PDUv1;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TrapInnecesario {
	
	public class SNMPAgent{
		public static final String community = "public";
		public String ipAddress;
		public static final int port = 162;
		
		public SNMPAgent( String ip ){
			ipAddress = ip;
		}
		
		public void sendTrap_Version1( int strap ) {
		        try {
		            // Create Transport Mapping
		            TransportMapping transport = new DefaultUdpTransportMapping();
		            transport.listen();
		            // Create Target
		            CommunityTarget cTarget = new CommunityTarget();
		            cTarget.setCommunity( new OctetString( community ) );
		            cTarget.setVersion( SnmpConstants.version1 );
		            cTarget.setAddress( new UdpAddress( ipAddress + "/" + port ) );
		            cTarget.setTimeout( 5000 );
		            cTarget.setRetries( 2 );
		            
		            PDUv1 pdu = new PDUv1();
		            pdu.setType( PDU.V1TRAP );
		            pdu.setGenericTrap( PDUv1.ENTERPRISE_SPECIFIC );
		            pdu.setSpecificTrap( strap );
		            pdu.setAgentAddress( new IpAddress( ipAddress ) );
		            // Send the PDU
		            Snmp snmp = new Snmp( transport );
		            snmp.send( pdu, cTarget );
		            snmp.close();
		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		}
	}
	
	private SNMPAgent snmp = new SNMPAgent( "127.0.0.1" );
	
	@RequestMapping( "/trap/{bet}" )
	public void trap( HttpServletRequest request, @PathVariable("bet") int hay ){
		snmp.sendTrap_Version1( 1 );
	}
}

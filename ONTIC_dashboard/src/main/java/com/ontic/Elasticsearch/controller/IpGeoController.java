package com.ontic.Elasticsearch.controller;

import java.io.IOException;
import java.net.InetAddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ontic.Elasticsearch.service.GeoIpService;

@RestController
public class IpGeoController {
	@Autowired
	private GeoIpService service;
	/*
	 * Get last anomalies based on the predefined seconds
	 */
	@RequestMapping("/geoipsrc/{ip}")
	public String webService(@PathVariable("ip") String ip) throws IOException {
		InetAddress ipAddress = InetAddress.getByName(ip);
		return service.geoIp(ipAddress);
	}
	@RequestMapping("/geoipdst/{ip}")
		public String webService1(@PathVariable("ip") String ip) throws IOException {
		InetAddress ipAddress = InetAddress.getByName(ip);
		return service.geoIp(ipAddress);
	}
	@RequestMapping("/geoipano/{ip}")
	public String webService2(@PathVariable("ip") String ip) throws IOException {
		InetAddress ipAddress = InetAddress.getByName(ip);
		return service.geoIp(ipAddress);
	}

}
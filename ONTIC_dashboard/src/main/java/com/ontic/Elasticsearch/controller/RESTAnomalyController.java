package com.ontic.Elasticsearch.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ontic.Elasticsearch.domain.AnomalyQuery;
import com.ontic.Elasticsearch.domain.AverageBytes;
import com.ontic.Elasticsearch.domain.BytesSegundo;
import com.ontic.Elasticsearch.domain.ClosedCount;
import com.ontic.Elasticsearch.domain.TopConv;
import com.ontic.Elasticsearch.domain.TopIp;
import com.ontic.Elasticsearch.domain.TopPorts;
import com.ontic.Elasticsearch.domain.TopTos;
import com.ontic.Elasticsearch.domain.TotalBytes;
import com.ontic.Elasticsearch.domain.TypeAnomalyAggr;
import com.ontic.Elasticsearch.domain.Value;
import com.ontic.Elasticsearch.domain.chusta;
import com.ontic.Elasticsearch.service.AnomalyService;
import com.ontic.Elasticsearch.service.NetflowService;

@RestController
public class RESTAnomalyController {

	@Autowired
	private AnomalyService service;
	@Autowired
	private NetflowService nservice;
	
	@RequestMapping("/newalerts/{bet}")
    public Map<String, Integer> newAlerts(HttpServletRequest request, @PathVariable("bet") String between){
		String tmp[] = between.split("X");
		String xmlSource=(String) request.getSession().getAttribute( "sourceAnomalies" );
		if (xmlSource==null) {
			xmlSource = "frenchxml";
			request.getSession().setAttribute( "sourceAnomalies", xmlSource );
		}
		Object dissim= request.getSession().getAttribute("importance");
		///////////////////////////////////////////////////////////////
		request.getSession().setAttribute( "numberalerts", 0 );
		////////////////////////////////////////////////////////////////
		SimpleDateFormat dt = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
		try {
			request.getSession().setAttribute( "lastAnomalySeen", dt.parse( tmp[1] ));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Map<String, Set<chusta>> l = service.getLastAnomaliesTable( request, xmlSource, tmp, dissim );
		Map<String,Integer> res = new TreeMap<String,Integer>();
		for( Map.Entry<String, Set<chusta>> entry : l.entrySet() ){
			for( chusta c : entry.getValue() ){
				String key = c.getDate();
				if( res.containsKey(key) ){
					int val = res.get(key);
					res.put( key, ++val );
				}else{
					res.put( key, 1 );
				}
			}
		}
		return res;
    }
	
	@RequestMapping("/numberalerts")
    public int getLastaAnomalyseen(HttpServletRequest request) {
		int numberAlerts;
		try{
			numberAlerts = (int)request.getSession().getAttribute("numberalerts");
		}catch (Exception e){
		return 0;
		}
		return numberAlerts;	
    }
	
	@RequestMapping("/anomalies4table/{bet}")
    public Map<String, Set<chusta>> lastAnomaliesTable( HttpServletRequest request, @PathVariable("bet") String between ){
		String tmp[] = between.split("X");
		String xmlSource=(String) request.getSession().getAttribute( "sourceAnomalies" );
		if (xmlSource==null) {
			xmlSource = "frenchxml";
			request.getSession().setAttribute( "sourceAnomalies", xmlSource );
		}
		Object dissim= request.getSession().getAttribute("importance");
		///////////////////////////////////////////////////////////////
		request.getSession().setAttribute( "numberalerts", 0 );
		////////////////////////////////////////////////////////////////
		SimpleDateFormat dt = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
		try {
			request.getSession().setAttribute( "lastAnomalySeen", dt.parse( tmp[1] ));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		/////////////////////////////////////////////////////////////////////
		Map<String, Set<chusta>> l = service.getLastAnomaliesTable( request, xmlSource, tmp, dissim );
		return l;
    }
	@RequestMapping("/typesdeanos/{bet}")
    public List<List<TypeAnomalyAggr>> query_jodona( HttpServletRequest request, @PathVariable("bet") String between ) {
		String tmp[] = between.split("X");
		String xmlSource=(String) request.getSession().getAttribute("sourceAnomalies");
		if (xmlSource==null) {
			xmlSource = "frenchxml";
			request.getSession().setAttribute("sourceAnomalies", xmlSource);
		}
		Object dissim= request.getSession().getAttribute("importance");
		List<List<TypeAnomalyAggr>> l = service.getTypesAnomalies(xmlSource, tmp, dissim );
		return l;
    }
	/*
	 * Get last anomalies based on the predefined seconds
	 */
	@RequestMapping("/anomalies/{bet}")
    public List<AnomalyQuery> lastAnomalies(HttpServletRequest request, @PathVariable("bet") String between ) {
		String tmp[] = between.split("X");
		String xmlSource=(String) request.getSession().getAttribute("sourceAnomalies");
		if (xmlSource==null) {
			xmlSource = "frenchxml";
			request.getSession().setAttribute("sourceAnomalies", xmlSource);
		}
		Object dissim= request.getSession().getAttribute("importance");
		List<AnomalyQuery> l = service.getLastAnomalies(xmlSource, tmp, dissim );
		return l;
    }
	
	/*
	 * Get the anomaly with id
	 */
	@RequestMapping("/anomaly/{id}")
    public List<AnomalyQuery> webService(HttpServletRequest request, @PathVariable("id") String Id) {
		String tmp[] = Id.split("X");
		String xmlSource=(String) request.getSession().getAttribute( "sourceAnomalies" );
		if (xmlSource==null) {
			xmlSource = "frenchxml";
			request.getSession().setAttribute("sourceAnomalies", xmlSource);
		}
		List<AnomalyQuery> l = service.getAnomaly(xmlSource, tmp );
		return l;
    }
	
	/*
	 * Check if any anomaly exists in the last sec miliseconds
	 */
	@RequestMapping("/check_new_anomalies/{bet}")
    public boolean checkRecentAnomalies(HttpServletRequest request, @PathVariable("bet") String between) {
		String tmp[] = between.split("X");
		String xmlSource=(String) request.getSession().getAttribute("sourceAnomalies");
		if (xmlSource==null) {
			xmlSource = "frenchxml";
			request.getSession().setAttribute("sourceAnomalies", xmlSource);
		}
		Object dissim= request.getSession().getAttribute("importance");
		List<AnomalyQuery> l = service.getLastAnomalies(xmlSource, tmp, dissim );
		return !(l.isEmpty());
    }
	
	/*
	 * Get the average in bytes per second between a given period
	 */
	@RequestMapping("/bytespersec/{between}")
    public List<AverageBytes> getBytes(HttpServletRequest request,@PathVariable("between") String between) {
		String tmp[] = between.split("X");

		return nservice.getNetflowsBetween( tmp, "netflowinbytes" );
    }
	@RequestMapping("/bytestotsec/{between}")
    public List<TotalBytes> getTotBytes(HttpServletRequest request,@PathVariable("between") String between) {
		String tmp[] = between.split("X");
		return nservice.getTotalsBetween( tmp, "netflowinbytes" );
    }
	
	/*
	 * Get the average in bytes per second between a given period
	 */
	@RequestMapping("/packetspersec/{opts}")
    public List<AverageBytes> getPackets(HttpServletRequest request,@PathVariable("opts") String opts) {
		String tmp[] = opts.split("X");
		return nservice.getNetflowsBetween( tmp, "netflowinpkts" );
    }
	@RequestMapping("/packetstotsec/{opts}")
    public List<TotalBytes> getTotPackets(HttpServletRequest request,@PathVariable("opts") String opts) {
		String tmp[] = opts.split("X");
		return nservice.getTotalsBetween( tmp, "netflowinpkts" );
    }
	/*
	 * Get the top 5 type of service
	 */
	@RequestMapping("/toptos/{opts}")
    public List<TopTos> getTopTos(HttpServletRequest request,@PathVariable("opts") String opts) {
		String tmp[] = opts.split("X");
		return nservice.getTos( tmp, "netflowsrctos" );
    }
	
	/*
	 * Get the top 5 protocols
	 */
	@RequestMapping("/topprot/{opts}")
    public List<TopTos> getTopProtocols(HttpServletRequest request,@PathVariable("opts") String opts) {
		String tmp[] = opts.split("X");
		return nservice.getTos( tmp, "netflowprotocol" );
    }
	
	/*
	 * Get the top 5 source ports
	 */
	@RequestMapping("/topsrcports/{opts}")
    public List<TopPorts> getTopSrcPorts(HttpServletRequest request,@PathVariable("opts") String opts) {
		String tmp[] = opts.split("X");
		return nservice.getPorts( tmp, "netflowl4srcport" );
    }
	
	/*
	 * Get the top 5 destination ports
	 */
	@RequestMapping("/topdstports/{opts}")
    public List<TopPorts> getTopDstPorts(HttpServletRequest request,@PathVariable("opts") String opts) {
		String tmp[] = opts.split("X");
		return nservice.getPorts( tmp, "netflowl4dstport" );
    }
	
	/*
	 * Get the top 5 destination ip
	 */
	@RequestMapping("/topdstip/{opts}")
    public List<TopIp> getTopDstIp(HttpServletRequest request,@PathVariable("opts") String opts) {
		String tmp[] = opts.split("X");
		return nservice.getIp( tmp, "netflowipv4dstaddr" );
    }
	/*
	 * Get the top 5 source ip
	 */
	@RequestMapping("/topsrcip/{opts}")
    public List<TopIp> getTopSrcIp(HttpServletRequest request,@PathVariable("opts") String opts) {
		String tmp[] = opts.split("X");
		return nservice.getIp( tmp, "netflowipv4srcaddr" );
    }
	@RequestMapping("/topconv/{opts}")
    public List<TopConv> getConversations(HttpServletRequest request,@PathVariable("opts") String opts) {
		String tmp[] = opts.split("X");
		return nservice.getConv( tmp, "conversation", 5 );
    }
	@RequestMapping("/avgbytes/{opts}")
    public List<Value> getAvgBytes(HttpServletRequest request,@PathVariable("opts") String opts) {
		String tmp[] = opts.split("X");
		
		return nservice.getAvg( tmp, "netflowinbytes" );
    }
	@RequestMapping("/sumbytes/{opts}")
    public List<Value> getSumBytes(HttpServletRequest request,@PathVariable("opts") String opts) {
		String tmp[] = opts.split("X");
		 
		return nservice.getSum( tmp, "netflowinbytes" );
    }
	@RequestMapping("/avgpackets/{opts}")
    public List<Value> getAvgPackets(HttpServletRequest request,@PathVariable("opts") String opts) {
		String tmp[] = opts.split("X");
		 
		return nservice.getAvg( tmp, "netflowinpkts" );
    }
	@RequestMapping("/sumpackets/{opts}")
    public List<Value> getSumPackets(HttpServletRequest request,@PathVariable("opts") String opts) {
		String tmp[] = opts.split("X");
		 
		return nservice.getSum( tmp, "netflowinpkts" );
    }
	@RequestMapping("/topflow/{opts}")
    public List<TopConv> getFlows(HttpServletRequest request,@PathVariable("opts") String opts) {
		String tmp[] = opts.split("X");
		 
		return nservice.getConv( tmp, "flow", 5 );
    }
	@RequestMapping("/closedpersec/{between}")
    public List<ClosedCount> getClosed(HttpServletRequest request,@PathVariable("between") String between) {
		String tmp[] = between.split("X");
		 
		return nservice.getClosedNetflowsBetween( tmp);
    }
    @RequestMapping("/bytesporsegundo/{between}")
    public BytesSegundo getAlgoguay(HttpServletRequest request,@PathVariable("between") String between) {
		String tmp[] = between.split("X");
		BytesSegundo l = nservice.getBaby( tmp );
		return l;
    }
}

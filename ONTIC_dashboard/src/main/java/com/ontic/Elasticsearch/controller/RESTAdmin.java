package com.ontic.Elasticsearch.controller;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ontic.Elasticsearch.config.GetPropertyValues;
import com.ontic.Elasticsearch.service.NetflowService;

@RestController
public class RESTAdmin {
	static protected GetPropertyValues prop = new GetPropertyValues();
	String sourceAnomalies = new String();
	@Autowired
	private NetflowService nservice;
	
	@RequestMapping("/setsourceanomalies/{db}")
	public void setAnomaliesSource( HttpServletRequest request, @PathVariable("db") String db ) {
		request.getSession().setAttribute( "lastAnomalySeen", null );
		request.getSession().setAttribute( "drew", null );
		request.getSession().setAttribute( "numberalerts", 0 );
		request.getSession().setAttribute( "sourceAnomalies", "frenchxml" );
		request.getSession().setAttribute( "importance", null );
	}
	
	@RequestMapping("/getsourceanomalies")
	public String getSourceAnomalies( HttpServletRequest request ) {
		try {
			String src = (String) request.getSession().getAttribute( "sourceAnomalies" );
			if( src==null || src.equals("") )
				src="frenchxml";
			return src;
		} catch (java.lang.NullPointerException e) {
			return "0X0X0X0X0X0X0Xstop";
		}
	}
	
	@RequestMapping("/getimportance")
	public List<Map<String, Integer>> getImportance( HttpServletRequest request ) {
		String src = (String) request.getSession().getAttribute( "sourceAnomalies" );
		if( src==null || src.equals("") )
			src="frenchxml";
		Object imp = request.getSession().getAttribute( "importance" );
		TreeMap<String, Integer> map = (TreeMap<String, Integer>)nservice.getImportances( src );
		Map<String, Integer> lev = new TreeMap<String, Integer>();
		String key="all";
		int value=0;
		if( imp==null){
			value=map.get( key );
		}else{
			if( map.containsKey((String)imp) ){
				key=(String)imp;
				value=map.get(key);
			}else{
				System.out.println("importance is wrong and shouldnt happen");
			}
		}
		lev.put( key, value );
		List<Map<String, Integer>> list = new ArrayList<Map<String, Integer>>();
		list.add( map );
		list.add( lev );
		return list;
	}
	
	@RequestMapping("/setimportance/{imp}")
	public void setImportance( HttpServletRequest request,  @PathVariable("imp") String level ){
		request.getSession().setAttribute( "importance", level );
	}
	
	@RequestMapping(value = "/listdirectory", produces = "application/json")
	public List<String> listDirectory () throws IOException{
		List<String> list = new ArrayList<String>();
		List<String> l = new ArrayList<String>();
		l = nservice.getFileName();
		try {
			String pathPcaps = prop.getPropValue( "pathPcaps" );
			File f = new File(pathPcaps);
			File[] files = f.listFiles();
			for (File file : files) {
				if(file.isDirectory() && !l.contains( file.getName() ) )
					list.add( file.getName() );
			}
			return list;
		}catch (Exception e){
			list.add("ERROR");
			return list;
		}
	}
	
	@RequestMapping(value = "/dinamicPcapLoaded", produces = "application/json")
	public List<String> dinamicPcapLoaded() throws IOException{
		return nservice.getFileName();
	}
	
	@RequestMapping(value = "/add/{pcap}")
	public void add(@PathVariable("pcap") String pcap) throws IOException {
		//ejecutar fe.sh
		String pathxml = prop.getPropValue( "simplexmldirectory" );
		String pathxmlf = prop.getPropValue( "fullxmldirectory" );
		String[] cmmm= { "bash", "-c", "rm "+ pathxml+"/* "+ pathxmlf+"/*" };
		exec_command( cmmm );

		String pathPcaps = prop.getPropValue( "pathPcaps" );
		String[] cmmm2= { "bash", "-c", "cp "+ pathPcaps+"/"+pcap+"/spanishxml/* "+ pathxmlf+"/" };
		exec_command( cmmm2 );

		String[] cmmm3= { "bash", "-c", "cp "+ pathPcaps+"/"+pcap+"/frenchxml/* "+ pathxml+"/" };
		exec_command( cmmm3 );

		String port = prop.getPropValue( "netflowport" );
//		String[] comm ={ pathPcaps+"/flowexport_linux_x86_64.bin", "-f", pathPcaps+"/"+pcap+"/"+pcap+".pcap", "-nf5", "127.0.0.1", port, "-a", "5s", "-e", "15s" };
		String[] comm ={ pathPcaps+"/flowexport_linux_x86_64.bin", "-f", pathPcaps+"/"+pcap+"/"+pcap+".pcap", "-nf5", "akkarchitecture", port, "-a", "5s", "-e", "15s" };
		String ex=exec_command( comm );
		if( ex!=null ){
			String port_for_name = prop.getPropValue( "filenameport" );
			String[] comm2={ "bash", "-c", "echo "+pcap+" > /dev/udp/localhost/"+port_for_name };
			exec_command( comm2 );
		}
		/*
		 * añadir anomalias
		 */
   }
	@RequestMapping(value = "/erase")
	public void erase() throws IOException {
		String hostport = prop.getPropValue( "elasticsearch" );
		String index = prop.getPropValue( "index" );
		String[] comm ={ "curl","-XDELETE", hostport+"/"+index };
		exec_command( comm );
		/*
		 * añadir port csv
		 */
		String pathPcaps = prop.getPropValue( "pathPcaps" );
		String pathcsv = prop.getPropValue( "csvdirectory" );
		String[] cmmm2= { "bash", "-c", "rm "+ pathcsv+"/* " };
		exec_command( cmmm2 );
		String[] cmmm3= { "bash", "-c", "cp "+ pathPcaps+"/service-names-port-numbers.csv "+ pathcsv+"/" };
		exec_command( cmmm3 );
   }
	private String exec_command( String[] comm ){
		Process process=null;
		try {
			process = Runtime.getRuntime().exec( comm );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		InputStream inputstream = process.getInputStream();
		BufferedInputStream bufferedinputstream = new BufferedInputStream(inputstream);
		BufferedReader r = new BufferedReader(new InputStreamReader(bufferedinputstream, StandardCharsets.UTF_8));
		String line="";
		try {
			line = r.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return line;
	}
}

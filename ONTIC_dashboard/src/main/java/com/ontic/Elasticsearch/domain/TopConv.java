package com.ontic.Elasticsearch.domain;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Hashtable;

import org.json.JSONException;
import org.json.JSONObject;

public class TopConv {

	private String[] key;
	private long doc_count;
	private static Hashtable<String, String> hashIsp = null;
	private static Hashtable<String, String> hashCountry = null;
	private String srcPortTraslated="";
	private String dstPortTraslated="";
	
	public TopConv(){
		
	}
	public TopConv( Hashtable<String, String> hashI, Hashtable<String, String> hashC ) {
		if( TopConv.hashIsp==null )
			TopConv.hashIsp = hashI;
		if( TopConv.hashCountry==null )
			TopConv.hashCountry = hashC;
	}
	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ( ( cp = rd.read() ) != -1 ) {
			sb.append( (char) cp );
		}
		return sb.toString();
	}
	private static JSONObject readIpInfo( String ip ) throws IOException, JSONException {
		InputStream is = new URL("http://ipinfo.io/" + ip + "/json").openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			JSONObject json = new JSONObject( readAll( rd ) );
			return json;
		} finally {
			is.close();
		}
	}
	public void setIsps() throws JSONException, IOException {
		String auxIsp = null;
//		String auxCountry = null;
		for(int i=0; i<2; i++){
			if ( hashIsp.containsKey( key[i] ) == false ) {
				JSONObject aux = readIpInfo( key[i] );
				auxIsp = aux.getString("org");
//				auxCountry = aux.getString("country");
				hashIsp.put( key[i], auxIsp );
//				hashCountry.put( key[i], auxCountry );
			}
		}
	}
	public void setTranslatedPorts( String srcPortTraslated, String dstPortTraslated ){
		this.srcPortTraslated=srcPortTraslated;
		this.dstPortTraslated=dstPortTraslated;
	}
	public String getIspSrc() {
		return hashIsp.get( key[0] );
	}

	public String getIspDst() {
		return hashIsp.get( key[1] );
	}

	public String getCountrySrc() {
		return hashCountry.get( key[0] );
	}

	public String getCountryDst() {
		return hashCountry.get( key[1] );
	}
	public void setKey(String k){
		key = k.split("_");
	}
	
	public String getSrc( ){
		return key[0];
	}
	public String getDst( ){
		return key[1];
	}
	public String getSrcPort( ){
		if(key.length > 2)
			return key[2];
		else return "";
	}
	public String getDstPort( ){
		if(key.length > 2)
			return key[3];
		else return "";
	}
	public String getProt( ){
		if(key.length > 2)
			return key[4];
		else return "";
	}
	public void setDoc_count( long k ){
		doc_count = k;
	}
	public long getCount( ){
		return doc_count;
	}
	public String getSrcPortTranslated() {
		return this.srcPortTraslated;

	}

	public String getDstPortTranslated() {
		return this.dstPortTraslated;
	}
}

package com.ontic.Elasticsearch.domain;

public class BytesSegundo {
	private Double bytessec;
	public BytesSegundo( Double b ){
		bytessec = b;
	}
	public Double getBytes(){
		Double number = 8*(bytessec/1024/1024);
		number = (double) Math.round(number * 100);
		number = number/100;
		return number;
	}
}

package com.ontic.Elasticsearch.domain;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/*
 * Class for query Average bytes per second
 */
public class AverageBytes{
	
	public class AverageValue{
		private long value;
		public void setValue(long v){
			value=v;
		}
		public long getValue(){
			return value;
		}
	}
	
	private Date key_as_string;
	private long doc_count;
	private AverageValue average;
	private long key;
	
	public void setKey(long k){
		key = k;
	}
	
	public void setKey_as_string(Date k){
		key_as_string = k;
	}
	public String getKey_as_string(){
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");	
		dt1.setTimeZone(TimeZone.getTimeZone("GMT"));;
		return dt1.format(key_as_string);
	}
	
	public void setDoc_count(long k){
		doc_count = k;
	}
	
	public void setAverage(AverageValue k){
		average = k;
	}
	public long getAverage(){
		return average.getValue();
	}
}

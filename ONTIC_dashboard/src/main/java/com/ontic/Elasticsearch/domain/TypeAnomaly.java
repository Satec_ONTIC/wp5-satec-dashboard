package com.ontic.Elasticsearch.domain;


public class TypeAnomaly {
	private class Start{
		private int doc_count_error_upper_bound;
		private int sum_other_doc_count;
		private TimeSeries[] buckets;
		public Start(){
			
		}
		public void SetDoc_count_error_upper_bound( int d ){
			this.doc_count_error_upper_bound=d;
		}
		public void setSum_other_doc_count( int s ){
			this.sum_other_doc_count = s;
		}
		public void setBuckets( TimeSeries[] t ){
			this.buckets = t;
		}
		public TimeSeries[] getBuckets(){
			return this.buckets;
		}
	}

	private String key;
	private int doc_count;
	private Start start;
	public TypeAnomaly(){
		
	}
	public void setKey( String k ){
		this.key=k;
	}
	public void setDoc_count( int d ){
		this.doc_count=d;
	}
	public void setStart( Start s){
		this.start=s;
	}
	//////////////////////////////////////////
	public String getKey(){
		return this.key;
	}
	public TimeSeries[] getStart(){
		return start.getBuckets();
	}
	//////////////////////////////////////////
}

package com.ontic.Elasticsearch.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

/*
 * Class for query to get anomaly documents
 */
public class AnomalyQuery {
	
	public class Anomaly{
		private Date start;
		private Date end;
		private long important;
		private long[] srcports={};
		private String typeanomaly;
		private String[] dstpoints={};
		private long id;
		private String[] protocols={};
		private String[] srcpoints={};
		private long[] dstports={};
	
		
		public void setSrcports (long[] srcports){
			this.srcports=srcports;
		}
		public long[] getSrcports (){
			return srcports;
		}
		
		public void setDstports (long[] dstports){
			this.dstports=dstports;
		}
		public long[] getDstports (){
			return dstports;
		}
		
		public void setImportant (long important){
			this.important=important;
		}
		public long getImportant (){
			return important;
		}
		
		public void setTypeanomaly (String typeanomaly){
			this.typeanomaly=typeanomaly;
		}
		public String getTypeanomaly (){
			return typeanomaly;
		}
		
		public void setSrcpoints (String[] srcpoints){
			this.srcpoints=srcpoints;
		}
		public String[] getSrcpoints (){
			return srcpoints;
		}
		
		public void setDstpoints (String[] dstpoints){
			this.dstpoints=dstpoints;
		}
		public String[] getDstpoints (){
			return dstpoints;
		}
		
		public void setId (long id){
			this.id=id;
		}
		public long getId (){
			return id;
		}
		public void setProtocols (String[] protocols){
			this.protocols=protocols;
		}
		public String[] getProtocols (){
			return protocols;
		}

		public Date getStart(){
			return start;
		}
		public void setStart(Date time){
			this.start=time;
		}
		public Date getEnd(){
			return end;
		}
		public void setEnd(Date time){
			this.end=time;
		}
	}
	
	
		private String _id;
		private String _index;
		private String _type;
		private String _score;
		private long[] sort;
		private Anomaly _source;
		
		
		public void set_source(Anomaly a){
			_source = a;
		}
		
		public String[] getSrcpoints (){
			return _source.srcpoints;
		}
		public String getTypeanomaly (){
			return _source.typeanomaly;
		}
		public String[] getDstpoints (){
			return _source.dstpoints;
		}
		
		public long getImportant(){
			return _source.important;
		}
		
		public void setSort(long[] s){
			sort = s;
		}

		public void set_index(String i){
			_index = i;
		}

		public void set_score(String i){
			_score = i;
		}

		public void set_type(String i){
			_type = i;
		}

		public Long getId(){
			return _source.id;
		}
		public void set_id(String id){
			this._id=id;
		}
		public String getStart(){
			SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			return dt1.format(_source.getStart());
		}
		public String getEnd(){
			SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			return dt1.format(_source.getEnd());
		}
}


package com.ontic.Elasticsearch.domain;

public class chusta implements Comparable{

	private String[] src;
	private String typo;
	private String start;
	private String end;
	private String[] dst;
	
	public chusta( String[] sr, String t, String s, String e, String[] d ){
		src = sr;
		typo=t;
		start=s;
		end = e;
		dst = d;
	}
	
	public String[] getSrc(){
		return src;
	}
	
	public String getType(){
		return typo;
	}
	
	public String getDate(){
		return start;
	}
	public String getDate2(){
		return end;
	}
	
	public String[] getDst(){
		return dst;
	}

	@Override
	public int compareTo(Object o) {
		chusta c = (chusta)o;
		return start.compareTo( c.getDate() );
	}
}

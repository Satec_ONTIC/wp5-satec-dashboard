package com.ontic.Elasticsearch.domain;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.ontic.Elasticsearch.service.GeoIpService;

public class AnimatedTable {
	
		private int id;
		private String Type_anomaly;
		private Date Start_Time;
		private String Important;
		private InetAddress [] ip_Src;
		private InetAddress [] ip_Dst;
		private String  Country_Src	;
		private String  Country_Dst	;
		private boolean news;
	
		private static GeoIpService geoip = new GeoIpService();
		
		public String getCountry_Src (){
			try {
				for(InetAddress a : ip_Src){
					if (Country_Src==null) Country_Src = geoip.geoIpOnlyCountry(a);
					else if (geoip.geoIpOnlyCountry(a)==Country_Src) {}
					else {
						Country_Src = "Multiple sources";
						return Country_Src;
					}
				}
				return Country_Src;
			} catch (IOException e) {
				e.printStackTrace();
			}
			Country_Src = "Unknown";
			return Country_Src;	
		}
		
		public String getCountry_Dst (){
			try {
				for(InetAddress a : ip_Dst){
					if (Country_Dst==null) Country_Dst = geoip.geoIpOnlyCountry(a);
					else if (geoip.geoIpOnlyCountry(a)==Country_Dst) {}
					else {
						Country_Dst = "Multiple destinations";
						return Country_Dst;
					}
				}
				return Country_Dst;
			} catch (IOException e) {
				e.printStackTrace();
			}
			return "Unknown";		
		}		
		
		public void setid(int id){
			this.id = id;
		}
		
		public int getid (){
			return id;
		}
		
		
		public void setType_anomaly(String Type_anomaly){
			this.Type_anomaly = Type_anomaly;
		}
		
		public String getType_anomaly (){
			return Type_anomaly;
		}
		
		public void setImportant(String Important){
			this.Important = Important;
		}
		
		public String getImportant (){
			return Important;
		}
		
		public Date getStart_Time (){
			return Start_Time;
		}
		
		public void setStart_Time(HttpServletRequest request, Date Start_Time){
			if (request.getSession().getAttribute("lastAnomalySeen")==null){
				request.getSession().setAttribute("lastAnomalySeen", Start_Time);
			}
			if (Start_Time.after((Date)request.getSession().getAttribute("lastAnomalySeen"))){
				request.getSession().setAttribute("lastAnomalySeen", Start_Time);
				this.setNews(true);
			} else this.setNews(false);

			this.Start_Time = Start_Time;
		}
		
		public InetAddress[] getip_Src (){
			return ip_Src;
		}
		public InetAddress[] getip_Dst (){
			return ip_Dst;
		}
		
		public void setip_Src (InetAddress[] ip_Src){
			this.ip_Src=ip_Src;
		}
		
		public void setip_Dst (InetAddress [] ip_Dst){
			this.ip_Dst=ip_Dst;
		}

		public boolean getNews() {
			return news;
		}

		private void setNews(boolean news) {
			this.news = news;
		}


}


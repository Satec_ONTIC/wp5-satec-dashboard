package com.ontic.Elasticsearch.domain;

public class Timepicker {
	private String start;
	private String end;
	private String current;
	private String speed;
	private String state;


	public Timepicker() {
		this.start = "0";
		this.end = "0";
		this.current = "0";
		this.speed = "X1";
		this.state = "STOP";
	}
	
	public void setPicker( String start, String end, String curr, String speed, String st ){
		this.start = start;
		this.end = end;
		this.current = curr;
		this.speed = speed;
		this.state = st;
	}

	public String getPicker (){
		String picker = this.start + "X" + this.end+ "X" + this.current + "X" + this.speed + "X" + this.state;
		return picker;
	}
	
	public int getMode (){
		return 0;
	}
}

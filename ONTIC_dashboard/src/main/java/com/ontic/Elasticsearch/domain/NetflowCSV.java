package com.ontic.Elasticsearch.domain;

import java.util.Date;

public class NetflowCSV {

	private String _index;
	private String _type;
	private String _id;
	private Double _score;
	private NetflowRecord _source;
	
	public void set_index( String in){
		_index = in;
	}
	public void set_type( String in){
		_type = in;
	}
	public void set_id( String in){
		_id = in;
	}
	public void set_score( Double in){
		_score = in;
	}
	public void set_source( NetflowRecord n){
		_source = n;
	}
	public String getConversation(){
		return _source.conversation;
	}
	public String getFlow(){
		return _source.flow;
	}
	public Date getNetflowfirstswitched(){
		return _source.netflowfirstswitched;
	}
	public long getNetflowinbytes(){
		return _source.netflowinbytes;
	}
	public long getNetflowinpkts(){
		return _source.netflowinpkts;
	}
	public String getNetflowipv4dstaddr(){
		return _source.netflowipv4dstaddr;
	}
	public String getNetflowipv4srcaddr(){
		return _source.netflowipv4srcaddr;
	}
	public long getNetflowl4dstport(){
		return _source.netflowl4dstport;
	}
	public long getNetflowl4srcport(){
		return _source.netflowl4srcport;
	}
	public Date getNetflowlastswitched(){
		return _source.netflowlastswitched;
	}
	public String getNetflowpcapfile(){
		return _source.netflowpcapfile;
	}
	public long getNetflowprotocol(){
		return _source.netflowprotocol;
	}
	public long getNetflowsrctos(){
		return _source.netflowsrctos;
	}
	public long getNetflowtcpflags(){
		return _source.netflowtcpflags;
	}
}

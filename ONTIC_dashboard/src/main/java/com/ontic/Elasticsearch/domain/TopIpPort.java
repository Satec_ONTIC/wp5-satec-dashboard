package com.ontic.Elasticsearch.domain;

/*
 * Class for queries with double aggregations
 */
public class TopIpPort {
	
	public class Group_by_port{
		private long doc_count_error_upper_bound;
		private long sum_other_doc_count;
		private TopTos[] buckets;
		
		public void setDoc_count_error_upper_bound( long s ){
			doc_count_error_upper_bound = s;
		}
		public void setSum_other_doc_count( long s ){
			sum_other_doc_count = s;
		}
		public void setBuckets( TopTos[] s ){
			buckets = s;
		}
		public TopTos[] getBuckets(){
			return buckets;
		}
		public void addIdx( ){
			for( int i=0 ; i<buckets.length ; i++){
				buckets[ i ].addIdx( i+1 );
			}
		}
	}
	
	private long doc_count;
	private String key;
	public long count1;
	public long count2;
	public long count3;
	public long count4;
	public long count5;
	private Group_by_port group_by_port;
	
	public void addIdx( int i ){
		count1 = count2 = count3 = count4 = count5 = 0;
		switch( i ){
		case 1 :
			count1 = doc_count;
			break;
		case 2:
			count2 = doc_count;
			break;
		case 3:
			count3 = doc_count;
			break;
		case 4:
			count4 = doc_count;
			break;
		case 5:
			count5 = doc_count;
			break;
		default:
			System.err.println("property index in TopIp must be [ 1,...,5 ]");
			break;
		}
		
		group_by_port.addIdx( );
	}
	
	public void setKey(String k){
		key = k;
	}
	
	public String getKey(){
		return key;
	}
	
	public void setDoc_count(long k){
		doc_count = k;
	}
	
	public void setGroup_by_port( Group_by_port g ){
		group_by_port = g;
	}
	public TopTos[] getPorts(){
		return group_by_port.getBuckets();
	}
}

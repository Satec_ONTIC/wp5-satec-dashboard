package com.ontic.Elasticsearch.domain;

import java.util.Calendar;
import java.util.Date;

public class NetflowRecord {

	public String conversation;		
	public String flow;		
	public Date netflowfirstswitched;	
	public long netflowinbytes;		
	public long netflowinpkts;		
	public String netflowipv4dstaddr;		
	public String netflowipv4srcaddr;		
	public long netflowl4dstport;		
	public long netflowl4srcport;		
	public Date netflowlastswitched;	
	public String netflowpcapfile;		
	public long netflowprotocol;		
	public long netflowsrctos;		
	public long netflowtcpflags;
	
	public void setConversation( String c ){
		conversation=c;
	}
	public void setFlow( String f ){
		flow=f;
	}
	public void setNetflowipv4dstaddr( String c ){
		netflowipv4dstaddr=c;
	}
	public void setNetflowipv4srcaddr( String c ){
		netflowipv4srcaddr=c;
	}
	public void setNetflowpcapfile( String c ){
		netflowpcapfile=c;
	}
	public void setNetflowfirstswitched( Date c ){
		netflowfirstswitched=c;
	}
	public void setNetflowlastswitched( Date c ){
		netflowlastswitched=c;
	}
	public void setNetflowinbytes( long c ){
		netflowinbytes=c;
	}
	public void setNetflowinpkts( long c ){
		netflowinpkts=c;
	}
	public void setNetflowl4dstport( long c ){
		netflowl4dstport=c;
	}
	public void setNetflowl4srcport( long c ){
		netflowl4srcport=c;
	}
	public void setNetflowprotocol( long c ){
		netflowprotocol=c;
	}
	public void setNetflowsrctos( long c ){
		netflowsrctos=c;
	}
	public void setNetflowtcpflags( long c ){
		netflowtcpflags=c;
	}
	
	public Date getFirst( ){
		return netflowfirstswitched;
	}
	
	public Date getFirst4db( ){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime( netflowfirstswitched );
		calendar.add( Calendar.HOUR, -2 );
		return calendar.getTime();
	}
}

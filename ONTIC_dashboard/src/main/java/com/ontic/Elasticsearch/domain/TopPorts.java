package com.ontic.Elasticsearch.domain;

public class TopPorts {

	public String key;
	public long count1;
	public long count2;
	public long count3;
	public long count4;
	public long count5;
	
	public TopPorts( String k ){
		this.key = k;
	}
	
	public void setC1(long c){
		count1 = c;
	}
	public void setC2(long c){
		count2 = c;
	}
	public void setC3(long c){
		count3 = c;
	}
	public void setC4(long c){
		count4 = c;
	}
	public void setC5(long c){
		count5 = c;
	}
}

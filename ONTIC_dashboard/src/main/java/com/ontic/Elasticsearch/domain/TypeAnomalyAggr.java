package com.ontic.Elasticsearch.domain;

public class TypeAnomalyAggr {

	private String key;
	private String key_as_string;
	private Long count;
	
	public TypeAnomalyAggr( String k, String t, Long c ){
		this.key = k;
		this.key_as_string = t;
		this.count = c;
	}
	
	public String getKey(){
		return this.key;
	}
	public String getKey_as_string(){
		return this.key_as_string;
	}
	public Long getCount(){
		return this.count;
	}
}

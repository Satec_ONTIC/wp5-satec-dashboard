package com.ontic.Elasticsearch.domain;

/*
 * Class for the top 5 queries. Key is long is the only difference with TopIp class.
 */
public class TopTos {
	private long doc_count;
	private long key;
	public long count1;
	public long count2;
	public long count3;
	public long count4;
	public long count5;
	
	public void setKey(long k){
		key = k;
	}
	
	public long getKey(){
		return key;
	}
	
	public void setDoc_count(long k){
		doc_count = k;
	}
	
	public void addIdx( int i ){
		count1 = count2 = count3 = count4 = count5 = 0;
		switch( i ){
		case 1 :
			count1 = doc_count;
			break;
		case 2:
			count2 = doc_count;
			break;
		case 3:
			count3 = doc_count;
			break;
		case 4:
			count4 = doc_count;
			break;
		case 5:
			count5 = doc_count;
			break;
		default:
			System.err.println("property index in TopIp must be [ 1,...,5 ]");
			break;
		}
	}
}

package com.ontic.Elasticsearch.domain;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeSeries {

	private long key;
	private Date key_as_string;
	private int doc_count;
	
	public TimeSeries(){
		
	}
	public void setKey( long k ){
		this.key = k;
	}
	public void setKey_as_string( Date k ){
		this.key_as_string = k;
	}
	public void setDoc_count( int d ){
		this.doc_count = d;
	}
	
	public String getKey_as_string(){
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
		return dt1.format( this.key_as_string );
	}
	
	public int getCount(){
		return this.doc_count;
	}
}

package com.ontic.Elasticsearch.domain;

public class AnomalyCSV {

		private AnomalyQuery ano;
		
		public AnomalyCSV( AnomalyQuery an ){
			ano = an;
		}
		
		public String getSrcpoints (){
			return ano.getSrcpoints()[0];
		}
		public String getTypeanomaly (){
			return ano.getTypeanomaly();
		}
		public String getDstpoints (){
			String allofthem = "";
			for( String dst : ano.getDstpoints() ){
				allofthem+=dst+" ";
			}
			return allofthem;
		}
		
		public long getImportant(){
			return ano.getImportant();
		}
		
		public Long getId(){
			return ano.getId();
		}
	
		public String getStart(){
			return ano.getStart();
		}
		public String getEnd(){
			return ano.getEnd();
		}
}

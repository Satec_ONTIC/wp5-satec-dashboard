/**
 * 
 */
function Player( ){
	this._modes = {
		    REALTIME : 0,
		    DEMO : 1,
		    FORENSIC : 2
	};
	this._states = {
		    PLAY : 0,
		    PAUSE : 1,
		    STOP : 2
	};
	this._speeds = {
		    X1 : 1,
		    X2 : 2,
		    X4 : 4,
		    X8 : 8
	};
	this.mode = "DEMO";
	this._state = "STOP";
	this.speed = "X1";
	this.static_start = true;
	this.interval = null;
	this.start;
	this.end;
	this.drag = false; // player is being dragged
	this._clock = new Clock( this._speeds );
}
Player.prototype.play = function(){
	if( this._state!='PLAY' ){
		if( this._state=="STOP" )
			this._clock.play();
		if( this._state=="PAUSE" )
			this._clock.resume();
		this._state = "PLAY";
		$('.btnPlay').toggleClass( 'paused' );
	}
}
Player.prototype.pause = function(){
	if( this._state=='PLAY' && this.mode!='REALTIME' ){
		this._state = "PAUSE";
		$('.btnPlay').toggleClass( 'paused' );
		this._clock.pause();
	}
}
Player.prototype.stop = function(){
	this._state = "STOP";
	$('.btnPlay').removeClass( 'paused' );
	this._clock.stop();
	this.setSpeed( "X1" );
}
Player.prototype.setSpeed = function( speed ){
	if( this.mode!='REALTIME' ){
		$('.btnx1').removeClass( 'selected' );
		$('.btnx2').removeClass( 'selected' );
		$('.btnx4').removeClass( 'selected' );
		$('.btnx8').removeClass( 'selected' );
		if( speed=="X1" )
			$('.btnx1').addClass( 'selected' );
		if( speed=="X2" )
			$('.btnx2').addClass( 'selected' );
		if( speed=="X4" )
			$('.btnx4').addClass( 'selected' );
		if( speed=="X8" )
			$('.btnx8').addClass( 'selected' );
		this.speed = speed;
		this._clock.setSpeed( speed );
	}
}
Player.prototype.setMode = function( mode ){
	if( this._state=="STOP" ){

			if( $( '.btnRT' ).hasClass('selected') )
				$( '.btnRT' ).removeClass( 'selected' );
			if( $( '.btnDE' ).hasClass('selected') )
				$( '.btnDE' ).removeClass( 'selected' );
			if( $( '.btnFO' ).hasClass('selected') )
				$( '.btnFO' ).removeClass( 'selected' );

		this.mode = mode;
		switch( this.mode ){
		case 'REALTIME':
			$( '.btnRT' ).addClass( 'selected' );
			break;
		case 'DEMO':
			$( '.btnDE' ).addClass( 'selected' );
			break;
		case 'FORENSIC':
			$( '.btnFO' ).addClass( 'selected' );
			break;
		default:
		}
	}
}
Player.prototype.setStatic = function( stat ){
	if( this._state=="STOP" ){
		this.static_start = stat;
		this._clock.setStatic( stat );
	}
}
Player.prototype.setInterval = function( interval ){
	if( this._state=="STOP" && !this.static_start ){
		this.interval = interval;
		this._clock.setInterval( interval );
	}
}
Player.prototype.setStart = function( start ){
	if( this._state=="STOP" ){
		this.start = start;
		this._clock.setStart( start );
	}
}
Player.prototype.setEnd = function( end ){
	if( this._state=="STOP" ){
		this.end = end;
		this._clock.setEnd( end );
	}
}
Player.prototype.moveBar = function( x, drag ){
	if( this.mode!='REALTIME' ){
		this.drag = drag==null ? this.drag : drag;
		
		var progress = $('.progress');
		var position = x - progress.offset().left;
		var percentage = 100 * position / progress.width();
		if(percentage > 100) {
			percentage = 100;
		}
		if(percentage < 0) {
			percentage = 0;
		}
		this._clock.movedBar( percentage );
	}
}
Player.prototype.getDrag = function( ){
	return this.drag;
}
Player.prototype.forceclock = function(){
	this._clock.forceredrawelements();
}
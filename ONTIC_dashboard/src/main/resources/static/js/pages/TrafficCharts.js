function TrafficCharts(){
	this._gauge = new GaugeChart( '#chart9', 380 );
	this._pie1 = new PieChart( '#chart1', 200, 'TOPDSTIPS', false, 'map2' );
	this._pie2 = new PieChart( '#chart2', 200, 'TOPSRCIPS', false, 'map1' );
	this._pie3 = new PieChart( '#chart3', 200, 'TOPDSTPORTS', false, null );
	this._pie4 = new PieChart( '#chart4', 200, 'TOPSRCPORTS', false, null );
	this._pie5 = new PieChart( '#chart5', 200, 'TOPPROT', false, null );
	this._pie6 = new PieChart( '#chart6', 200, 'TOPTOS', false, null );
	this._line1 = new LineChart( '#chart7', 625, ['AVGBYTESEC','TOTBYTESEC'], null );
	this._line2 = new LineChart( '#chart8', 625, ['AVGPCKTSEC','TOTPCKTSEC'], null );
	this._line3 = new LineChart( '#chart11', 800, ['CLOSEDSEC'], null );
}
TrafficCharts.prototype.draw = function( FROM, TO ){
	this._gauge.draw( FROM, TO );
	this._pie1.draw( FROM, TO );
	this._pie2.draw( FROM, TO );
	this._pie3.draw( FROM, TO );
	this._pie4.draw( FROM, TO );
	this._pie5.draw( FROM, TO );
	this._pie6.draw( FROM, TO );
	this._line1.draw( FROM, TO, null, null );
	this._line2.draw( FROM, TO, null, null );
	this._line3.draw( FROM, TO, null, null );
}
TrafficCharts.prototype.clear = function(){
	this._gauge.clear();
	this._pie1.clear( );
	this._pie2.clear( );
	this._pie3.clear( );
	this._pie4.clear( );
	this._pie5.clear();
	this._pie6.clear( );
	this._line1.clear();
	this._line2.clear();
	this._line3.clear();
}
/**
 * 
 */
function DemoMode( data ){
	this._init = function(){
		var i;
		for(i=0; i<data.length; i++ ){
		  var dates = data[i].split( '_' );
		  var year = dates[0].substring( 0,4 );
		  var month = dates[0].substring( 4,6 );
		  var day = dates[0].substring( 6,8 );
		  var hour = dates[0].substring( 8,10 );
		  var min = dates[0].substring( 10,12 );
		  var sec = dates[0].substring( 12,14 );
		  
		  var year1 = dates[1].substring( 0,4 );
		  var month1 = dates[1].substring( 4,6 );
		  var day1 = dates[1].substring( 6,8 );
		  var hour1 = dates[1].substring( 8,10 );
		  var min1 = dates[1].substring( 10,12 );
		  var sec1 = dates[1].substring( 12,14 );
  		  $('#crazy ul').append("<li title='click to play'>"+ year+"-"+month+"-"+day+" "+hour+":"+min+":"+sec +" "+
  				  										year1+"-"+month1+"-"+day1+" "+hour1+":"+min1+":"+sec1+"</li>");
  	  	}
		if( i<3 ){
			for( i; i<3; i++ ){
				$('#crazy ul').append("<li class='noactive'>"+"NO DATA"+"</li>");
			}
		}
		var sly = new Sly("#crazy", {
    		slidee : $('.slidee'),
    		horizontal: 1,
  		    itemNav: 'basic',
  		    smart: 0,
  		    activateOn:'mouseenter',
  		    scrollBy: 1,
  		    mouseDragging: 1,
  		    swingSpeed: 0.2,
  			scrollBar: $('.demoscrollbar'),
  		    dragHandle: 1,
  			clickBar: 1,
  			elasticBounds: 1,
  		    speed: 600,
  		    startAt: 0
    	  }, {
    	  }).init();
		$('#crazy ul li').each( function(){
			if( !$(this).hasClass('noactive') ){
				$( this ).click( function(e){
		  			  var pcap = e.target.textContent;
		  			  $(".videoContainer").html( "<div class='col-md-1 col-md-offset-5 loading'></div>" );
		  			  dashboard.setStart( pcap.substring( 0,19 ) );
		  			  dashboard.setEnd( pcap.substring( 20,39 ) );
		  			  dashboard.setMode( "DEMO" );
		  			  $('.btnPlay').click();
		  			  $('#shortcut').click();
		  		  } );
			}
  	  });
		$('.demobackward i').click( function(){
  		  sly.toStart();
  	  });
  	  $('.demoforward i').click( function(){
  		  sly.toEnd();
  	  });
	};
	this._init();
}
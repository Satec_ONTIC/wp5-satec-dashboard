
function RealtimeMode(){
	this._init = function(){
		var i;
		$('#crazy ul').append("<li title='click to play'>"+5+"</li>");
		for(i=15; i<=60; i+=15 ){
  		  $('#crazy ul').append("<li title='click to play'>"+i+"</li>");
  	  	}
		var sly = new Sly("#crazy", {
    		slidee : $('.slidee'),
    		horizontal: 1,
  		    itemNav: 'basic',
  		    smart: 0,
  		    activateOn:'mouseenter',
  		    scrollBy: 1,
  		    mouseDragging: 1,
  		    swingSpeed: 0.2,
  			scrollBar: $('.rtscrollbar'),
  		    dragHandle: 1,
  			clickBar: 1,
  			elasticBounds: 1,
  		    speed: 600,
  		    startAt: 0
    	  }, {
    	  }).init();
		$('#crazy ul li').each( function(){
			if( !$(this).hasClass('noactive') ){
				$( this ).click( function(e){
		  			  var minutes = e.target.textContent;
		  			  $(".videoContainer").html("<div class='col-md-1 col-md-offset-5 loading'></div>");
		  			  dashboard.setEnd( 'now' );
		  			  dashboard.setStatic( false );
		  			  dashboard.setInterval( minutes*60*1000 );
		  			  dashboard.setMode( "REALTIME" );
		  			  $('.btnPlay').click();
		  			  $('#shortcut').click();
		  		  } );
			}
  	  });
		$('.rtbackward i').click( function(){
  		  sly.toStart();
  	  });
  	  $('.rtforward i').click( function(){
  		  sly.toEnd();
  	  });
	};
	this._init();
}

function ForensicMode( data ){
	this.start;
	this.end;
	this.total;
	this._init = function(){
		var self = this;
		var i;
		for(i=0; i<data.length; i++ ){
			var dates = data[i].split( '_' );
			  var year = dates[0].substring( 0,4 );
			  var month = dates[0].substring( 4,6 );
			  var day = dates[0].substring( 6,8 );
			  var hour = dates[0].substring( 8,10 );
			  var min = dates[0].substring( 10,12 );
			  var sec = dates[0].substring( 12,14 );
			  
			  var year1 = dates[1].substring( 0,4 );
			  var month1 = dates[1].substring( 4,6 );
			  var day1 = dates[1].substring( 6,8 );
			  var hour1 = dates[1].substring( 8,10 );
			  var min1 = dates[1].substring( 10,12 );
			  var sec1 = dates[1].substring( 12,14 );
			  $('#crazy ul').append("<li title='click to play'>"+ year+"-"+month+"-"+day+" "+hour+":"+min+":"+sec +" "+
							year1+"-"+month1+"-"+day1+" "+hour1+":"+min1+":"+sec1+"</li>");
  	  	}
		if( i<3 ){
			for( i; i<3; i++ ){
				$('#crazy ul').append("<li class='noactive'>"+"NO DATA"+"</li>");
			}
		}
		var sly = new Sly("#crazy", {
    		slidee : $('.slidee'),
    		horizontal: 1,
  		    itemNav: 'basic',
  		    smart: 0,
  		    activateOn:'mouseenter',
  		    scrollBy: 1,
  		    mouseDragging: 1,
  		    swingSpeed: 0.2,
  			scrollBar: $('.forensicscrollbar'),
  		    dragHandle: 1,
  			clickBar: 1,
  			elasticBounds: 1,
  		    speed: 600,
  		    startAt: 0
    	  }, {
    	  }).init();
		$('#crazy ul li').each( function(){
			if( !$(this).hasClass('noactive') ){
				$( this ).click( function(e){
		  			  var pcap = e.target.textContent;
		  			  var mindate = pcap.substring( 0,10 ).replace( /-/g, "/" );
		  			  var maxdate = pcap.substring( 20,30 ).replace( /-/g, "/" );
		  			  var mintime = pcap.substring( 11,16 );
		  			  var maxtime = pcap.substring( 31,36 );
		  			  $.ajax({
			    	      type: "GET",
			    	      url: '/forensic2',
			    	      success: function( data2 ) {
			    	    	  $(".videoContainer").html( data2.html );
			    	    	  $('#frompicker').datetimepicker({
			    	    			format : 'Y-m-d H:i:s',
			    	    			step : 1,
			    	    			minDate:mindate,
			    	    			maxDate:maxdate,
			    	    			minTime:mintime,
			    	    			maxTime:maxtime,
			    	    			startDate:mindate,
			    	    			inline:true,
			    	    			theme:'dark',
			    	    			onSelectTime : function(ct, $i) {
			    	    				var v = $('#frompicker').datetimepicker('getValue')[0].value;
			    	    				if (v != "") {
			    	    					$('#topicker').prop( 'disabled', false );
			    	    					$('#topicker').datetimepicker( { inline:true } );
			    	    					self.start = v;
			    	    					dashboard.setStart( v );
			    	    				}
			    	    			}
			    	    		});
			    	    	  $('#topicker').datetimepicker({
			    	    			format : 'Y-m-d H:i:s',
			    	    			step : 1,
			    	    			inline:false,
			    	    			theme:'dark',
			    	    			minDate:mindate,
			    	    			maxDate:maxdate,
			    	    			minTime:mintime,
			    	    			maxTime:maxtime,
			    	    			startDate:mindate,
			    	    			onSelectTime : function(ct, $i) {
			    	    				var v = $('#topicker').datetimepicker('getValue')[0].value;
			    	    				if (v != "") {
			    	    					self.end = v;
			    	    					self.total = (moment( self.end ).toDate().getTime() - moment( self.start ).toDate().getTime())/1000;
				    	    				if( self.total>0 ){
				    	    					dashboard.setEnd( v );
				    	    					$.ajax({
				    	    						type: 'GET',
				    	    						url: '/forensic3',
				    	    						success: function( data3 ){
				    	    							$(".videoContainer").html( data3.html );
				    	    							var half = 50*self.total/100;
				    	    							$('.w3-center').html( Math.round(half)+' seconds' );
				    	    							$('.slideThree input').click(function(e){
				    	    								if( e.target.checked ){
				    	    									$(".w3-progress-container").css( 'display', 'block' );
				    	    									dashboard.setStatic( false );
																dashboard.setInterval( ($('.w3-center').text().split(' seconds')[0])*1000 );
				    	    								}else{
				    	    									$(".w3-progress-container").css( 'display', 'none' );
				    	    									dashboard.setStatic( true );
				    	    								}
				    	    							});
				    	    							$('.w3-progress-container').click(function(e){
				    	    								var progress = $('.w3-progress-container');
				    	    								var position = e.pageX - progress.offset().left;
				    	    								var percentage = 100 * position / progress.width();
				    	    								if(percentage > 100) {
				    	    									percentage = 100;
				    	    								}
				    	    								if(percentage < 0) {
				    	    									percentage = 0;
				    	    								}
				    	    								$('.w3-progressbar').css( 'width', percentage+'%' );
				    	    								var seconds = percentage*self.total/100;
				    	    								$('.w3-center').html( Math.round(seconds)+' seconds' );
				    	    								dashboard.setInterval( Math.round(seconds)*1000 );
				    	    							});
				    	    							$('#play button').click(function(){
				    	    								  dashboard.setMode( "FORENSIC" );
				    	    					  			  $('.btnPlay').click();
				    	    					  			  $('#shortcut').click();
				    	    							});
				    	    						},
				    	    						dataType: 'json'
				    	    					});
				    	    					$(".videoContainer").html("<div class='col-md-1 col-md-offset-5 loading'></div>");
				    	    				}
			    	    				}
			    	    			},
			    	    			onGenerate: function(){
			    	    				$('#topicker').prop( 'disabled', true );
			    	    			}
			    	    		});
			    	      },
			    	      dataType: 'json'
			    	  });
		  			  $(".videoContainer").html("<div class='col-md-1 col-md-offset-5 loading'></div>");
		  		  } );
			}
  	  });
		$('.forensicbackward i').click( function(){
  		  sly.toStart();
  	  });
  	  $('.forensicforward i').click( function(){
  		  sly.toEnd();
  	  });
	};
	this._init();
}
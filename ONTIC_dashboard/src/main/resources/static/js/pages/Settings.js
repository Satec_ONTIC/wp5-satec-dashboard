function Settings(){
	this._init = function(){
		$.get("/getsourceanomalies", function( sourceanomalies ){
	   		 if (sourceanomalies == "" || sourceanomalies=="frenchxml" )
	   			 $("#currentlySelected").append( "ONTIC Anomaly Detection Subsystem" );
	   		 else if( sourceanomalies=="spanishxml" )
	   		 	$( "#currentlySelected" ).append( "Alternative" );
   	 	});
		$.get("/getimportance", function(dissim){
  			 for(var humbral in dissim[0]){
  				 $("#chooseAnomaliesImp").append("<option value='"+humbral+"'>level: "+humbral+", total number of anomalies: "+dissim[0][humbral]+"</option>");
  			 }
//  			 $('.selectpicker').selectpicker('refresh');
  			 for(var unic in dissim[1]){
  			 	$('#currentlySelectedImp').append( "level: "+unic+", total number of anomalies: "+dissim[1][unic] );
  			 }
      	 });
		$('#sendAnomaliesImp').on('click',function(event){
   		 var level = $("#chooseAnomaliesImp.selectpicker").find(":selected")[0].value;
		 $.get("/setimportance/"+level, function(data){
				 $('#settings').click();
		 });
		 });
	};
	this._init();
}
function FlowCharts(){
	this._flow1 = new FlowD3( '#chartconv', 'TOPCONV', 5, 300, 200, '#conv-details', 'mapconv' );
	this._flow2 = new FlowD3( '#chartflow', 'TOPFLOW', 5, 300, 200, '#flow-details', 'mapflow' );
}
FlowCharts.prototype.draw = function( FROM, TO ){
	this._flow1.draw( FROM, TO );
	this._flow2.draw( FROM, TO );
}
FlowCharts.prototype.clear = function(){
	this._flow1.clear();
	this._flow2.clear();
}
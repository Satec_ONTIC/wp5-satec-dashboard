function Datamap(){
    	this.$container = $("#container1");
    	this.instance = new Datamaps({
    		scope: 'world',
            element: document.getElementById('container1'),
            projection: 'winkel3',
            // change the projection to something else only if you have absolutely no cartographic sense
            fills: { defaultFill: 'black', attack: "#ff0000", monitor:'#ffff00', recognition: '#00ff00', unknown: '#4d4d33', blue:'blue' },

            geographyConfig: {
              dataUrl: null,
              hideAntarctica: true,
              borderWidth: 0.75,
              borderColor: '#4393c3',
              popupTemplate: function(geography, data) {
                return '<div class="hoverinfo" style="color:white;background:black">' +
                       geography.properties.name + '</div>';
              },
              popupOnHover: true,
              highlightOnHover: false,
              highlightFillColor: 'black',
              highlightBorderColor: 'rgba(250, 15, 160, 0.2)',
              highlightBorderWidth: 2
            },
            done: this._handleMapReady.bind(this)
    	});
}
Datamap.prototype._handleMapReady = function(datamap) {
	    	this.zoom = new Zoom({
	    	$container: this.$container,
	      	datamap: datamap
	      });
}
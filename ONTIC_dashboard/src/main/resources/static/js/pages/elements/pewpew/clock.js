
function Clock( speeds ){
	this._refresh = 2;
	this._speeds = speeds;
	this._tics = 2;
	this.speed = 1000;
	this.start; // miliseconds
	this.end;
	this.state = "STOP";
	this._currtime; // date
	this.pewpew = null;
}
Clock.prototype._setStartEnd = function( startend ){
	this.start = moment.utc( startend.start ).toDate().getTime();
	this.end = startend.end=='now' ? startend.end : moment.utc( startend.end ).toDate().getTime();
	if( !this.pewpew )
		this.pewpew = new Pewpew( startend.start, startend.end );
	else
		this.pewpew.setStartEnd( startend.start, startend.end );
}
Clock.prototype._setSpeed = function( speed ){
	this.speed = parseInt( speed );
	this._tics = this._refresh*this._speeds[ speed=="1000" ? "X1" : speed=="500" ? "X2" : speed=="250" ? "X4" : speed=="125" ? "X8" : "" ];
}
Clock.prototype._requestOnServer = function(){
	var self = this;
	$.ajax({
   		type : "GET",
   		url : '/getpicker',
   		success : function( data ){
   			var allparams = [];
   			allparams = data.split( "X" );
   			self.state = allparams[ 4 ];
   			self._setSpeed( allparams[ 3 ] );
   			self._setStartEnd( { start: allparams[ 0 ], end: allparams[ 1 ] } );
   			self._currtime = moment.utc( allparams[ 2 ] ).toDate();
   		}
   	});
}
Clock.prototype._updateCurr = function( ticsleft, self ){
	if( self.state=='PLAY' ){
		self._currtime.setSeconds( self._currtime.getSeconds() + 1 );
		if( self.end!='now' && self._currtime.getTime() > self.end ){
			self.state = "PAUSE";
		}else{
			self._redrawelements();
		}
	}else if( self.state=='STOP' ){
		self._redrawelements();
	}
	if( ticsleft==0 ){
		self._requestOnServer();
		ticsleft = self._tics;
	}
	setTimeout( self._updateCurr, self.speed, --ticsleft, self );
}
Clock.prototype._redrawelements = function(){
	if( this.state=="PLAY" ){
		if( this.pewpew )
			this.pewpew.draw( moment( this._currtime ).format("YYYY-MM-DD HH:mm:ss"), this.speed*15 ); // truquillo: anomalies appear every 15 seconds
	}else if( this.state=="STOP"){
		if( this.pewpew )
			this.pewpew.clean();
	}
}
Clock.prototype.play = function(){
	this._updateCurr( 0, this );
}
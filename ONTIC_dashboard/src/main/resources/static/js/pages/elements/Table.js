function Table( div, top ){
	this._instance = null;
	this._top = top;
	this._$container = div;
	this._queries = ['/avgbytes', '/avgpackets', '/sumbytes', '/sumpackets'];
	this._protocols = ["HOPOPT IPv6 Hop-by-Hop Option",
                       "ICMP Internet Control Message",
                       "IGMP Internet Group Management",
                       "GGP Gateway-to-Gateway",
                       "IPv4",
                       "ST Stream",
                       "TCP",
                       "CBT CBT",
                       "EGP Exterior Gateway Protocol",
                       "IGP any private interior gateway (used by Cisco for their IGRP)",
                       "BBN-RCC-MON BBN RCC Monitoring",
                       "NVP-II Network Voice Protocol",
                       "PUP PUP",
                       "ARGUS (deprecated)	ARGUS",
                       "EMCON EMCON",
                       "XNET Cross Net Debugger",
                       "CHAOS Chaos",
                       "UDP",
                       "MUX Multiplexing",
                       "DCN-MEAS DCN Measurement Subsystems",
                       "HMP Host Monitoring",
                       "PRM Packet Radio Measurement",
                       "XNS-IDP XEROX NS IDP",
                       "TRUNK-1 Trunk-1",
                       "TRUNK-2 Trunk-2",
                       "LEAF-1 Leaf-1",
                       "LEAF-2 Leaf-2",
                       "RDP Reliable Data Protocol",
                       "IRTP Internet Reliable Transaction",
                       "ISO-TP4 ISO Transport Protocol Class 4",
                       "NETBLT Bulk Data Transfer Protocol",
                       "MFE-NSP MFE Network Services Protocol",
                       "MERIT-INP MERIT Internodal Protocol",
                       "DCCP Datagram Congestion Control Protocol",
                       "3PC Third Party Connect Protocol",
                       "IDPR Inter-Domain Policy Routing Protocol",
                       "XTP",
                       "DDP Datagram Delivery Protocol",
                       "IDPR-CMTP IDPR Control Message Transport Proto",
                       "TP++ TP++ Transport Protocol",
                       "IL IL Transport Protocol",
                       "IPv6",
                       "SDRP Source Demand Routing Protocol",
                       "IPv6-Route Routing Header for IPv6	Y",
                       "IPv6-Frag Fragment Header for IPv6	Y",
                       "IDRP Inter-Domain Routing Protocol",
                       "RSVP Reservation Protocol",
                       "GRE Generic Routing Encapsulation",
                       "DSR Dynamic Source Routing Protocol",
                       "BNA",
                       "ESP Encap Security Payload",
                       "AH Authentication Header",
                       "I-NLSP Integrated Net Layer Security TUBA",
                       "SWIPE (deprecated) IP with Encryption",
                       "NARP NBMA Address Resolution Protocol",
                       "MOBILE IP Mobility",
                       "TLSP Transport Layer Security Protocol using Kryptonet key management",
                       "SKIP",
                       "IPv6-ICMP",
                       "IPv6-NoNxt No Next Header for IPv6",
                       "IPv6-Opts Destination Options for IPv6",
                       "any host internal protocol",
                       "CFTP",
                       "any local network",
                       "SAT-EXPAK SATNET and Backroom EXPAK",
                       "KRYPTOLAN",
                       "RVD MIT Remote Virtual Disk Protocol",
                       "IPPC Internet Pluribus Packet Core",
                       "any distributed file system",
                       "SAT-MON SATNET Monitoring",
                       "VISA",
                       "IPCV Internet Packet Core Utility",
                       "CPNX Computer Protocol Network Executive",
                       "CPHB Computer Protocol Heart Beat",
                       "WSN Wang Span Network",
                       "PVP Packet Video Protocol",
                       "BR-SAT-MON Backroom SATNET Monitoring",
                       "SUN-ND SUN ND PROTOCOL-Temporary",
                       "WB-MON WIDEBAND Monitoring",
                       "WB-EXPAK WIDEBAND EXPAK",
                       "ISO-IP ISO Internet Protocol",
                       "VMTP",
                       "SECURE-VMTP",
                       "VINES",
                       "TTP Transaction Transport Protocol/IPTM Internet Protocol Traffic Manager",
                       "NSFNET-IGP",
                       "DGP Dissimilar Gateway Protocol",
                       "TCF",
                       "EIGRP",
                       "OSPFIGP",
                       "Sprite-RPC",
                       "LARP Locus Address Resolution Protocol",
                       "MTP Multicast Transport Protocol",
                       "AX.25",
                       "IPIP IP-within-IP Encapsulation Protocol",
                       "MICP (deprecated) Mobile Internetworking Control Pro.",
                       "SCC-SP Semaphore Communications Sec. Pro.",
                       "ETHERIP Ethernet-within-IP Encapsulation",
                       "ENCAP Encapsulation Header",
                       "any private encryption scheme",
                       "GMTP",
                       "IFMP Ipsilon Flow Management Protocol",
                       "PNNI",
                       "PIM Protocol Independent Multicast",
                       "ARIS",
                       "SCPS",
                       "QNX",
                       "A/N Active Networks",
                       "IPComp IP Payload Compression Protocol",
                       "SNP Sitara Networks Protocol",
                       "Compaq-Peer",
                       "IPX-in-IP",
                       "VRRP Virtual Router Redundancy Protocol",
                       "PGM PGM Reliable Transport Protocol",
                       "any 0-hop protocol",
                       "L2TP Layer Two Tunneling Protocol",
                       "DDX D-II Data Exchange",
                       "IATP Interactive Agent Transfer Protocol",
                       "STP Schedule Transfer Protocol",
                       "SRP SpectraLink Radio Protocol",
                       "UTI",
                       "SMP Simple Message Protocol",
                       "SM (deprecated) Simple Multicast Protocol",
                       "PTP Performance Transparency Protocol",
                       "ISIS over IPv4",
                       "FIRE",
                       "CRTP Combat Radio Transport Protocol",
                       "CRUDP Combat Radio User Datagram",
                       "SSCOPMCE",
                       "IPLT",
                       "SPS Secure Packet Shield",
                       "PIPE Private IP Encapsulation within IP",
                       "SCTP Stream Control Transmission Protocol",
                       "FC Fibre Channel",
                       "RSVP-E2E-IGNORE",
                       "Mobility Header",
                       "UDPLite",
                       "MPLS-in-IP",
                       "manet",
                       "HIP Host Identity Protocol",
                       "Shim6",
                       "WESP Wrapped Encapsulating Security Payload",
                       "ROHC Robust Header Compression",
                       "","","","","","","","","","",
                       "Use for experimentation and testing",
                       "Use for experimentation and testing"];
	this._clear = false;
	this._colors = {};
	this._init = function(){
		if( typeof($(this._$container)[0])!='undefined' ){
	  		  this._instance = $( this._$container )
	  		  .DataTable( {
	  			    bPaginate: false,
	  			    bLengthChange: false,
	  			    ordering: true,
	  			    order: [[ 1, 'desc' ]],
	  			    columnDefs: [
	  		        {
	  		            targets: 0,
	  		            orderable: false
	  		        },
	  		        {
	  		            targets: [0,1,2,3,4,5],
	  		            width: '10px'
	  		        }],
	  			    bInfo: false,
	  			    bAutoWidth: false,
	  			    bFilter : false,
	  			    dom: '<"top"i>rt<"bottom"flp><"clear">'
	  			} );
	  		  for( var i=0; i<this._top; i++ )
	  			  this._instance.row.add( [ "","","","","","" ] );
	//		var rowNumber = $(this._$container+' tr').length - 1;
	//		$(this._$container+' tbody tr:nth-child('+ rowNumber + ')').popover(
	//						{
	//							html : true,
	//							content : makeHtmlConv(
	//									datos[0].countrySrc,
	//									datos[0].ispSrc,
	//									datos[0].countryDst,
	//									datos[0].ispDst),
	//									delay : {
	//										"show" : SHOWTIME,
	//										"hide" : HIDETIME
	//									},
	//									placement : PLACEMENT,
	//									trigger : "hover"
	//						});
  	  }
	};
	this._init();
}
Table.prototype.clear = function(){
	if( this._instance!=null ){
		this._clear = true;
		this._instance.clear().draw();
		this._instance.destroy();
		this._init();
	}
}
Table.prototype.destroy = function(){
	if( this._instance!=null ){
		this._instance.clear().draw();
		this._instance.destroy();
	}
}
Table.prototype.setColors = function( colors ){
	this._colors = colors;
}
Table.prototype.draw = function( data, FROM, TO, index, row, self ){
	if( self==null )
		this._clear = false;
	self = self==null ? this : self;
	if( typeof( data[0] )!='undefined' ){
		var param="";
		if (data[0].srcPort != ""){
			param = "flow="+data[0].src+"_"+data[0].dst+"_"+data[0].srcPort+"_"+data[0].dstPort+"_"+data[0].prot;
		}else
			param = "conversation="+data[0].src+"_"+data[0].dst;  // datos=[{ src:'', dst:'', count:'' }]
		$.ajax({
		      type: "GET",
		      url: self._queries[ index ]+'/'+FROM+'X'+TO+'X'+param+'.blabla',
		      success: function( data1 ) { 
		    	  if( !self._clear ){
			    	  if( index == 0 ){
			    		  var conversation = [];
			    		  var src = "<b><font color='"+self._colors[ data[0].src ]+"'>"+data[0].src+"</font></b>";
			    		  var dst = "<b><font color='"+self._colors[ data[0].dst ]+"'>"+data[0].dst+"</font></b>";
			    		  var combo = src + " to " + dst;
			    		  if(data[0].srcPort != ""){
			    			  combo+= "<br>["+data[0].srcPort+"-"+data[0].dstPort+", "+self._protocols[ data[0].prot ]+"]";
			    		  }
			    		  conversation.push( combo );
			    		  conversation.push( " " );
		//				  var rowNumber = row+1;
		//					$(self._$container+' tbody tr:nth-child('
		//						+ rowNumber + ')').popover(
		//						{
		//							html : true,
		//							content : makeHtmlConv(
		//													datos[0].countrySrc,
		//													datos[0].ispSrc,
		//													datos[0].countryDst,
		//													datos[0].ispDst),
		//													delay : {
		//														"show" : SHOWTIME,
		//														"hide" : HIDETIME
		//													},
		//													placement : PLACEMENT,
		//													trigger : "hover"
		//										});
			    		  self._instance.cell( row,index ).data( conversation[0] ).draw();
			    		  self._instance.cell( row,index+1 ).data( (data[0].count).toLocaleString('en') ).draw();
			    	  }
			    	  if( data1.length ){
			    		  // index 0 or 2 for bytes
			    		  if( (index==0) || (index==2) ){
			    			  self._instance.cell( row,index+2 ).data( self._toBytes(data1[0].key) ).draw();
			    		  }else
			    			  self._instance.cell( row,index+2 ).data( (data1[0].key).toLocaleString('en') ).draw();
			    	  }	    	  
			    	  if(( data.length==1 )&&( index == self._queries.length-1 )){
		    		  	  
			    	  }else if( index == self._queries.length-1 ){
			    		  var arrQaux = data.slice();	// to avoid modifying original array
			    		  arrQaux.shift();
			    		  self.draw( arrQaux, FROM, TO, 0, ++row, self );
		    	  }else{
		    		  self.draw( data, FROM, TO, ++index, row, self );
		    	  }
		      }
		      },
		      dataType: 'json'
		  });
	}
}
Table.prototype._toBytes = function( bytes ){
	var sizes = ['b', 'KB', 'MB', 'GB', 'TB'];
	if (bytes == 0) return '0 Byte';
	var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
	return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
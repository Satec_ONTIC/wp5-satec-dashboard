function TableAnomalies( div, divmap, parent ){
	this._parent = parent;
	this._instance = null;
	this._map = divmap!=null ? new Map( divmap ) : null;
	this._$container = div;
	this._clear = false;
	this._data = [];
	this._datamap = [];
	this._colors = {
			ATTACK: '#ff0000',
			MONITOR: '#e6e600',
			RECOG: '#009933',
			UNKNOWN: '#5c5c3d'
	};
	this._init = function(){
		if( typeof($(this._$container)[0])!='undefined' ){
			var self = this;
			 $( self._$container+' tfoot th' ).each( function (){
 		        var title = $(this).text();
 		        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
 		    });
 		  self._instance = $(self._$container).on('init.dt', function (){
	    			setTimeout( function(){
	    				$( '#clearfilter' ).click(function(){
	    					$( self._$container+' tfoot th input' ).each(function(){
	    						$(this).val('').change();
	    					});
	    				});
	    				self._instance.columns().every( function (){
	    			        var that = this;
	    			        $( 'input', this.footer() ).on( 'keyup change', function (){
	    			                that.search( this.value ).draw();
	    			        } );
	    			    } );
	    			}, 1000);
	    			$( self._$container+"_filter" ).css("display", "none");
	    	    } )
	    		.DataTable( {
	    			data: [],
	    			autoWidth: false,
	    		    bFilter : true,
	    		    info: false,
	    		    dom: '<"top"i>rt<"bottom"flp><"clear">',
	    		    pageLength: 5,
	    		    order: [[ 3, 'asc' ]],
	    		    columnDefs: [
	 		    		        {
	 		    		            targets: [0,1],
	 		    		            orderable: false
	 		    		        }
	 		    		        ]
	    		});
  	  }
	};
	this._from;
	this._to;
	this._handlers = function(){
		var self = this;
		$( self._$container+' tbody' ).on( 'click', 'tr', function (){
			var index = $(this).closest("tr").index();
			var ipsrc = $(self._$container+" tr").eq(index + 2).find('td').eq(0).html();
			var ipdst = $(self._$container+" tr").eq(index + 2).find('td').eq(1).html();
			var when = $(self._$container+" tr").eq(index + 2).find('td').eq(3).html();
			var when2 = $(self._$container+" tr").eq(index + 2).find('td').eq(4).html();
			var type = $(self._$container+" tr").eq(index + 2).find('td').eq(2).html();
	        var dstarr = ipdst.split("</span>, ");
	        for( var i=0; i<dstarr.length; i++){
	        	dstarr[i] = dstarr[i].split( ">" )[1].split('<')[0];
	        }
	        ipsrc = ipsrc.split('>')[1].split('<')[0];
	        self._from = moment.utc( new Date(when) ).format("YYYY-MM-DD HH:mm:ss");
	        self._to = moment.utc( new Date(when2) ).format("YYYY-MM-DD HH:mm:ss");
	        self._pop( when, when2, [[ipsrc], dstarr], type );
		});
		$( self._$container+' tbody' ).on( 'mouseenter', 'tr td span', function (){
			self._map.center( $(this).text() );
			$(this).css( {
				'color': '#004d66',
				'font-weight':'bold'
			} );
		} );
		$(self._$container+' tbody').on( 'mouseleave', 'tr td span', function (){
			$( this ).css( {
				"color": "#616161",
				"font-weight": "normal"
			} );
			self._map.fitWorld();
		});
		$('#down').on( 'click', function (e) {
			$.get("/downloadCSVn/"+self._from+"X"+self._to+".blabla", function(data){
				var filename, link;
				
				if ( data!=null ){
					filename = 'netflows'+self._from+self._to+'.csv';	
					self._createcsv( filename, data );
				}
				setTimeout( $.get("/downloadCSVa/"+self._from+"X"+self._to+".blabla", function(data2){
					var filename, link;
					
					if ( data2!=null ){
						filename = 'anomalies'+self._from+self._to+'.csv';
						self._createcsv( filename, data2 );
					}
				 }),1000 );
			 });
		} );
	};
	this._init();
	this._handlers();
}
TableAnomalies.prototype._createcsv = function( filename, data ){
	var blob = new Blob([ data ], {
        type : "application/csv;charset=utf-8;"
    });

    if (window.navigator.msSaveBlob) {
        // FOR IE BROWSER
        navigator.msSaveBlob(blob, filename);
    } else {
        // FOR OTHER BROWSERS
        var link = document.createElement("a");
        var csvUrl = URL.createObjectURL(blob);
        link.href = csvUrl;
        link.style = "visibility:hidden";
        link.download = filename;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
}
TableAnomalies.prototype.clear = function(){
	if( this._instance!=null ){
		this._clear = true;
		if( this._map )
			this._map.clear();
		this._instance.clear().draw();
		this._instance.destroy();
		this._init();
	}
}
TableAnomalies.prototype.draw = function( FROM, TO ){
	this._clear = false;
	var self = this;
	$.ajax({
	      type: "GET",
	      url: '/anomalies4table/'+FROM+'X'+TO,
	      success: function( data ){
	    	  if( !self._clear ){
	    		  self._createData( data );
		    	  if( self._map )
					  self._map.draw( self._datamap, FROM, TO );
			      var page = self._instance.page.info().page;
			      self._instance.clear();
				  self._instance.rows.add( self._data ).draw();
				  self._instance.page( page ).draw( false );
	    	  }
	        },
	       dataType: 'json'
	  });
}
TableAnomalies.prototype._createData = function( data ){
	this._data = [];
	this._datamap = [];
	if( data==null || typeof(data)=="undefined" ){
		this._data =[[null,null,null,null,null]];
	}
	for ( var srcip in data ) {
		   if( data.hasOwnProperty(srcip) ){
			   this._datamap.push( { key:srcip } );
		    	for( var an in data[srcip] ){
		    		var ano = data[srcip][an];
					var dests="";
					for( var y=0; y<ano.dst.length; y++){
						var geodst = ano.dst[y];
						if( geodst!="" && geodst!=null ){
							var tip = geodst.split(",")[0].split("[\"")[1].split("\"]")[0];
							this._datamap.push( { key:tip } );
							dests += "<span>"+tip+"</span>, ";
						}
					}
					if( dests.length > 0 )
						dests = dests.slice( 0, dests.length-2 );
		    		this._data.push( [ "<span>"+srcip+"</span>", dests, ano.type, ano.date, ano.date2 ] );
		    	}
		   }
	}
}
TableAnomalies.prototype._pop = function( from, to, arrip, type ){
	dashboard.pause();
	$('#custom-details').css('display','block');
	$('#entrada').html( "" );
	var deftext="";
	for( var j=0; j<arrip.length; j++ ){
		if( j==0 ){
				deftext='Source Ips';
				$('#entrada').append( '<img src="img/marker-blue.png">' );
				$('#entrada').append( '<label style="margin-right:10px;">Source</label>' );
		}else if( j==1 ){
				deftext='Destination Ips';
		}else{
				deftext='other';
		}
		$('#entrada').append( "<select id='select_ip_"+j+"'></select>" );
		for( var k=0; k<arrip[j].length; k++ ){
				$( '#select_ip_'+j ).append( '<option value="'+arrip[j][k]+'">'+arrip[j][k]+'</option>' );
		}
		if( k>1 )
				$( '#select_ip_'+j ).append( '<option value="all">view all</option>' );
		$( '#select_ip_'+j ).quickselect({
				activeButtonClass : 'btn-primary active',
				buttonClass : 'btn btn-default',
				selectDefaultText : deftext,
				wrapperClass : 'btn-group'
		});
	}
	$('#entrada').append( '<label style="margin-left:10px;">Destination</label>' );
	$('#entrada').append( '<img src="img/marker-red.png">' );
	var self = this;
	$("#entrada .btn").change(function(event) {
				var val1 = $( '#entrada span:eq(0)' ).text();
				var val2 = $( '#entrada span:eq(1)' ).text();
				var granarray = [];
				var origenes = [];
				var destinos = [];
				if( val1=="view all" ){
					$( '#entrada select:eq(0)' ).find('option').each(function() {
						if( $(this).text()!="view all" )
							origenes.push( $(this).text() );
					});
				}else{
					origenes.push( val1 );
				}
				if( val2=="view all" ){
					$( '#entrada select:eq(1)' ).find('option').each(function() {
						if( $(this).text()!="view all" )
							destinos.push( $(this).text() );
					});
				}else{
					destinos.push( val2 );
				}
				for( var q=0; q<origenes.length; q++ ){
					for( var z=0; z<destinos.length; z++ ){
						var att={};
						att.src= origenes[q];
						att.dst= destinos[z];
						granarray.push( att );
					}
				}
				mapdet.draw( granarray, self._from, self._to, 0 );
		});
		$('#entrada').append( "<div id='additional'><b>PERIOD:</b> "+from+" <b>to</b> "+to );
		if( type!=null ){
			$('#entrada').append( "<b>DESCRIPTION:</b> <i>"+type+"</i>" );
		}
		$('#entrada').append( "</div>" );
	    var pie1= new PieChart( '#chart1details', 200, 'TOPDSTIPS', false, null );
	    pie1.draw( this._from, this._to );
	    var pie2= new PieChart( '#chart2details', 200, 'TOPSRCIPS', false, null );
	    pie2.draw( this._from, this._to );
	    var pie3= new PieChart( '#chart3details', 200, 'TOPDSTPORTS', false, null );
	    pie3.draw( this._from, this._to );
	    var pie4= new PieChart( '#chart4details', 200, 'TOPSRCPORTS', false, null );
	    pie4.draw( this._from, this._to );
	    var pie5= new PieChart( '#chart5details', 200, 'TOPTOS', false, null );
	    pie5.draw( this._from, this._to );
	    var pie6= new PieChart( '#chart6details', 200, 'TOPPROT', false, null );
	    pie6.draw( this._from, this._to );
	    var flow1 = new FlowD3( '#chart12details', 'TOPCONV', 5, 200, 200, '#conv-details0', null );
	    flow1.draw( this._from, this._to );
	    var flow2 = new FlowD3( '#chart13details', 'TOPFLOW', 5, 200, 200, '#conv-details1', null );
	    flow2.draw( this._from, this._to );
	    var from5 = new Date(this._from);
	    var to5 = new Date(this._to);
	    from5.setMinutes( from5.getMinutes()-5 );
	    to5.setMinutes( to5.getMinutes()+5 );
	    from5=moment( from5 ).format('YYYY-MM-DD HH:mm:ss');
	    to5=moment( to5 ).format('YYYY-MM-DD HH:mm:ss');
	    var line2 = new LineChart( '#chart9details', 600, [ 'AVGPCKTSEC','TOTPCKTSEC' ]);
	    line2.draw( from5, to5, null, null );
	    line2.xgrids( [ {value:this._from},{value:this._to} ], null );
	    var line3 = new LineChart( '#chart11details', 600, [ 'CLOSEDSEC' ] );
	    line3.draw( from5, to5, null, null );
	    line3.xgrids( [ {value:this._from},{value:this._to} ], null );
	    var val1 = $( '#entrada span:eq(0)' ).text();
		var val2 = $( '#entrada span:eq(1)' ).text();
		var att={};
		att.src=val1;
		att.dst=val2;
		var mapdet = new MapFlow( 'mapdetails' );
		mapdet.draw( [att], this._from, this._to, 0 );
//	    DeltaY=0;
	    Custombox.open({
	                target: '#custom-details',
	                effect: 'fadein',
	                width: $('.videoContainer').css('width'),
	                speed: 1000,
	                position: [ 'center' , 'center' ],
	                close : function(){
	                	$('#entrada').html( "" );
	                	pie1.destroy();pie1 = null;
	                	pie2.destroy();pie2 = null;
	                	pie3.destroy();pie3 = null;
	                	pie4.destroy();pie4 = null;
	            	    pie5.destroy();pie5 = null;
	            	    pie6.destroy();pie6 = null;
	            	    flow1.destroy();flow1 = null;
	            	    flow2.destroy();flow2 = null;
	            	    line2.destroy();line2 = null;
	            	    line3.destroy();line3 = null;
	            	    mapdet.destroy();mapdet=null;
	                	$('#custom-details').css('display','none');
	                }
	    });
}
TableAnomalies.prototype.filter = function( type, when ){
	$(this._$container+" tfoot tr th:eq(2) input").val( type ).change();
	$(this._$container+" tfoot tr th:eq(3) input").val( when ).change();
}
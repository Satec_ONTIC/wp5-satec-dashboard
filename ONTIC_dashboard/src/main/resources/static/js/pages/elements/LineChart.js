
function LineChart( div, width, lines, parent ){
	this._parent = parent;
	this._lines = {
			CLOSEDSEC : { query:'/closedpersec', value: ['count', 'key_as_string'] },
			AVGPCKTSEC : { query: '/packetspersec', value: [ 'average', 'key_as_string' ] },
			TOTPCKTSEC : { query: '/packetstotsec', value: [ 'total', 'key_as_string' ] },
			AVGBYTESEC : { query: '/bytespersec', value: [ 'average', 'key_as_string' ] },
			TOTBYTESEC : { query: '/bytestotsec', value: [ 'total', 'key_as_string' ] },
			TYPESANOMALIES: { query: '/typesdeanos', value: [ 'count', 'key_as_string' ] }
	};
	this._$container = div;
	this._instance = null;
	this._width = width;
	this._line = lines;
	this._clear = false;
	this._init = function(){
		var self = this;
		if( typeof($(this._$container)[0])!='undefined' ){
			var xs ={};
			xs['N/A'] = 'key';
			var json = {};
			json['N/A'] = 0;
			json['key'] = '';
			var color = {};
			color['N/A'] = "#0099CC";
			this._instance = c3.generate({
    			size: {
    		  		width: this._width
    			},
        		bindto: this._$container,
        		data:{
        			json: [json],
        			mimeType: 'json',
        			xs: xs,
            		xFormat: '%Y-%m-%d %H:%M:%S',
        			keys: {value: [ 'N/A','key' ] },
        			axes: {},
        			colors:color,
        			unload: true,
        			onmouseover: function(e) {
        				if( self._line[0]=='TYPESANOMALIES' ){
	        				var type = e.name.toUpperCase();
	        				var when = moment( e.x ).format( "YYYY-MM-DD HH:mm:ss" );
	        				if( self._parent )
	        					self._parent.filterTable( type, when );
        				}
        			}
        		},
        		axis : {
            		x : {
                		type : 'timeseries',
                		tick: {
                    		fit: true,
                    		format: "%H:%M:%S"
                		}
            		},
            		y: {
            			padding: { bottom:0 },
            			tick: {
                    		format: this._line!="TYPESANOMALIES" ? d3.format("s") : function (d) {
                                return (parseInt(d) == d) ? d : null;
                            }
                		},
                		label:{
                			text: this._lines[ this._line[0] ].value[0],
                			position:'outer-middle'
                		}
            		},
            		y2: {
            			show: ( this._line!="TYPESANOMALIES" && this._line!='CLOSEDSEC' ) ? true : false,
            			padding: { bottom:0 },
            			tick: {
                    		format: this._line!="TYPESANOMALIES" ? d3.format("s") : function (d) {
                                return (parseInt(d) == d) ? d : null;
                            }
                		},
                		label:{
                			text:'',
                			position:'outer-middle'
                		}
            		}
        		},
        		zoom: {
        	        enabled: false
        	    }
    		});
		}
	};
	this._enable = true;
	this._handler = function(){
		var self = this;
		$(this._$container).click( function(){
			self._enable = !self._enable;
			if( !self._enable )
				self.clear();
		} );
	};
	this._init();
	this._handler();
}
LineChart.prototype.clear = function(){
	if( this._instance!=null ){
		this._clear = true;
		this._instance = this._instance.destroy();
		this._init();
	}
}
LineChart.prototype.destroy = function(){
	if( this._instance!=null ){
		this._instance = this._instance.destroy();
	}
}
LineChart.prototype.draw = function( FROM, TO, line, self ){
	self = self==null ? this : self;
	if( self._enable ){
		if( line==null )
			self._clear = false;
		var avg = line==null ? this._line : line;
		$.ajax({
		      type: "GET",
		      url: self._lines[ avg[0] ].query+'/'+FROM+"X"+TO,
		      success: function( data ) {
		    	  var xs = {};
		    	  xs[ self._lines[avg[0]].value[0] ] = self._lines[avg[0]].value[1];
		    	  var axes={ };
		    	  if( line!=null ){
		    		  axes[ self._lines[avg[0]].value[0] ] = 'y2';
		    	  }
		    	  if( data.length > 0 && typeof($(self._$container)[0])!='undefined' && !self._clear ){
		    			  for (var q=0;q<data.length;q++)
						  {
		    				  data[q].key_as_string = moment( moment.utc( data[q].key_as_string ).toDate() ).format( "YYYY-MM-DD HH:mm:ss" );
						  }
				    	  if( self._instance ){
				    		  self._instance.load({
				    			  json: data,
				    			  xs:xs,
				    			  xFormat: '%Y-%m-%d %H:%M:%S',
				    			  mimeType: 'json',
				    			  keys: { value: self._lines[avg[0]].value },
				    			  axes: axes,
				    			  unload:['N/A']
				    		  });
				    		  if( line!=null ){
				    			  self._instance.axis.labels( { y2: self._lines[ avg[0] ].value[0] } );
				    		  }
				    		  if( avg.length>1 ){
						    	  var avg2 = avg.slice();
					    		  avg2.shift();
					    		  setTimeout( self.draw, 500, FROM, TO, avg2, self );
					    	  }
				    	  }  
			      }
		        },
		       dataType: 'json'
		  });
	}
}
LineChart.prototype.xgrids = function( grids, color ){
	this._instance.xgrids( grids );
	if( color )
		$("g.c3-xgrid-lines line").css( "stroke", color );
}
LineChart.prototype.drawAnomalies = function( FROM, TO ){
	if( this._enable ){
		var self = this;
		self._clear = false;
		$.ajax({
		      type: "GET",
		      url: self._lines[ self._line[0] ].query+'/'+FROM+"X"+TO,
		      success: function( data ) {
		    	  if( !self._clear ){
		    		  var unloadthem = ['attack', 'to monitor', 'recognition', 'unknown', 'N/A' ];
		    		for( var t=0; t<data.length; t++ ){
		    			if( data[t].length>0 ){
		    				unloadthem.splice( unloadthem.indexOf( data[t][0].key ), 1 );
		    			}
		    		}
		    		var unloadids = ['N/A'];
		    		var datanames = self._instance.data.names();
		    		for( var ids in datanames ){
		    			if( datanames.hasOwnProperty(ids) ){
		    				if( unloadthem.indexOf( datanames[ids] ) > -1 ){
		    					unloadids.push( ids );
		    				}
		    			}
		    		}
		    		self._instance.unload( { ids: unloadids } );
		    		setTimeout( function(){
		    			self._loadLine( data, 0, self );
		    		}, 100 );	  
			      }
		        },
		       dataType: 'json'
		  });
	}
}
LineChart.prototype._loadLine = function( data, i, self ){
	if( !self._clear && i<data.length ){
    		  if( data[i].length>0 ){
    			      var names = {};
    			  	  var prefix = i+"_";
		    		  names[prefix+self._lines[ self._line[0] ].value[0] ]=data[i][0].key;
		    		  var xs = {};
		    		  xs[prefix+self._lines[ self._line[0] ].value[0]] = prefix+self._lines[ self._line[0] ].value[1];
		    		  var newData = self._addPrefix( prefix,data[i] );
		    		  var color = {};
		    		  if( names[prefix+self._lines[ self._line[0] ].value[0]]=="attack" ){
		    			  color[prefix+self._lines[ self._line[0] ].value[0]] = "#ff0000";
		    		  }else if( names[prefix+self._lines[ self._line[0] ].value[0]]=="to monitor" ){
		    			  color[prefix+self._lines[ self._line[0] ].value[0]] = "#e6e600";
		    		  }else if( names[prefix+self._lines[ self._line[0] ].value[0]]=="recognition" ){
		    			  color[prefix+self._lines[ self._line[0] ].value[0]] = "#009933";
		    		  }else if( names[prefix+self._lines[ self._line[0] ].value[0]]=="unknown" ){
		    			  color[prefix+self._lines[ self._line[0] ].value[0]] = "#5c5c3d";
		    		  }
					  if( self._instance ){
							self._instance.xgrids( [ {value: 0 } ] );
					        self._instance.load({
					    			  json: newData,
					    			  xs:xs,
					    			  xFormat: '%Y-%m-%d %H:%M:%S',
					    			  keys: {value: [ prefix+self._lines[ self._line[0] ].value[0], 
					    			                  prefix+self._lines[ self._line[0] ].value[1] ]
					    			  		},
					    			  mimeType: 'json',
					    			  colors:color,
					    			  type: 'area-spline'
					    	});
					    	self._instance.data.names( names );
					    	setTimeout( function(){
				    			self._loadLine( data, ++i, self );
				    		}, 100 );	
					 }
    		  }else{
    			  setTimeout( function(){
		    			self._loadLine( data, ++i, self );
		    		}, 100 );	
    		  }
	}
}
LineChart.prototype._addPrefix = function( prefix, source ){
	 if (Object.prototype.toString.call(source) === '[object Array]') {
		  var clone = [];
		  for (var i=0; i<source.length; i++) {
			  clone[i] = this._addPrefix(prefix, source[i]);
		  }
		  return clone;
	  } else if (typeof(source)=="object") {
		  var clone = {};
		  for (var prop in source) {
			  if (source.hasOwnProperty(prop)) {
				clone[prefix + prop] = this._addPrefix(prefix, source[prop]);
			  }
		  }
		  return clone;
	  } else {
		  return source;
	  }
}
/**
 * 
 */
function GaugeChart( div, width ){
	this._$container = div;
	this._instance = null;
	this._width = width;
	this._clear = false;
	this._init = function(){
		if( typeof($(this._$container)[0])!='undefined' ){
			this._instance = c3.generate({
	    			size: {
	    		  		width: this._width
	    			},
	    			bindto: this._$container,
	    			data: {
	    				json: { bytes:0 },
	    				mimeType: 'json',
	    	  			type: "gauge",
	    	  			onmouseover: function (d, element) {
	    	  				
	    	  			}
	    			},
	    			gauge:{
	    		        label: {
	    		            format: function(value, ratio) {
	    		                return value;
	    		            },
	    		            show: true // to turn off the min/max labels.
	    		        },
		    		    min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
		    		    max: 1000, // 100 is default
		    		    units: ' Mb/s',
		    		    width: 19 // for adjusting arc thickness
	    		    },
	    		    color: {
	    		        pattern: ['#FF0000', '#F97600', '#F6C600', '#60B044'], // the three color levels for the percentage values.
	    		        threshold: {
	    		            unit: 'Mb/s', // percentage is default
	    		            max: 1000, // 100 is default
	    		            values: [300, 600, 900, 1000]
	    		        }
	    		    },
	    			onmouseout: function (){
	    				
	    			},
	    			transition: {
	    				  duration: null
	    			}
	    		});
		}
	};
	this._enable = true;
	this._handler = function(){
		var self = this;
		$(this._$container).click( function(){
			self._enable = !self._enable;
			if( !self._enable )
				self.clear();
		} );
	};
	this._init();
	this._handler();
}
GaugeChart.prototype.clear = function(){
	if( this._instance!=null ){
		this._clear = true;
		this._init();
	}
}
GaugeChart.prototype.destroy = function(){
	if( this._instance!=null ){
		this._instance = this._instance.destroy();
	}
}
GaugeChart.prototype.draw = function( FROM, TO ){
	if( this._enable ){
		this._clear = false;
		var self = this;
		$.ajax({
		      type: "GET",
		      url: '/bytesporsegundo/'+FROM+"X"+TO,
		      success: function(data) {
		    	  if( data==null || typeof($(self._$container)[0])=='undefined' || self._clear ){
		    		  
		    	  }else{
		    		  if( self._instance ){
		    			  self._instance.load({
			    			  json: data,
			    			  mimeType: 'json'
			    		  });
		    		  }
		    	  }
		        },
		       dataType: 'json'
		  });
	}
}
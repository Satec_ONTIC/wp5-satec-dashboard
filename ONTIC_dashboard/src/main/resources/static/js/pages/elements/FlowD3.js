function FlowD3( div, flow, top, width, height, divtable, divmap ){
	this._$container = div;
	this._top = top;
	this._width = width;
	this._height = height;
	this._flows = {
			TOPCONV:'/topconv',
			TOPFLOW:'/topflow'
	};
	this._COLORS = [ "#DD7115",'#0099CC',
	                 "#DB1C14",
	                 "#D0BA4E","#761116",
	                 "#432F50","#F92935",
	                 "#EDE8DC","#217C5B","#2AA3D6",
	                 "#1D996F","#4F0C1B","#1268AE","#4E496C",
	                 "#C99B0D","#EED119","#96223C","#DFD09A","#FE5D75", "#000000", "#2688B6" ];
	this._flow = flow;
	this._clear = false;
	this._instance = null;
	this._table = new Table( divtable, top );
	this._map = divmap!=null ? new MapFlow( divmap ) : null;
	this._init = function( data ){
		var self = this;
		if( typeof($(this._$container)[0])!='undefined' ){
			if( this._instance ){
		  		  this._instance[0][0].firstChild.remove();
		  	  }
	    	  var AIP=[];
	    	  var ANpackts = [];
	    	  var LeyendaIP = [];
	    	  var Tpaquetes = 0;
	    	  var colores=this._COLORS;
	    		  // Initialize a square matrix.
	    		 for (var i = 0; i <= 2*this._top; i++) {
	    		    ANpackts[i] = [];
	    		    for (var j = 0; j <= 2*this._top; j++) {
	    		      ANpackts[i][j] = 0;
	    		    }
	    		 }

	    		//Indexo las ip origen y destino.
	    		for (i = 0; i < data.length; i++) {
	    			var posipsrc=AIP.indexOf(data[i].src);
	    			var posipdst=AIP.indexOf(data[i].dst);
	    			
	    			if (posipsrc == -1){
	    				posipsrc=AIP.push(data[i].src)-1;
	    				LeyendaIP[LeyendaIP.length] = data[i].src;
	    			}
	    			if (posipdst == -1){
	    				posipdst=AIP.push(data[i].dst)-1;
	    				LeyendaIP[LeyendaIP.length] = data[i].dst;
	    			}
	    			ANpackts[posipsrc][posipdst]+=data[i].count;	
	    		}

	    		  var longip =AIP.length;
	    		  //Ajustamos los datos al ser variables 
	    		  //Generamos la tabla con el tamaño correcto y calculamos el maximo de paquetes para la leyenda.
	    		  var finaltable = [];
	    		    for (var i = 0; i < longip; i++) {
	    		    finaltable[i] = [];
	    		    for (var j = 0; j < longip; j++) {
	    		      finaltable[i][j] = ANpackts[i][j];
	    		      Tpaquetes+=ANpackts[i][j];
	    		    }
	    		  }
	    		  //Ajustamos los colores que se muestran en la leyenda.
	    		    var newcolores=colores.slice();
	    		  newcolores.splice(longip,(newcolores.length)-longip);
	    		  var mapeao = {};
	    		  for(var l=0; l<LeyendaIP.length; l++){
	    			  mapeao[LeyendaIP[l]]=newcolores[l];
	    		  }
	    		  //Calculamos el numero de etiquetas...
	    		  	var numticks;
	    		  	var formatdata = 1;
	    			var format = "";
	    			var leyendatick = 1;
	    			var ndigitos=0;
	    			
	    			while( Tpaquetes>=1 ) 
	    			{ 
	    				Tpaquetes=Tpaquetes/10; 
	    				ndigitos=ndigitos+1; 
	    			} 
	    			  numticks = Math.pow(10,ndigitos -3);
	    			  if (numticks<1) numticks=1; 		  
	    		  	  leyendatick = 10; 
	    		  	  
	    			 if (ndigitos>=6 && ndigitos<10 ){
	    				formatdata = Math.pow(10,3);
	    				format = " M";
	    			}else if (ndigitos>=10){
	    					formatdata = Math.pow(10,6);
	    					format = " Millones ";
	    				}
	    		var chord = d3.layout.chord()
	    		    .padding(.05)
	    		    .sortSubgroups(d3.descending)
	    		    .matrix(finaltable);
	    		var width = this._width,
	    		    height = this._height,
	    		    innerRadius = Math.min(width, height) * .41,
	    		    outerRadius = innerRadius * 1.1;
	    		var fill = d3.scale.ordinal()
	    		    .domain(d3.range(7))
	    		    .range(colores);
	    		this._instance = d3.select( this._$container );
	    		var svg = d3.select( this._$container ).append("svg")
	    		    .attr("width", width)
	    		    .attr("height", height)
	    		  .append("g")
	    		    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
	    		svg.append("g").selectAll("path")
	    		    .data(chord.groups)
	    		  .enter().append("path")
	    		    .style("fill", function(d) { return fill(d.index); })
	    		    .style("stroke", function(d) { return fill(d.index); })
	    		    .attr("d", d3.svg.arc().innerRadius(innerRadius).outerRadius(outerRadius))
	    		    .on("mouseover", fade(.1))
	    		    .on("mouseout", fade(1));

	    		var ticks = svg.append("g").selectAll("g")
	    		    .data(chord.groups)
	    		  .enter().append("g").selectAll("g")
	    		    .data(function(d){
	    		    	var k = (d.endAngle - d.startAngle) / d.value;
	  	    		  return d3.range(0, d.value, numticks).map(function(v, i) {
	  	    		    return {
	  	    		      angle: v * k + d.startAngle,
	  	    		      label: i % leyendatick ? null : (v / formatdata )+ format
	  	    		    };
	  	    		  });
	    		    })
	    		  .enter().append("g")
	    		    .attr("transform", function(d) {
	    		      return "rotate(" + (d.angle * 180 / Math.PI - 90) + ")"
	    		          + "translate(" + outerRadius + ",0)";
	    		    });
	    		ticks.append("line")
	    		    .attr("x1", 1)
	    		    .attr("y1", 0)
	    		    .attr("x2", 5)
	    		    .attr("y2", 0)
	    		    .style("stroke", "#000");
	    		ticks.append("text")
	    		    .attr("x", 8)
	    		    .attr("dy", ".35em")
	    		    .attr("transform", function(d) { return d.angle > Math.PI ? "rotate(180)translate(-16)" : null; })
	    		    .style("text-anchor", function(d) { return d.angle > Math.PI ? "end" : null; })
	    		    .text(function(d) { return d.label; });
	    		svg.append("g")
	    		    .attr("class", "chord")
	    		  .selectAll("path")
	    		    .data(chord.chords)
	    		  .enter().append("path")
	    		    .attr("d", d3.svg.chord().radius(innerRadius))
	    		    .style("fill", function(d) { return fill(d.target.index); })
	    		    .style("opacity", 1);

	    		//Returns an event handler for fading a given chord group.
	    		function fade(opacity) {
	    		  return function(g, i) {
	    		    svg.selectAll(".chord path")
	    		        .filter(function(d) {  return d.source.index != i && d.target.index != i; })
	    		      .transition()
	    		        .style("opacity", opacity);
	    		  };
	    		}
	    		 var w = -100;
	    		var legend = svg.append("g")
	    		  .attr("class", "legend")
	    		  .attr("x", w - 65)
	    		  .attr("y", 125)
	    		  .attr("height", 100)
	    		  .attr("width", 100);
	    		legend.selectAll("rect")
	    		  .data(newcolores)
	    		  .enter()
	    		  .append("rect")
	    		  .attr("x", function(d, i){
	    			  i = i % 2;
	    			  return i *  116 - 100;
	    			  })
	    		  .attr("y", function(d, i){ 
	    			  i = i / 2;
	    			  i = Math.floor( i ); 
	    			  return 250+(i*20);
	    		  })
	    		  .attr("width", 10)
	    		  .attr("height", 10)
	    		  .style("fill", function(d) { 
	    			  return d;
	    		  }) 
	    		legend.selectAll("text")
	    		  .data(LeyendaIP)
	    		  .enter()
	    		  .append("text")
	    		  .attr("x", function(d, i){
	    			  i = i % 2;
	    			  return i *  116 - 100 + 12;
	    			  })
	    		  .attr("y", function(d, i){ 
	    			  i = i / 2;
	    			  i = Math.floor( i ); 
	    			  return 259+(i*20);
	    		  })
	    		  .text(function(d) {
	    		      return d;
	    		    });
	    		return mapeao;
		}
	};
	this._enable = true;
	this._handler = function(){
		var self = this;
		$(this._$container).click( function(){
			self._enable = !self._enable;
			if( !self._enable )
				self.clear();
		} );
	};
	this._init( [{count: 1,src: 'N/A',dst: 'N/A'}] );
	this._handler();
}

FlowD3.prototype.clear = function(){
	if( this._instance!=null ){
		this._clear = true;
		this._table.clear();
		if( this._map )
			this._map.clear();
		this._init( [{count: 1,src: 'N/A',dst: 'N/A'}] );
	}
}
FlowD3.prototype.destroy = function(){
	if( this._instance!=null ){
		this._table.destroy();
		if( this._map )
			this._map.destroy();
		this._instance[0][0].firstChild.remove();
	}
}
FlowD3.prototype.draw = function( FROM, TO ){
	if( this._enable ){
		this._clear = false;
		var self = this;
		$.ajax({
		      type: "GET",
		      url: self._flows[ self._flow ]+'/'+FROM+"X"+TO,
		      success: function(data) {
		    	  if( !self._clear ){
		    		  if( self._map )
		    			  self._map.draw( data, FROM, TO, 0 );
		    		  if( typeof( data[0] )!='undefined' ){
		    			  var colors =self._init( data );
			    		  self._table.setColors( colors );
		    		  }
			    	  self._table.draw( data, FROM, TO, 0, 0, null );
		    	  }
		        },
		       dataType: 'json'
		  });
	}
}
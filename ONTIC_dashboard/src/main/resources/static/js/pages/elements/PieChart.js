
function PieChart( div, width, pie, tooltip, divmap ){
	this._istool = tooltip;
	this._$container = div;
	this._instance = null;
	this._width = width;
	this._map = divmap!=null ? new Map( divmap ) : null;
	this._values = [ 'count1', 'count2', 'count3', 'count4', 'count5' ];
	this._id = 'key';
	this._pies = {
			TOPPROT : { query: '/topprot', esfield: 'netflowprotocol' },
			TOPTOS : { query: '/toptos', esfield: 'netflowsrctos' },
			TOPSRCPORTS : { query: '/topsrcports', esfield: 'netflowl4srcport' },
			TOPDSTPORTS : { query: '/topdstports', esfield: 'netflowl4dstport' },
			TOPSRCIPS : { query: '/topsrcip', esfield: 'netflowipv4srcaddr' },
			TOPDSTIPS : { query: '/topdstip', esfield: 'netflowipv4dstaddr' }
	};
	this._pie = pie;
	this._clear = false;
	this._protocols = ["HOPOPT IPv6 Hop-by-Hop Option",
	                               "ICMP Internet Control Message",
	                               "IGMP Internet Group Management",
	                               "GGP Gateway-to-Gateway",
	                               "IPv4",
	                               "ST Stream",
	                               "TCP",
	                               "CBT CBT",
	                               "EGP Exterior Gateway Protocol",
	                               "IGP any private interior gateway (used by Cisco for their IGRP)",
	                               "BBN-RCC-MON BBN RCC Monitoring",
	                               "NVP-II Network Voice Protocol",
	                               "PUP PUP",
	                               "ARGUS (deprecated)	ARGUS",
	                               "EMCON EMCON",
	                               "XNET Cross Net Debugger",
	                               "CHAOS Chaos",
	                               "UDP",
	                               "MUX Multiplexing",
	                               "DCN-MEAS DCN Measurement Subsystems",
	                               "HMP Host Monitoring",
	                               "PRM Packet Radio Measurement",
	                               "XNS-IDP XEROX NS IDP",
	                               "TRUNK-1 Trunk-1",
	                               "TRUNK-2 Trunk-2",
	                               "LEAF-1 Leaf-1",
	                               "LEAF-2 Leaf-2",
	                               "RDP Reliable Data Protocol",
	                               "IRTP Internet Reliable Transaction",
	                               "ISO-TP4 ISO Transport Protocol Class 4",
	                               "NETBLT Bulk Data Transfer Protocol",
	                               "MFE-NSP MFE Network Services Protocol",
	                               "MERIT-INP MERIT Internodal Protocol",
	                               "DCCP Datagram Congestion Control Protocol",
	                               "3PC Third Party Connect Protocol",
	                               "IDPR Inter-Domain Policy Routing Protocol",
	                               "XTP",
	                               "DDP Datagram Delivery Protocol",
	                               "IDPR-CMTP IDPR Control Message Transport Proto",
	                               "TP++ TP++ Transport Protocol",
	                               "IL IL Transport Protocol",
	                               "IPv6",
	                               "SDRP Source Demand Routing Protocol",
	                               "IPv6-Route Routing Header for IPv6	Y",
	                               "IPv6-Frag Fragment Header for IPv6	Y",
	                               "IDRP Inter-Domain Routing Protocol",
	                               "RSVP Reservation Protocol",
	                               "GRE Generic Routing Encapsulation",
	                               "DSR Dynamic Source Routing Protocol",
	                               "BNA",
	                               "ESP Encap Security Payload",
	                               "AH Authentication Header",
	                               "I-NLSP Integrated Net Layer Security TUBA",
	                               "SWIPE (deprecated) IP with Encryption",
	                               "NARP NBMA Address Resolution Protocol",
	                               "MOBILE IP Mobility",
	                               "TLSP Transport Layer Security Protocol using Kryptonet key management",
	                               "SKIP",
	                               "IPv6-ICMP",
	                               "IPv6-NoNxt No Next Header for IPv6",
	                               "IPv6-Opts Destination Options for IPv6",
	                               "any host internal protocol",
	                               "CFTP",
	                               "any local network",
	                               "SAT-EXPAK SATNET and Backroom EXPAK",
	                               "KRYPTOLAN",
	                               "RVD MIT Remote Virtual Disk Protocol",
	                               "IPPC Internet Pluribus Packet Core",
	                               "any distributed file system",
	                               "SAT-MON SATNET Monitoring",
	                               "VISA",
	                               "IPCV Internet Packet Core Utility",
	                               "CPNX Computer Protocol Network Executive",
	                               "CPHB Computer Protocol Heart Beat",
	                               "WSN Wang Span Network",
	                               "PVP Packet Video Protocol",
	                               "BR-SAT-MON Backroom SATNET Monitoring",
	                               "SUN-ND SUN ND PROTOCOL-Temporary",
	                               "WB-MON WIDEBAND Monitoring",
	                               "WB-EXPAK WIDEBAND EXPAK",
	                               "ISO-IP ISO Internet Protocol",
	                               "VMTP",
	                               "SECURE-VMTP",
	                               "VINES",
	                               "TTP Transaction Transport Protocol/IPTM Internet Protocol Traffic Manager",
	                               "NSFNET-IGP",
	                               "DGP Dissimilar Gateway Protocol",
	                               "TCF",
	                               "EIGRP",
	                               "OSPFIGP",
	                               "Sprite-RPC",
	                               "LARP Locus Address Resolution Protocol",
	                               "MTP Multicast Transport Protocol",
	                               "AX.25",
	                               "IPIP IP-within-IP Encapsulation Protocol",
	                               "MICP (deprecated) Mobile Internetworking Control Pro.",
	                               "SCC-SP Semaphore Communications Sec. Pro.",
	                               "ETHERIP Ethernet-within-IP Encapsulation",
	                               "ENCAP Encapsulation Header",
	                               "any private encryption scheme",
	                               "GMTP",
	                               "IFMP Ipsilon Flow Management Protocol",
	                               "PNNI",
	                               "PIM Protocol Independent Multicast",
	                               "ARIS",
	                               "SCPS",
	                               "QNX",
	                               "A/N Active Networks",
	                               "IPComp IP Payload Compression Protocol",
	                               "SNP Sitara Networks Protocol",
	                               "Compaq-Peer",
	                               "IPX-in-IP",
	                               "VRRP Virtual Router Redundancy Protocol",
	                               "PGM PGM Reliable Transport Protocol",
	                               "any 0-hop protocol",
	                               "L2TP Layer Two Tunneling Protocol",
	                               "DDX D-II Data Exchange",
	                               "IATP Interactive Agent Transfer Protocol",
	                               "STP Schedule Transfer Protocol",
	                               "SRP SpectraLink Radio Protocol",
	                               "UTI",
	                               "SMP Simple Message Protocol",
	                               "SM (deprecated) Simple Multicast Protocol",
	                               "PTP Performance Transparency Protocol",
	                               "ISIS over IPv4",
	                               "FIRE",
	                               "CRTP Combat Radio Transport Protocol",
	                               "CRUDP Combat Radio User Datagram",
	                               "SSCOPMCE",
	                               "IPLT",
	                               "SPS Secure Packet Shield",
	                               "PIPE Private IP Encapsulation within IP",
	                               "SCTP Stream Control Transmission Protocol",
	                               "FC Fibre Channel",
	                               "RSVP-E2E-IGNORE",
	                               "Mobility Header",
	                               "UDPLite",
	                               "MPLS-in-IP",
	                               "manet",
	                               "HIP Host Identity Protocol",
	                               "Shim6",
	                               "WESP Wrapped Encapsulating Security Payload",
	                               "ROHC Robust Header Compression",
	                               "","","","","","","","","","",
	                               "Use for experimentation and testing",
	                               "Use for experimentation and testing"];
	this._tos =[
	                 "(Best effort)",
	                 "(Priority)",
	                 "(Immediate)",
	                 "(Flash mainly used for voice signaling or for video)",
	                 "(Flash Override)",
	                 "(Critic mainly used for voice RTP)",
	                 "(Internetwork Control)",
	                 "(Network Control)"
	            ];
	this._tooltip = this._istool ? new Tooltip( div, pie, this._protocols, width ) : null;
	this._FROM;
	this._TO;
	this._init = function(){
		var self = this;
		if( typeof($(this._$container)[0])!='undefined' ){
			var color ={};
			color['N/A'] = "#0099CC";
			this._instance = c3.generate({
    			size: {
    		  		width: this._width
    			},
    			bindto: this._$container,
    			data: {
    				columns: [['N/A', 10]],
    	  			type: "pie",
    	  			colors: color,
    	  			unload: true,
    	  			onmouseover: function (d, element) {
    	  				if( self._istool ){
	    	  				var param;
	    	  				if( self._pie=="TOPPROT" ){
	    	  					param = self._pies[ self._pie ].esfield+"="+self._protocols.indexOf( d.name );
	    	  				}else if( self._pie=="TOPTOS" ){
	    	  					param = self._pies[ self._pie ].esfield+"="+(d.name).split(" (")[0];
	    	  				}else if( self._pie=="TOPDSTPORTS" || self._pie=="TOPSRCPORTS" ){
	    	  					param = self._pies[ self._pie ].esfield+"="+(d.name.split("(")[1]).split(")")[0];
	    	  				}else{
	    	  					param = self._pies[ self._pie ].esfield+"="+d.name;
	    	  				}
	    	  				self._tooltip.enable();
	    	  				self._tooltip.setParameters( param );
	    	  				self._tooltip.setName( d.name );
	    	  				self._tooltip.tooltipme( self._FROM, self._TO, null, null );
    	  				}
    	  			}
    			},
    			onmouseout: function (){
    				if( self._istool ){
    					self._tooltip.disable();
    				}
    			}
    		});
		}
	};
	this._enable = true;
	this._handler = function(){
		var self = this;
		$(this._$container).click( function(){
			self._enable = !self._enable;
			if( !self._enable )
				self.clear();
		} );
	};
	this._init();
	this._handler();
}
PieChart.prototype.clear = function(){
	if( this._instance!=null ){
		this._clear = true;
		if( this._map )
			this._map.clear();
//		this._instance = this._instance.destroy();
		this._init();
	}
}
PieChart.prototype.destroy = function(){
	if( this._instance!=null ){
		this._instance = this._instance.destroy();
	}
}
PieChart.prototype.draw = function( FROM, TO ){
	if( this._enable ){
		this._clear = false;
		this._FROM = FROM;
		this._TO = TO;
		var self = this;
		$.ajax({
		      type: "GET",
		      url: self._pies[ self._pie ].query+'/'+FROM+"X"+TO,
		      success: function(data){
		    	  var names = {};
		    	  if( data.length > 0 && typeof($(self._$container)[0])!='undefined' && !self._clear ){
		    		  if( self._map )
		    			  self._map.draw( data, FROM, TO );
		    		  if( self._pie=='TOPPROT' ){
		    			  for( var j=0; j<self._values.length; j++ ){
		    	  				names[ self._values[j] ]= self._protocols[ data[j][self._id] ];
		    	  			}
		    		  }else if( self._pie=='TOPTOS' ){
		    			  for( var j=0; j<self._values.length; j++ ){
		    	  				names[ self._values[j] ]=  data[j][self._id]+" "+self._last( data[j][self._id] );
		    	  			}
		    		  }else{
		    			  for( var j=0; j<self._values.length; j++ ){
		    	  				names[ self._values[j] ]=  ""+data[j][self._id];
		    	  			}
		    		  }
		    		  if( self._instance ){
			    		  self._instance.load({
			    			  json: data,
			    			  mimeType: 'json',
			    			  keys: { value: self._values },
			    			  unload: ['N/A']
			    		  });
			    		  self._instance.data.names( names );
			    	  }
		    	  }
		        },
		       dataType: 'json'
		  });
	}
}
PieChart.prototype._last3 = function(number, last){
	var rig = 8-last;
	var a = number;
	return ( a >>> rig );
}
PieChart.prototype._last = function(number){
	return this._tos[ this._last3( number, 3) ];
}
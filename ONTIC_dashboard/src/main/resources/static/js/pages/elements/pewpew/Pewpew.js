function FixedQueue( size, initialValues ){
        initialValues = (initialValues || []);
        var queue = Array.apply( null, initialValues );
        queue.fixedSize = size;
        queue.push = FixedQueue.push;
        queue.splice = FixedQueue.splice;
        queue.unshift = FixedQueue.unshift;
        FixedQueue.trimTail.call( queue );
        return( queue );
      }
      FixedQueue.trimHead = function(){
        if (this.length <= this.fixedSize){ return; }
        Array.prototype.splice.call( this, 0, (this.length - this.fixedSize) );
      };
      FixedQueue.trimTail = function(){
        if (this.length <= this.fixedSize) { return; }
        Array.prototype.splice.call( this, this.fixedSize, (this.length - this.fixedSize)
        );
      };
      FixedQueue.wrapMethod = function( methodName, trimMethod ){
        var wrapper = function(){
          var method = Array.prototype[ methodName ];
          var result = method.apply( this, arguments );
          trimMethod.call( this );
          return( result );
        };
        return( wrapper );
      };
      FixedQueue.push = FixedQueue.wrapMethod( "push", FixedQueue.trimHead );
      FixedQueue.splice = FixedQueue.wrapMethod( "splice", FixedQueue.trimTail );
      FixedQueue.unshift = FixedQueue.wrapMethod( "unshift", FixedQueue.trimTail );
      
function Pewpew( start, end ){
	this.start = start;
	this.end = end;
	this.hits = {};
	this._fqueue = 20;
	this._wait4lines = false;
	this._enter = false;
	this._fixedhits = FixedQueue( this._fqueue, [  ] );
	this.map = new Datamap();
	this._init = function(){
		var self = this;
        $.ajax({
        	type : "GET",
        	url : '/anomalies4table/'+self.start+'X'+self.end,
        	success : function(data) {
	           		for( var srcip in data ){
	           			if( data.hasOwnProperty(srcip) ){
		           		       	var srcll = self._srclatlong( data[srcip] );
		           		       	if( srcll!=null ){
			           		       		for( var ind in data[srcip] ){
			           		       				var anom = data[srcip][ind];
			           		       				anom.date = anom.date;
			           		       				for( var ind2 in anom.dst ){
			           		       					var geodestination = anom.dst[ind2];
			           		       					if( geodestination !="" && geodestination!=null ){
				           		       					var dstip = geodestination.split(",")[0].split("[\"")[1].split("\"]")[0];
				           		       					var dstcountry = geodestination.split(",")[3].split("[\"")[1].split("\"]")[0];
				           		       					var dstlat = geodestination.split(",")[2].split("[")[1].split("]")[0];
				           		       					var dstlng = geodestination.split(",")[1].split("[")[1].split("]")[0];
				           		       					var colour;
						           		         		if( anom.type.indexOf("ATTACK")>-1 ){
						           		         			colour = "#ff0000";
						           		         		}else if( anom.type.indexOf("MONITOR")>-1 ){
						           		         			colour = "#ffff00";
						           		         		}else if( anom.type.indexOf("RECOGNITION")>-1 ){
						           		         			colour = "#00ff00";
						           		         		}else{
						           		         			colour = "#4d4d33";
						           		         		}
						           		         		var rad = 5;
						           		         		var hit = { origin : { latitude: +srcll.srclat, longitude: +srcll.srclong, ip:srcip, country:srcll.srccountry, type:anom.type, boom: {
																           		             		 radius: 4, 
																           		                     latitude: +srcll.srclat, 
																           		                     longitude: +srcll.srclong, 
																           		                     fillOpacity: 0,
																           		                     fillKey: 'blue',
																           		                     borderOpacity: 1, 
																           		                     attk: srcll.srccountry+' ('+srcip+') '+anom.type, 
																           		                     borderColor: "blue",
																           		                     borderWidth: 1} },
											           		        destination : { latitude: +dstlat, longitude: +dstlng, ip:dstip, country:dstcountry, type:anom.type, boom:{
														           		                    	 radius: rad, 
															           		                     latitude: +dstlat, 
															           		                     longitude: +dstlng, 
															           		                     fillOpacity: 0.1,
															           		                     fillKey: colour=="#ff0000" ? 'attack' : 
															           		                    	      colour=="#ffff00" ? 'monitor' :
															           		                    	      colour=="#00ff00" ? 'recognition' :
															           		                    	      'unknown',
															           		                     borderOpacity: 0.25, 
															           		                     attk: dstcountry+' ('+dstip+') '+anom.type, 
															           		                     borderColor: colour,
															           		                     borderWidth: 1} }
																    };
						           		         		if( self.hits.hasOwnProperty(anom.date) ){
						           		         			var arrhits = self.hits[anom.date];
						           		         			arrhits.push( hit );
						           		         			self.hits[anom.date] = arrhits;
						           		         		}else{
						           		         			self.hits[anom.date] = [hit];
						           		         		}		           		         		
			           		       					}
			           		       				}
			           		       		}
		           		       	}
		           	}
	           	}
        	}, dataType : 'json'
        });
	};
	this._handlers = function(){
		var self = this;
		$('#attackdiv').click( function(e){
			var datatime = $(e.target).data('time');
			var dataindex = $(e.target).data('index');
			if( typeof(datatime)!='undefined' && typeof(dataindex)!='undefined' ){
				var attack = self.hits[datatime][dataindex];
				self._fixedhits.push( $.extend( {}, attack ) );
				var booms = [];
				for( var f=0; f<self._fixedhits.length;f++ ){
					booms.push( self._fixedhits[f].origin.boom );
					booms.push( self._fixedhits[f].destination.boom );
				}
				if( fxsound ){
			        document.getElementById("starwars").load();
			        document.getElementById("starwars").play();
			    }
				self.map.instance.arc( self._fixedhits, { strokeWidth: 2, strokeColor: "blue", arcSharpness: 1 });
				self.map.instance.bubbles(booms, {
				    popupTemplate: function(geo, data) {
				      return '<div class="hoverinfo">' + data.attk + '</div>';
				    }, exitDelay: 250
				});
				self._wait4lines = true;
				setTimeout( function(){
					self._undraw( 0, self );
				}, 500 );
			}
		});
		$('#attackdiv').mouseenter(function(){
			self._enter = true;
		});
		$('#attackdiv').mouseleave(function(){
			self._enter = false;
		});
	};
	if( this.end!='now' )
		this._init();
	this._handlers();
}
Pewpew.prototype.draw = function( second, speed ){
		var hitssec = this.hits[second];
		if( typeof(hitssec)!='undefined' && hitssec.length > 0 ){
			this._wait4lines = false;
			this._draw( hitssec, 0, speed, second, this );
		}
}
Pewpew.prototype._draw = function( arr, index, speed, second, self ){
	if( index < arr.length){
		self._fixedhits.push( $.extend( {}, arr[ index ] ) );
		var booms = [];
		for( var f=0; f<self._fixedhits.length;f++ ){
			booms.push( self._fixedhits[f].origin.boom );
			booms.push( self._fixedhits[f].destination.boom );
		}
		if( fxsound ){
	        document.getElementById("starwars").load();
	        document.getElementById("starwars").play();
	    }
		self.map.instance.arc(self._fixedhits, { strokeWidth: 2, strokeColor: "blue", arcSharpness: 1 });
		self.map.instance.bubbles(booms, {
			    popupTemplate: function(geo, data) {
			      return '<div class="hoverinfo">' + data.attk + '</div>';
			    }, exitDelay: 250
		});
		$('#attackdiv').append("<div data-time='"+second+"' data-index='"+index+"'>"+arr[index].origin.country + " (" + arr[index].origin.ip + ") " +
	                  " <font data-time='"+second+"' data-index='"+index+"' color='"+arr[index].destination.boom.borderColor+"'>attacks</font> " +
	                  arr[index].destination.country +" (" + arr[index].destination.ip + ") " +
	                  " <font data-time='"+second+"' data-index='"+index+"' color='steelblue'>(" + arr[index].origin.type + ")</font></div> ");
		if( !self._enter )
			$('#attackdiv').animate({scrollTop: $('#attackdiv').prop("scrollHeight")}, speed/arr.length );
		setTimeout( self._draw, speed/arr.length, arr, ++index, speed, second, self );
	}else{
		self._wait4lines = true;
		setTimeout( function(){
			self._undraw( 0, self );
		}, 500 );
	}
}
Pewpew.prototype._undraw = function( index, self ){
	if( index < self._fqueue && self._wait4lines ){
		self._fixedhits.push( $.extend( {}, {
			origin:{boom:{}},
			destination:{boom:{}}
		} ) );
		var booms = [];
		for( var f=0; f<self._fixedhits.length;f++ ){
			booms.push( self._fixedhits[f].origin.boom );
			booms.push( self._fixedhits[f].destination.boom );
		}
		self.map.instance.arc(self._fixedhits, { strokeWidth: 2, strokeColor: "blue", arcSharpness: 1 });
		self.map.instance.bubbles(booms, {
			    popupTemplate: function(geo, data) {
			      return '<div class="hoverinfo">' + data.attk + '</div>';
			    }, exitDelay: 500
		});
		setTimeout( self._undraw, 100, ++index, self );
	}
}
Pewpew.prototype.clean = function(){
    this._fixedhits = FixedQueue( this._fqueue, [  ] );
    this.map.instance.svg.selectAll('path.datamaps-arc').remove();
    this.map.instance.svg.selectAll('circle.datamaps-bubble').remove();
    $('#attackdiv').html("");
}
Pewpew.prototype._srclatlong = function( arrdata ){
	var srcLat, srcLng, country;
	if( arrdata[0].src[0]!="" && arrdata[0].src[0]!=null ){
		srcLng = arrdata[0].src[0].split(",")[1].split("[")[1].split("]")[0];
		srcLat= arrdata[0].src[0].split(",")[2].split("[")[1].split("]")[0];
		country= arrdata[0].src[0].split(",")[3].split("[\"")[1].split("\"]")[0];
		return { srclat: srcLat, srclong: srcLng, srccountry: country };
	}else
		return null;
}
Pewpew.prototype.setStartEnd = function( start, end ){
	if( this.start==start && this.end==end ){
	}else{
		this.start = start;
		this.end = end;
		this.hits = {};
		if( this.end!='now' )
			this._init();
	}
}
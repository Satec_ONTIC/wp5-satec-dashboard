
function MapFlow( div ){
	this._instance = null;
	this._layer = null;
	this._globalmarkers = [];
	this._arraydataconversationssrc = [];
	this._arraydataconversationsdst = [];
	this._icons = {
			src: 'img/marker-blue.png',
			dst: 'img/marker-red.png'
	};
	this._$container = div;
	this._clear = false;
	this._init = function(){
		var openstreetmap = L.tileLayer(
							'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
							{
								maxZoom : 18,
								minZoom : 1,
								bounds: [
								         [83.66, -179.70],
								         [-65.39, 179.70]
								],
								attribution : '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors, Points &copy 2012 LINZ'
							});
		var latlng = L.latLng(20, 0);
		this._instance = L.map( this._$container, {
						center : latlng,
						zoom : 1,
						layers : [ openstreetmap ]
					});
	};
	this._init();
}
MapFlow.prototype.clear = function(){
	if( this._instance!=null ){
		this._clear = true;
		this._instance.fitWorld();
		if( this._layer )
			this._instance.removeLayer( this._layer );
	}
}
MapFlow.prototype.destroy = function(){
	if( this._instance!=null ){
		this._instance.remove();
	}
}
MapFlow.prototype.draw = function( data, FROM, TO, i ){
		var self = this;
		if( i==0 ){
			self._clear = false;
			if( self._layer )
				self._instance.removeLayer( self._layer );
			self._arraydataconversationssrc = [];
			self._arraydataconversationsdst = [];
			self._layer = L.markerClusterGroup();
		}
		if( typeof( data[i] )!='undefined' ){
			$.ajax({
				type : "GET",
				url : "/geoipsrc/" + data[i].src + ".blabla",
				success : function(data1) {
					if( !self._clear ){
						if( data1.responseText!="" )
							self._arraydataconversationssrc.push( data1 );
						$.ajax({
							type : "GET",
							url : "/geoipdst/" + data[i].dst + ".blabla",
							success : function(data2) {
								if( !self._clear){
									if( data2.responseText!="" )
										self._arraydataconversationsdst.push(data2);
									i++;
									if (i < data.length) {
										self.draw( data, FROM, TO, i );
									}else{
										self._filterarrays();
									}
								}
							},
							dataType : 'json'
						});
					}
				},
				dataType : 'json'
			});
	}
}
MapFlow.prototype._filterarrays = function(){
	if ( (this._arraydataconversationssrc.length == this._arraydataconversationsdst.length) && this._arraydataconversationssrc.length>0 ){
		this._addpaths();
	}
	this._instance.addLayer(this._layer);
}
MapFlow.prototype._addpaths = function(){
	var Iconred = L.icon({
		iconUrl : this._icons['dst'],
		iconAnchor : [ 12, 40 ],
		popupAnchor : [ 0, -45 ],
	});
	var Iconblue = L.icon({
		iconUrl : this._icons['src'],
		iconAnchor : [ 12, 40 ],
		popupAnchor : [ 0, -45 ],
	});
	for (var w = 0; w < this._arraydataconversationssrc.length; w++) {
		var srclat = this._arraydataconversationssrc[w].latitude;
		var srclong = this._arraydataconversationssrc[w].longitude;

		var dstlat = this._arraydataconversationsdst[w].latitude;
		var dstlong = this._arraydataconversationsdst[w].longitude;

		var polygon = L.polygon(
				[
						[ srclat[0],
								srclong[0] ],
						[ dstlat[0],
								dstlong[0] ] ], {
					color : 'red'
				}).addTo( this._layer );
		var markerdst = L.marker(new L.LatLng(dstlat[0], dstlong[0]), {
			icon : Iconred
		});
		var markersrc = L.marker(new L.LatLng(srclat[0], srclong[0]), {
			icon : Iconblue
		});
		
		this._layer.addLayer(markerdst);
		this._layer.addLayer(markersrc);
	}
}
/**
 * 
 */
function Tooltip( parentcontainer, pie, protocols, width ){
	this._width = width==null ? 250 : width;
	this._text = "<ul style='list-style-type:none'>";
	this._pies = {
			TOPPROT : [ 'TOPSRCIPS', 'TOPDSTIPS', 'TOPSRCPORTS', 'TOPDSTPORTS', 'SUMBYTE', 'SUMPCKT' ],
			TOPTOS : [ 'TOPSRCIPS', 'TOPDSTIPS', 'TOPSRCPORTS', 'TOPDSTPORTS', 'TOPPROT', 'SUMBYTE', 'SUMPCKT' ],
			TOPSRCPORTS : [ 'TOPSRCIPS', 'TOPDSTIPS', 'TOPPROT', 'SUMBYTE', 'SUMPCKT' ],
			TOPDSTPORTS : [ 'TOPSRCIPS', 'TOPDSTIPS', 'TOPPROT', 'SUMBYTE', 'SUMPCKT' ],
			TOPSRCIPS : [ 'TOPSRCPORTS', 'TOPDSTPORTS', 'TOPPROT', 'SUMBYTE', 'SUMPCKT' ],
			TOPDSTIPS : [ 'TOPSRCPORTS', 'TOPDSTPORTS', 'TOPPROT', 'SUMBYTE', 'SUMPCKT' ]
	};
	this._tooltips = {
			TOPSRCIPS : { query:'/topsrcip' , text:'Top 5 Source addrs: ' },
			TOPDSTIPS: { query:'/topdstip', text: 'Top 5 Dest addrs: '},
			TOPSRCPORTS: { query:'/topsrcports', text: 'Top 5 Source ports: '},
			TOPDSTPORTS: { query:'/topdstports', text: 'Top 5 Dest ports: '},
			SUMBYTE: { query:'/sumbytes', text: '# bytes: '},
			SUMPCKT: { query:'/sumpackets', text: '# packets: '},
			TOPPROT: { query:'/topprot', text: 'Top 5 protocols: '}
	};
	this._protocols = protocols;
	this._$container = parentcontainer;
	this._param;
	this._what;
	this._pie = pie;
	this._init = function(){
		if( !$( this._$container ).hasClass("tooltipstered") ){
			$( this._$container ).tooltipster({
	  			delay: 100,
	  	        maxWidth: this._width,
	  	        theme: 'tooltipster-default',
	  	        speed: 300,
	  	        interactive: true,
	  	        animation: 'grow',
	  	        position: 'top-left'
	  		} );
		}
	};
	this._init();
}
Tooltip.prototype.setParameters = function( p ){
	this._param = p;
}
Tooltip.prototype.setName = function( name ){
	this._what = this._text;
	this._what += name;
}
Tooltip.prototype.tooltipme = function( FROM, TO, arrQ, self ){
	arrQ = arrQ==null ? this._pies[ this._pie ]: arrQ;
	self = self==null ? this : self;
	$.ajax({
	      type: "GET",
	      url: self._tooltips[ arrQ[0] ].query+'/'+FROM+'X'+TO+'X'+self._param+'.blabla',
	      success: function( data ) {
	    	  self._what += "<hr><li>";
	    	  self._what += self._tooltips[ arrQ[0] ].text+"<br>";
	    	  for(var i=0 ; i<data.length; i++){
	    		  if( arrQ[0]=="TOPPROT"){
	    			  self._what += self._protocols[ data[i].key ];
	    		  }else if( arrQ[0]=="SUMBYTE" || arrQ[0]=="SUMPCKT" ){
	    			  self._what += (data[i].key).toLocaleString();
	    		  }else{
	    			  self._what += data[i].key;
	    		  }
	    		  if(i < data.length-1)
	    			  self._what += ", ";
	    	  }
	    	  self._what += "</li>";
	    	  if( arrQ.length==1 ){
	    		  self._what += "<hr></ul>";
	    		  $(self._$container).tooltipster('content', $( self._what ));
	    		  $(self._$container).tooltipster('show');
	    		  return self._what;
	    	  }else{
	    		  var arrQaux = arrQ.slice();	// to avoid modifying original array
	    		  arrQaux.shift();
	    		  return self.tooltipme( FROM, TO, arrQaux, self );
	    	  }
	      },
	      dataType: 'json'
	  });
}
Tooltip.prototype.enable = function(){
	$(this._$container).tooltipster('enable');
	$(this._$container).tooltipster('hide');
	var rect = this._cumulativeOffset( $(this._$container)[0] );
	$(this._$container).tooltipster('option','offsetY', rect.top-posY);
	$(this._$container).tooltipster('option','offsetX', posX-rect.left);
}
Tooltip.prototype.disable = function(){
	$(this._$container).tooltipster( 'disable' );
}
Tooltip.prototype._cumulativeOffset = function( dom ){
	var top = 0, left = 0;
    do {
        top += dom.offsetTop  || 0;
        left += dom.offsetLeft || 0;
        dom = dom.offsetParent;
    } while(dom);

    return {
        top: top,
        left: left
    };
}
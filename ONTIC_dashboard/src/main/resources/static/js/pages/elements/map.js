
function Map( div ){
	this._instance = null;
	this._layer = null;
	this._globalmarkers = [];
	this._$container = div;
	this._clear = false;
	this._init = function(){
		var openstreetmap = L.tileLayer(
							'http://{s}.tile.osm.org/{z}/{x}/{y}.png',
							{
								maxZoom : 18,
								attribution : '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors, Points &copy 2012 LINZ'
							});
		var latlng = L.latLng(51, 9);
		this._instance = L.map( this._$container, {
						center : latlng,
						zoom : 1,
						layers : [ openstreetmap ]
					});
	};
	this._init();
}
Map.prototype.clear = function(){
	if( this._instance!=null ){
		this._clear = true;
		this._instance.fitWorld();
		if( this._layer )
			this._instance.removeLayer( this._layer );
	}
}
Map.prototype.draw = function( data, FROM, TO ){
		this._clear = false;
		var self = this;
		if( this._layer )
			this._instance.removeLayer( this._layer );
		 this._layer = L.markerClusterGroup();
		for (var i = 0; i < data.length; i++) {
				var param= data[i].key;
				var coor = null;
				for( var h=0; h<self._globalmarkers.length; h++ ){
					if( self._globalmarkers[h].ip == param ){
						coor = {};
						coor['latitude'] = [ self._globalmarkers[h].lat];
						coor['longitude'] = [ self._globalmarkers[h].long];
						coor['ip'] = [ self._globalmarkers[h].ip];
						break;
					}
				}
				if( coor==null ){
					$.ajax({
						type : "GET",
						url : "/geoipsrc/" + param + ".blabla",
						success : function(data1) {
								if( !self._clear ){
										if (data1 != null) {
											self._drawCoordinates( data1 );
										}	
								}
						},
						dataType : 'json'
					});
				}else{
					self._drawCoordinates( coor );
				}	
		}
}
Map.prototype._drawCoordinates = function( data1 ){
	var pin = L.latLng(0, 0);
	pin.lat = data1.latitude;
	pin.lng = data1.longitude;
	var searchlat = pin.lat[0];
	var searchlng = pin.lng[0];
	var searchip = data1.ip[0];
	this._globalmarkers.push( {
		ip: searchip,
		lat: searchlat,
		long: searchlng
	} );

	var marker = L.marker( new L.LatLng(pin.lat, pin.lng) );
	marker.bindPopup('IP = ' + data1.ip + "<br>" + 'Country = '
			+ data1.country_name + "<br>" + "Country code = "
			+ data1.country_code3 + "<br>" + "Latitude = "
			+ data1.latitude + "<br>" + "Longitude = "
			+ data1.longitude);
	this._layer.addLayer( marker );
	this._instance.addLayer( this._layer );
}
Map.prototype.fitWorld = function(){
	this._instance.fitWorld();
}
Map.prototype.setZoom = function( zoom ){
	this._instance.setZoom( zoom );
}
Map.prototype.center = function( ip ){
	for(var l=0; l<this._globalmarkers.length; l++){
		if( this._globalmarkers[l].ip==ip ){
			this._instance.setView( [ this._globalmarkers[l].lat, this._globalmarkers[l].long ] );
			this._instance.setZoom( 6 );
			break;
		}
	}
}
function AnomalieCharts(){
	this._table = new TableAnomalies( '#tablealarms', 'map3', this );
	this._line = new LineChart( '#charttype', 950, ['TYPESANOMALIES'], this );
	$( "#custom-details #report" ).on('click', function(e) {	
		svgAsPngUri(document.getElementById("chart1details").getElementsByTagName('svg')[0], {}, function(uri) {
			doc = new jsPDF('p', 'pt', 'a4', true);
			doc.setFontSize(12);
			svgAsPngUri(document.getElementById("chart2details").getElementsByTagName('svg')[0], {}, function(uri2) {
				svgAsPngUri(document.getElementById("chart3details").getElementsByTagName('svg')[0], {}, function(uri3) {
					svgAsPngUri(document.getElementById("chart4details").getElementsByTagName('svg')[0], {}, function(uri4) {
						svgAsPngUri(document.getElementById("chart5details").getElementsByTagName('svg')[0], {}, function(uri5) {
							svgAsPngUri(document.getElementById("chart6details").getElementsByTagName('svg')[0], {}, function(uri6) {
								svgAsPngUri(document.getElementById("chart12details").getElementsByTagName('svg')[0], {}, function(uri12) {
									svgAsPngUri(document.getElementById("chart13details").getElementsByTagName('svg')[0], {}, function(uri13) {
//										svgAsPngUri(document.getElementById("chart7details").getElementsByTagName('svg')[0], {
//											selectorRemap: function(s) { return s.replace(/.c3 /g, ''); }
//										}, function(uri7) {
											svgAsPngUri(document.getElementById("chart9details").getElementsByTagName('svg')[0], {
												selectorRemap: function(s) { return s.replace(/.c3 /g, ''); }
											}, function(uri9) {
												svgAsPngUri(document.getElementById("chart11details").getElementsByTagName('svg')[0], {
													selectorRemap: function(s) { return s.replace(/.c3 /g, ''); }
												}, function(uri11) {
													doc.text( 50, 30, "Data Network Traffic Dashboard" );
													doc.setLineWidth( 1.5 );
													doc.line( 50, 35, 400, 35 );
														doc.fromHTML($('#additional').get(0), 0, 80, {'width': 250}, function(dispose) {
															function tableToJson(table) {
															var data = [];
															var headers = [];
															for (var i=0; i<table.rows[0].cells.length; i++) {
															    headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi,'');
															}
															for (var i=0; i<table.rows.length; i++) {
															    var tableRow = table.rows[i];
															    var rowData = {};

															    for (var j=0; j<tableRow.cells.length; j++) {
															    	if( j==0 ){
															    		var text = tableRow.cells[j].innerText;
																		tableRow.cells[j].innerHTML = text;
															    	}
															    		rowData[ headers[j] ] = tableRow.cells[j].innerHTML;
															    }

															    data.push(rowData);
															}       
															return data; 
															}
								
																doc.text(110, 155, "Top 5 deestination ip");
																doc.addImage(uri, 'PNG', 115, 160, 100, 150);
																doc.text(390, 155, "Top 5 source ip");
																doc.addImage(uri2, 'PNG', 380, 160, 100, 150);
																
																doc.text(110, 360, "Top 5 destination port");
																doc.addImage(uri3, 'PNG', 115, 365, 100, 150);
																doc.text(390, 360, "Top 5 source port");
																doc.addImage(uri4, 'PNG', 380, 365, 100, 150);
																
																doc.text(110, 565, "Top 5 type of service");
																doc.addImage(uri5, 'PNG', 115, 570, 100, 150);
																doc.text(390, 565, "Top 5 protocols");
																doc.addImage(uri6, 'PNG', 380, 570, 100, 150);
																doc.addPage();
																
																doc.text(80, 100, "Top 5 conversations");
																doc.addImage(uri12, 'PNG', 50, 115, 120, 120);
																var table1 = tableToJson($('#conv-details0').get(0)),
															        table2 = tableToJson($('#conv-details1').get(0));
															   doc.cellInitialize();
															   $.each(table1, function (i, row)
															    {
															        $.each(row, function (j, cellContent) {
//															                doc.margins = 1;
															                doc.setFont("courier ");
															                doc.setFontType("bolditalic "); 
															                doc.setFontSize(8);
															                doc.cell(240, 120, j=="conversation" ? 125 : 40, 20, cellContent, i);
															        });
															    });
															   doc.setFontSize(12);
															   doc.text(90, 350, "Top 10 flows");
															   doc.addImage(uri13, 'PNG', 50, 365, 120, 120);
															   doc.cellInitialize();
															   $.each(table2, function (i, row)
																	    {
																	        $.each(row, function (j, cellContent) {
																	                doc.setFont("courier ");
																	                doc.setFontType("bolditalic ");
																	                doc.setFontSize(8);
																	                doc.cell(235, 330, j=="flow" ? 130 : 40, 20, cellContent, i);
																	        });
																	    });
															   	doc.setFontSize(12);
//																doc.text(250, 600, "Average/Total In bytes");
//																doc.addImage(uri7, 'PNG', 50, 620, 490, 160);
																doc.addPage();
																doc.text(250, 90, "Average/Total In packets");
																doc.addImage(uri9, 'PNG', 50, 120, 490, 160);
																doc.text(250, 350, "Average/Total in bytes");
																doc.addImage(uri11, 'PNG', 50, 370, 490, 160);
																doc.setFontSize(6);
																doc.save('report.pdf');	
														    });
												});
											});
//										});
									});
								});
							});
						});
					});
				});
			});
		});
	});
}
AnomalieCharts.prototype.draw = function( FROM, TO ){
	this._table.draw( FROM, TO );
	this._line.drawAnomalies( FROM, TO );
}
AnomalieCharts.prototype.clear = function(){
	this._table.clear();
	this._line.clear();
}
AnomalieCharts.prototype.filterTable = function( type, when ){
	this._table.filter( type ,when );
}
AnomalieCharts.prototype.gridLine = function( grid, color ){
	this._line.xgrids( grid, color );
}
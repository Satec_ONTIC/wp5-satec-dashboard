function OpenPcap(){
	this._init = function(){
		$.ajax({
			type:'GET',
			url:'/listdirectory',
			success: function( data ){
				for (var j=0; j<data.length; j++){
	        		var select = document.getElementById( 'choosePCAP' );
	        		var opt = document.createElement( 'option' );
	    			opt.value = data[j];
	    			opt.innerHTML = data[j];
	    			select.appendChild(opt);
	    		}
			},
			mimeType:'json'
		});
		$.get("/dinamicPcapLoaded", function(loaded){
			var loadedlist = document.getElementById('loaded');
	        	for (var j=0; j<loaded.length; j++){
	        		var opt = document.createElement("LI");
	        		opt.className += "list-group-item list-group-item-success";
	        		var dates = loaded[j].split('_');
	        		opt.innerHTML = dates[0].substring( 0,4 )+"-"+dates[0].substring( 4,6 )+"-"+dates[0].substring( 6,8 )+" "+
	        						dates[0].substring( 8,10 )+":"+dates[0].substring( 10,12 )+":"+dates[0].substring( 12,14 )+" "+
	        						dates[1].substring( 0,4 )+"-"+dates[1].substring( 4,6 )+"-"+dates[1].substring( 6,8 )+" "+
	        						dates[1].substring( 8,10 )+":"+dates[1].substring( 10,12 )+":"+dates[1].substring( 12,14 );
	        		loadedlist.appendChild(opt);
    			}
	        	 if (loaded.length==0){
	        		var opt = document.createElement("LI");
	        		opt.className += "list-group-item list-group-item-danger";
	        		opt.innerHTML = "No pcap loaded into database";
	    			loadedlist.appendChild(opt);
	        	 }
    	 });
		$('#send').on('click',function(event){
		 	var pcap = $("#choosePCAP.selectpicker").find(":selected").text();
			 $.get("/add/"+pcap, function(data){
				 $('.btnOpen').click();
			 });
	     });
		 $('#erase').on('click', function(event){
	             $.get("/erase", function(data){
	            	 $('.btnOpen').click();
				 });
	      });
	};
	this._init();
}
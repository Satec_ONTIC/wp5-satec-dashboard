function Export(){
	this._init = function(){
		var self = this;
		$('#frompicker').datetimepicker({
			format : 'Y-m-d H:i:s',
			step : 1,
			inline:true,
			theme:'dark'
		});
		$('#topicker').datetimepicker({
			format : 'Y-m-d H:i:s',
			step : 1,
			inline:true,
			theme:'dark'
		});
		$('#downdown').on('click',function(event) {
		 	var anornet = $("#anomaliesornetflow.selectpicker").find(":selected").text();
		 	var start = $('#frompicker').datetimepicker('getValue')[0].value;
		 	var end = $('#topicker').datetimepicker('getValue')[0].value;

    		var start1 = moment.utc( moment( start ).toDate() ).format("YYYY-MM-DD HH:mm:ss");
    		var end1 = moment.utc( moment( end ).toDate() ).format("YYYY-MM-DD HH:mm:ss");
    		if( anornet=="Netflows" ){
    			$.get("/downloadCSVn/"+start1+"X"+end1+".blabla", function(data){
					var filename, link;
					if ( data!=null ){
						filename = 'netflows'+start+end+'.csv';
						self._createcsv( filename, data );
					}
				 });
    		}else if( anornet=="Anomalies" ){
    			$.get("/downloadCSVa/"+start1+"X"+end1+".blabla", function(data){
					var filename, link;
					if ( data!=null ){
						filename = 'anomalies'+start+end+'.csv';
						self._createcsv( filename, data );
					}
				 });
    		}
	     });
	};
	this._init();
}
Export.prototype._createcsv = function( filename, data ){
	var blob = new Blob([ data ], {
        type : "application/csv;charset=utf-8;"
    });

    if (window.navigator.msSaveBlob) {
        // FOR IE BROWSER
        navigator.msSaveBlob(blob, filename);
    } else {
        // FOR OTHER BROWSERS
        var link = document.createElement("a");
        var csvUrl = URL.createObjectURL(blob);
        link.href = csvUrl;
        link.style = "visibility:hidden";
        link.download = filename;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
}
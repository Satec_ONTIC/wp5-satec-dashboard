/**
 * 
 */
function Clock( speeds ){
	this._refresh = 2;
	this._speeds = speeds;
	this._tics = 2;
	this.static_start = true;
	this.speed = 1000;
	this.start;
	this.end;
	this.state = "STOP";
	this._FROM;
	this._TO;
	this.interval = 0; //miliseconds
	this._forceredraw = false;
	this._anomalies = {};
}
Clock.prototype.setInterval = function( interval ){
	if( this.state=="STOP" && !this.static_start ){
		this.interval = interval;
	}
}
Clock.prototype.setStart = function( start ){
	if( this.state=="STOP" ){
		this.start = moment( start ).toDate().getTime();
	}
}
Clock.prototype.setEnd = function( end ){
	if( this.state=="STOP" ){
		this.end = end=='now' ? end : moment( end ).toDate().getTime();
		if( this.start && this.end ){
			var self = this;
			  $.ajax({
			      type: "GET",
			      url: '/newalerts/'+moment.utc(new Date(this.start)).format("YYYY-MM-DD HH:mm:ss")+'X'+moment.utc(new Date(this.end)).format("YYYY-MM-DD HH:mm:ss"),
			      success: function( data ){
			    	  self._anomalies = data;
			      },
			      dataType: 'json'
			  });
		  }	
	}
}
Clock.prototype.setStatic = function( stat ){
	if( this.state=="STOP" ){
		this.static_start = stat;
	}
}
Clock.prototype.setSpeed = function( speed ){
	if( speed=="X1" ){
		this.speed = 1000;
	}
	if( speed=="X2" )
		this.speed = 1000/2;
	if( speed=="X4" )
		this.speed = 1000/4;
	if( speed=="X8" )
		this.speed = 1000/8;
	this._tics = this._refresh*this._speeds[ speed ];
}
Clock.prototype._updateOnServer = function(){
	$.ajax({
		type : "POST",
		url : '/setpicker/' + moment.utc(new Date(this.start)).format("YYYY-MM-DD HH:mm:ss")+'X'+moment.utc(new Date(this.end)).format("YYYY-MM-DD HH:mm:ss")+'X'+moment.utc(this._TO).format("YYYY-MM-DD HH:mm:ss")+'X'+this.speed+'X'+this.state,
		success : function(data){
		}
	});
}
Clock.prototype._updateFROMTO = function( ticsleft, self ){
	if( self.state=="PLAY" ){
		self._TO.setSeconds( self._TO.getSeconds() + 1 );
		if( self.end!='now' && self._TO.getTime() > self.end ){
			dashboard.pause();
		}else{
			if( !self.static_start ){
				if( (self._TO.getTime()-self._FROM.getTime()) > self.interval )
					self._FROM.setSeconds( self._FROM.getSeconds() + 1 );
			}
			self._setTitles();
			self._setClock();
			self._setTimebar();
			self._setProgressbar();
			self._warning();
			if( ticsleft==0 || self._forceredraw ){
				self._forceredraw = false;
				self._redrawelements();
				self._updateOnServer();
				ticsleft = self._tics;
			}
			setTimeout( self._updateFROMTO, self.speed, --ticsleft, self );
		}
	}
}
Clock.prototype._redrawelements = function(){
	if( this.state=="PLAY" ){
//		var to = this._TO.getTime()+(this._tics*1000);
//		to = new Date( to );
		dashboard.draw( moment.utc( this._FROM ).format("YYYY-MM-DD HH:mm:ss"), moment.utc( this._TO ).format("YYYY-MM-DD HH:mm:ss") );
	}else if( this.state=="STOP"){
		dashboard.clear();
	}
}
Clock.prototype.pause = function(){
	this.state = "PAUSE";
	this._updateOnServer();
}
Clock.prototype.stop = function(){
	this.state = "STOP";
	this._updateOnServer();
	$('.duration').text( "" );
	this._FROM = null;
	this._TO = null;
	this._setClock();
	this._setTimebar();
	this._setProgressbar();
	this._redrawelements();
	this._setTitles();
	this._warning();
}
Clock.prototype.play = function(){
	this.state = "PLAY";
	if( this.end=='now' ){
		var now= new Date();
		var then = new Date();
		then.setSeconds( then.getSeconds() - this.interval/1000 );
		this.start = now.getTime();
		this._FROM = then;
		this._TO = now;
	}else{
		this._FROM = new Date( this.start );
		this._TO = new Date( this.start );
	}
	if( this.end!='now' ){
		var totaltime = (this.end-this.start)/1000;
		var m = Math.floor(totaltime/60)<10 ? "0"+Math.floor(totaltime/60) : Math.floor(totaltime/60);
		var s = Math.floor(totaltime-(m*60))<10 ? "0"+Math.floor(totaltime-(m*60)) : Math.floor(totaltime-(m*60));
		$('.duration').text( m+":"+s );
	}
	this._updateFROMTO( 0, this );
}
Clock.prototype.resume = function(){
	this.state = "PLAY";
	this._updateFROMTO( 0, this );
}
Clock.prototype.forceredrawelements = function(){
	this._forceredraw = true;
}
Clock.prototype._setClock = function(){
	var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
	var dayNames= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
	if(this._TO!=null && typeof(this._TO)!="undefined"){
		// Output the day, date, month and year   
		$('#Date').html(dayNames[this._TO.getDay()] + " " + this._TO.getDate() + ' ' + monthNames[this._TO.getMonth()] + ' ' + this._TO.getFullYear());
		var seconds = this._TO.getSeconds();
		$("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
		var minutes = this._TO.getMinutes();
		$("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
		var hours = this._TO.getHours();
		// Add a leading zero to the hours value
		$("#hours").html(( hours < 10 ? "0" : "" ) + hours);
	}else{
		$('#Date').html( "" );
		$("#sec").html( "" );
		$("#min").html( "" );
		$("#hours").html( "" );
	}
}
Clock.prototype._setProgressbar = function(){
	if( this.end!='now' ){
		var progress = $('.progress');
	
		var totaltime = this.end-this.start;
		var positionto = (this._TO==null ? this.start : this._TO.getTime())-this.start;
		var positionfrom = (this._FROM==null ? this.start : this._FROM.getTime())-this.start;
		var percentageto = 100 * positionto / totaltime;
		if(percentageto > 100) {
			percentageto = 100;
		}
		if(percentageto < 0) {
			percentageto = 0;
		}
		$('.timeBar').css('width',percentageto+'%');
		var percentagefrom = 100 * positionfrom / totaltime;
		if(percentagefrom > 100) {
			percentagefrom = 100;
		}
		if(percentagefrom < 0) {
			percentagefrom = 0;
		}
		$('.bufferBar').css('width',percentagefrom+'%');
	}else{
		if( this.state!='STOP' ){
			$('.progress-bar').css( 'display', 'block' );
		}else{
			$('.progress-bar').css( 'display', 'none' );
		}
	}
}
Clock.prototype._setTimebar = function(){
	if(this._TO!=null && typeof(this._TO)!="undefined"){
		var seconds = (this._TO.getTime()-this.start)/1000;
		var m = Math.floor(seconds/60)<10 ? "0"+Math.floor(seconds/60) : Math.floor(seconds/60);
		var s = Math.floor(seconds-(m*60))<10 ? "0"+Math.floor(seconds-(m*60)) : Math.floor(seconds-(m*60));
		$('.currenttime').text( m+":"+s );
	}else{
		$('.currenttime').text( "" );
	}
}
Clock.prototype.movedBar = function( percentage ){
	if(this.state!="STOP" && this.end!='now' ){
		var totaltime = this.end-this.start;
		var totime = percentage*totaltime/100;
		var to = totime + this.start;
		var temp = new Date( to );
		if( temp.getTime() <= this._FROM.getTime() ){
			this._FROM = new Date( to );
		}else{
			this._TO = new Date( to );
		}
		this._setClock();
		this._setTimebar();
		this._setProgressbar();
		this.forceredrawelements();
		this._warning();
	}
}
Clock.prototype._setTitles = function(){
	if( this.state=='STOP' ){
		$('.progress').prop( 'title', '' );
		$('.bufferBar').prop( 'title', '' );
	}else{
		$('.progress').prop( 'title', moment( this._TO ).format("YYYY-MM-DD HH:mm:ss") );
		$('.bufferBar').prop( 'title', moment( this._FROM ).format("YYYY-MM-DD HH:mm:ss") );
	}
}
Clock.prototype._warning = function(){
	if( this.state!="STOP" ){
		var tot = 0;
		for( var cuando in this._anomalies ){
			if( this._anomalies.hasOwnProperty(cuando) ){
				if( ( moment(cuando).toDate().getTime() >= this._FROM.getTime() ) && ( moment(cuando).toDate().getTime() <= this._TO.getTime() ) ){
					tot += this._anomalies[ cuando ];
				}
			}
		}
		$( '.w3-badge' ).text( tot );
		var hay = this._anomalies[ moment( this._TO ).format("YYYY-MM-DD HH:mm:ss") ];
		if( typeof( hay )!='undefined' ){
//			$.ajax({
//				type : "POST",
//				url : '/trap/' + hay,
//				success : function(data){
//				}
//			});
			$( '.w3-badge' ).css( {
				'-webkit-animation': 'pulsate 1s ease-out',
				'-webkit-animation-iteration-count': 'infinite'
			} );
			setTimeout( function(){
				$( '.w3-badge' ).css( {
					'-webkit-animation': 'none'
				} );
			},this.speed );
		}
	}else{
		$( '.w3-badge' ).text( "0" );
	}
}
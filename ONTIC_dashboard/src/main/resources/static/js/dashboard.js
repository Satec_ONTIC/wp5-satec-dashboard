/**
 * 
 */
function Dashboard(){
	this._pages = {
			
	};
	this.player;
	this.page;
	this.start = null;
	this.end = null;
}
Dashboard.prototype.init = function(){
	var self = this;
	$('.progress').on('mousedown', function(e){
		if( self.player )
			self.player.moveBar( e.pageX, true );
	});
	$(document).on('mouseup', function(e) {
		if( self.player ){
    		if( self.player.getDrag() ) {
    			self.player.moveBar( e.pageX, false );
    		}
		}
	});
	$(document).on('mousemove', function(e){
		var isIE = document.all? true:false;
	    var _x, _y;

	    if (isIE) {
	        _x = event.clientX + document.body.scrollLeft;
	        _y = event.clientY + document.body.scrollTop;
	    }else{
	        _x = e.pageX;
	        _y = e.pageY;
	    } 
	    posX=_x;
	    posY=_y;
	    posY+=DeltaY;
		if( self.player ){
    		if( self.player.getDrag() ) {
    			self.player.moveBar( e.pageX, null );
    		}
		}
	});
	document.addEventListener( "wheel", function( e ){
		var e = window.event || e; // old IE support
		DeltaY+=e.deltaY;
		return false;
	}, {passive: true} );
	$('.btnDE').click(function(){
		$('.btnStop').click();
		if( self.page ){
			  self.page = null;
		  }
		self.player = null;
		self.start = null;
		self.end = null;
		$.ajax({
		      type: "GET",
		      url: '/dinamicPcapLoaded',
		      success: function( data ) {
		    	  $.ajax({
		    	      type: "GET",
		    	      url: '/demo',
		    	      success: function( data2 ) {
		    	    	  $(".videoContainer").html( data2.html );
		    	    	  new DemoMode( data );
		    	      },
		    	      dataType: 'json'
		    	  });
		      },
		      dataType: 'json'
		  });
		$(".videoContainer").html("<div class='col-md-1 col-md-offset-5 loading'></div>");
	});
	$('.btnFO').click(function(){
		$('.btnStop').click();
		if( self.page ){
			  self.page = null;
		  }
		self.player = null;
		self.start = null;
		self.end = null;
		$.ajax({
		      type: "GET",
		      url: '/dinamicPcapLoaded',
		      success: function( data ) {
		    	  $.ajax({
		    	      type: "GET",
		    	      url: '/forensic',
		    	      success: function( data2 ) {
		    	    	  $(".videoContainer").html( data2.html );
		    	    	  new ForensicMode( data );
		    	      },
		    	      dataType: 'json'
		    	  });
		      },
		      dataType: 'json'
		  });
		$(".videoContainer").html("<div class='col-md-1 col-md-offset-5 loading'></div>");
	});
	$('.btnRT').click(function(){
		$('.btnStop').click();
		if( self.page ){
			  self.page = null;
		  }
		self.player = null;
		self.start = null;
		self.end = null;
		$.ajax({
		      type: "GET",
		      url: '/realtime',
		      success: function( data ) {
		    	  $(".videoContainer").html( data.html );
    	    	  new RealtimeMode();
		      },
		      dataType: 'json'
		  });
		$(".videoContainer").html("<div class='col-md-1 col-md-offset-5 loading'></div>");
	});
	$('.btnOpen').click(function(){
		$('.btnStop').click();
		if( self.page ){
			  self.page = null;
		  }
		self.player = null;
		self.start = null;
		self.end = null;
		$.ajax({
		      type: "GET",
		      url: '/openpcap',
		      success: function( data ) {
		    	  $(".videoContainer").html( data.html );
    	    	  new OpenPcap();
		      },
		      dataType: 'json'
		  });
		$(".videoContainer").html("<div class='col-md-1 col-md-offset-5 loading'></div>");
	});
	$('#settings').click(function(){
		$('.btnStop').click();
		if( self.page ){
			  self.page = null;
		  }
		self.player = null;
		self.start = null;
		self.end = null;
		$.ajax({
		      type: "GET",
		      url: '/settings',
		      success: function( data ) {
		    	  $(".videoContainer").html( data.html );
    	    	  new Settings();
		      },
		      dataType: 'json'
		  });
		$(".videoContainer").html("<div class='col-md-1 col-md-offset-5 loading'></div>");
	});
	$('#export').click(function(){
		$('.btnStop').click();
		if( self.page ){
			  self.page = null;
		  }
		self.player = null;
		self.start = null;
		self.end = null;
		$.ajax({
		      type: "GET",
		      url: '/export',
		      success: function( data ) {
		    	  $(".videoContainer").html( data.html );
    	    	  new Export();
		      },
		      dataType: 'json'
		  });
		$(".videoContainer").html("<div class='col-md-1 col-md-offset-5 loading'></div>");
	});
	$('#sobre').click(function(){
		self.pause();
		$('#about').css('display','block');
		Custombox.open({
            target: '#about',
            effect: 'fadein',
            width: '500px',
            speed: 1000,
            position: [ 'center' , 'center' ],
            close : function(){
            	$('#about').css('display','none');
            }
		});
	});
	$('.btnx8').click(function(){
		if( self.player )
			self.player.setSpeed( 'X8' );
	});
	$('.btnx4').click(function(){
		if( self.player )
			self.player.setSpeed( 'X4' );
	});
	$('.btnx2').click(function(){
		if( self.player )
			self.player.setSpeed( 'X2' );
	});
	$('.btnx1').click(function(){
		if( self.player )
			self.player.setSpeed( 'X1' );
	});
	$('.btnStop').click(function(){
		if( self.player )
			self.player.stop();
	});
	$('.btnPlay').click(function(){
		if( self.player ){
			if( $('.btnPlay').hasClass( 'paused' ) )
				self.player.pause();
			else
				self.player.play();
		}
	});
	$('#changedemo').click(function(){
		self._request_dynamic_html_sidebar('traffic');
	});
	$('#changeflow').click(function(){
		self._request_dynamic_html_sidebar('flow');
	});
	$('#changeanom').click(function(){
		self._request_dynamic_html_sidebar('unada');
	});
	$('#livemap').click(function(){
		window.open( "livemap.html", "ONTIC Attack Map", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, height="+screen.height+", width="+screen.width );
	});
	$('#shortcut').click(function(){
		$('.sidebar ul.nav-sidebar li.active a').click();
	});
	$( ".btnDE" ).click();
}
Dashboard.prototype.draw = function( FROM, TO ){
	if( this.page )
		this.page.draw( FROM, TO );
}
Dashboard.prototype.clear = function(){
	if( this.page )
		this.page.clear();
}
Dashboard.prototype.pause = function(){
	if( this.player )
		this.player.pause();
}
Dashboard.prototype.play = function(){
	if( this.player )
		this.player.play();
}
Dashboard.prototype.setStart = function( start ){
	if( this.player==null||typeof(this.player)=="undefined" ){
		  this.player = new Player( );
	  }
	  this.player.setStart( start );
	  this.start = start;
}
Dashboard.prototype.setEnd = function( end ){
		if( this.player==null||typeof(this.player)=="undefined" ){
			  this.player = new Player( );
		  }
		  this.player.setEnd( end );
		  this.end = end;  
}
Dashboard.prototype.setMode = function( mode ){
	if( this.player )
		this.player.setMode( mode );
}
Dashboard.prototype.setStatic = function( stat ){
	if( this.player )
		this.player.setStatic( stat );
}
Dashboard.prototype.setInterval = function( interval ){
	if( this.player )
		this.player.setInterval( interval );
}
Dashboard.prototype._request_dynamic_html_sidebar = function( query ){
	var self = this;
	if( self.page ){
		  self.page = null;
	  }
	$.ajax({
	      type: "GET",
	      url: '/'+query,
	      success: function( data ) {
	    	  $(".videoContainer").html( data.html );
	    	  if( self.start && self.end && self.end!='now' )
	    		  $('.page-header').append('<span>('+self.start+'</span><span>'+self.end+')</span>');
	    	  $(".sidebar ul.nav-sidebar li").each( function( index ){
		    		  if( query=="traffic" && index==0 ){
		    			  
		    			  $(this).addClass( "active" );  
		    			  self.page = new TrafficCharts();
		    			  
		    		  }else if( query=="flow" && index==1 ){
		    			  
		    			  $(this).addClass( "active" );
		    			  self.page = new FlowCharts();
		    			  
		    		  }else if( query=="unada" && index==2 ){
		    			  $(this).addClass( "active" );
		    			  self.page = new AnomalieCharts();
		    		  }else{
		    			  if( $(this).hasClass( "active" ) )
		    				  $(this).removeClass( "active" );
		    		  }
	    	  });
	    	  if( self.player )
	    		  self.player.forceclock();
	      },
	      dataType: 'json'
	  });
    $(".videoContainer").html("<div class='col-md-1 col-md-offset-5 loading'></div>");
}